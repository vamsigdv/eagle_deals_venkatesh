<?php

return [
    'shiprocket_email'        => env('SHIPROCKET_EMAIL'),
    'shiprocket_password'       => env('SHIPROCKET_PASSWORD'),
    'shiprocket_api_url' => env('SHIPROCKET_API_URL'),

];