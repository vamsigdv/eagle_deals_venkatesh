<?php


namespace App\Traits;

trait CurlRequestTrait
{
    
    public function curlPost($url, $token, $data)
    {
        try {
            $url =config('global.shiprocket_api_url').$url;
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . $token,
                'Content-Type: application/json',
            ]);

            $response = curl_exec($ch);

            if (curl_errno($ch)) {
                throw new \Exception("cURL error: " . curl_error($ch));
            }

            curl_close($ch);

            // \Log::info("Request Data: " . json_encode($data));
            // \Log::info("Response: " . $response);

            return json_decode($response, true);
        } catch (\Exception $e) {
            \Log::error("Error in cURL request: " . $e->getMessage());
            return null; // Or handle the error in a way appropriate to your application
        }
    }


    public function getToken()
    {
        $tokenData = [
            "email" => config('global.shiprocket_email'),
            "password" =>  config('global.shiprocket_password'),
         
        ];
        $url=config('global.shiprocket_api_url') . "auth/login";
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($tokenData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);

        $response = curl_exec($ch);

        curl_close($ch);

        return json_decode($response, true);
    }

    public function curlShipGet($url, $token)
    {   
        $url =config('global.shiprocket_api_url').$url;
        \Log::info($url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer '.$token,
            'Content-Type: application/json',
        ]);

        $response = curl_exec($ch);

        curl_close($ch);

        return json_decode($response, true);
    }
 
    public function curlPatch($url, $token, $data, $id)
    {
        $ch = curl_init($url . '/' . $id); // Append the user's ID to the URL

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH"); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $token,
            'Content-Type: application/json',
        ]);

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }
   
    public function curlDelete($url, $token, $postData = null)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $token,
            'cache-control: no-cache',
            'content-type: application/json'
        ]);

        if ($postData !== null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }

    public function generateRandomPassword($length = 10) {
        $lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
        $uppercaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $numbers = '0123456789';
        $specialCharacters = '!@#$%^&*';
        
        $characterSets = [$lowercaseLetters, $uppercaseLetters, $numbers, $specialCharacters];
        $password = '';
        
        // Make sure there is at least one character from each character set
        foreach ($characterSets as $charSet) {
            $password .= $charSet[rand(0, strlen($charSet) - 1)];
        }
        
        // Fill the rest of the password with random characters
        for ($i = strlen($password); $i < $length; $i++) {
            $randomCharSet = $characterSets[rand(0, count($characterSets) - 1)];
            $password .= $randomCharSet[rand(0, strlen($randomCharSet) - 1)];
        }
        
        // Shuffle the password to randomize the character order
        $password = str_shuffle($password);
        
        // Ensure there are no more than 2 identical characters in a row
        $password = preg_replace('/(.)\\1{2,}/', '$1$1', $password);
        
        return $password;
    }

    public function formatPermissionData($data){
        $resourceServerIdentifier =config('global.auth0_api_identifier');
        $postData = [
            "permissions" => []
        ];
        foreach ($data as $permission) {
            // Construct the associative array for the permission
            $permissionData = [
                "resource_server_identifier" => $resourceServerIdentifier,
                "permission_name" => $permission
            ];
            // Add the permission data to the "permissions" array
            $postData["permissions"][] = $permissionData;
        }
        // Print the resulting $postData
        return $postData;
    }
}