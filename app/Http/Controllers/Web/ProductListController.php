<?php

namespace App\Http\Controllers\Web;

use App\CPU\Helpers;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\Category;
use App\Model\FlashDeal;
use App\Model\FlashDealProduct;
use App\Model\OrderDetail;
use App\Model\Product;
use App\Model\Review;
use App\Model\Shop;
use App\Model\Translation;
use App\Model\Wishlist;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function App\CPU\translate;

class ProductListController extends Controller
{
    public function products(Request $request)
    {
        $theme_name = theme_root_path();

        return match ($theme_name){
            'default' => self::default_theme($request),
            'theme_aster' => self::theme_aster($request),
            'theme_fashion' => self::theme_fashion($request),
            'theme_all_purpose' => self::theme_all_purpose($request),
        };
    }

    public function get_all_products(Request $request){
        // \Log::error('vamsi');
        \Log::error($request);
        // brand: filter_by_brand,
        // category_id:category_id,
        // search_by_filter_value:search_by_filter_value,
        // discount_checkbox: discount_checkbox,
        // sort_by: filter_sort_product,
        // min_price: min,
        
        //              max_price: max,
        // stock_checkbox:stock_checkbox,
        // filter_by_ram:filter_by_ram,
       

        // filter_by_rating:filter_by_rating,
                      

        $request['sort_by'] == null ? $request['sort_by'] == 'latest' : $request['sort_by'];

        // $products = Product::where('status',1)->with(['reviews'])->select('id','name','slug','thumbnail','purchase_price','unit_price')->get();
        $porduct_data = Product::where('status','1')->with(['reviews','rating']);
        if ($request->has('category_id')) {
            
            $products = $porduct_data->get();
            $product_ids = [];
            foreach ($products as $product) {
                foreach (json_decode($product['category_ids'], true) as $category) {
                    if ($category['id'] == $request['category_id']) {
                        array_push($product_ids, $product['id']);
                    }
                }
            }
            $query = $porduct_data->whereIn('id', $product_ids);
          
        }
        if ($request->has('brand')) {
            // \Log::info($request['brand']);
            \Log::error('brand');
            $query = $porduct_data->whereIn('brand_id', $request['brand']);
        }
        if (!$request->has('search_by_filter_value') || $request['search_by_filter_value'] == 'latest') {
            $query = $porduct_data;
        }
        if($request->has('search_by_filter_value')){
            \Log::error('search by filter value');
            if ($request['search_by_filter_value'] == 'best_selling_products') {
                $details = OrderDetail::with('product')
                    ->select('product_id', DB::raw('COUNT(product_id) as count'))
                    ->groupBy('product_id')
                    ->orderBy("count", 'desc')
                    ->get();
                $product_ids = [];
                foreach ($details as $detail) {
                    array_push($product_ids, $detail['product_id']);
                }
                $query = $porduct_data->whereIn('id', $product_ids);
            }
    
            if ($request['search_by_filter_value'] == 'most-favorite') {
                $details = Wishlist::with('product')
                    ->select('product_id', DB::raw('COUNT(product_id) as count'))
                    ->groupBy('product_id')
                    ->orderBy("count", 'desc')
                    ->get();
                $product_ids = [];
                foreach ($details as $detail) {
                    array_push($product_ids, $detail['product_id']);
                }
                $query = $porduct_data->whereIn('id', $product_ids);
            }
    
            if ($request['search_by_filter_value'] == 'featured') {
                $query = Product::with(['reviews'])->active()->where('featured', 1);
            }
    
            if ($request['search_by_filter_value'] == 'featured_deal') {
                $featured_deal_id = FlashDeal::where(['status'=>1])->where(['deal_type'=>'flash_deal'])->pluck('id')->first();
                $featured_deal_product_ids = FlashDealProduct::where('flash_deal_id',$featured_deal_id)->pluck('product_id')->toArray();
                $query = Product::with(['reviews'])->active()->whereIn('id', $featured_deal_product_ids);
            }
            if ($request['search_by_filter_value'] == 'top_rated') {
                $reviews = Review::select('product_id', DB::raw('AVG(rating) as count'))
                    ->groupBy('product_id')
                    ->orderBy("count", 'desc')->get();
                $product_ids = [];
                foreach ($reviews as $review) {
                    array_push($product_ids, $review['product_id']);
                }
                $query = $porduct_data->whereIn('id', $product_ids);
            }
        }
        
        if ($request->has('discount_checkbox')) {
            \Log::error('discouynt');
            // $query = Product::with(['reviews'])->active()->whereIn('discount', $request->input('discount_checkbox'));
            $query =  $porduct_data->whereIn('discount', $request->input('discount_checkbox'));

        }
        if ($request->has('filter_by_ram')) {
            \Log::error('filter_by_ram');
            $query =  $porduct_data->whereIn('ram', $request->input('filter_by_ram'));
        }
                // Check if filter_by_rating is an array
        if ($request->has('filter_by_rating') && is_array($request->input('filter_by_rating'))) {
            // Get array of ratings
            $ratings = $request->input('filter_by_rating');

            // Initialize an empty array to store product IDs
            $productIds = [];

            // Loop through each rating in the array
            foreach ($ratings as $rating) {
                // Get the average ratings for products
                $reviews = Review::select('product_id', DB::raw('AVG(rating) as average_rating'))
                    ->groupBy('product_id')
                    ->orderBy('average_rating', 'desc')
                    ->get();

                // Filter product IDs based on the current rating
                $filteredProductIds = $reviews->filter(function ($review) use ($rating) {
                    return $review->average_rating >= $rating;
                })->pluck('product_id')->toArray();

                // Merge the filtered product IDs into the main array
                $productIds = array_merge($productIds, $filteredProductIds);
            }

            // Remove duplicates from the product IDs array
            $productIds = array_unique($productIds);

            // Retrieve products based on filtered product IDs
            $query = $porduct_data->whereIn('id', $productIds);
        }

        if ($request->has('stock_checkbox')) {
            \Log::error('stock_checkbox');
            
            $stockCheckboxValues = $request->input('stock_checkbox');
            
            if (in_array('in_stock', $stockCheckboxValues)) {
                \Log::error('in_stock is present');
                $query = $porduct_data->where('current_stock', '>', 1);
            }
            
            if (in_array('out_of_stock', $stockCheckboxValues)) {
                \Log::error('out_of_stock is present');
                $query = $porduct_data->where('current_stock', '<=', 1);
            }
        }
        
        if ($request['sort_by'] == 'latest') {
            $fetched = $query->latest();
        } elseif ($request['sort_by'] == 'low-high') {
            $fetched = $query->orderBy('unit_price', 'ASC');
        } elseif ($request['sort_by'] == 'high-low') {
            $fetched = $query->orderBy('unit_price', 'DESC');
        } elseif ($request['sort_by'] == 'a-z') {
            $fetched = $query->orderBy('name', 'ASC');
        } elseif ($request['sort_by'] == 'z-a') {
            $fetched = $query->orderBy('name', 'DESC');
        } else {
            $fetched = $query->latest();
        }

        if ($request['min_price'] != null || $request['max_price'] != null) {
            $fetched = $fetched->whereBetween('purchase_price', [$request['min_price'], $request['max_price']]);
        }
        if ($request->has('search_by_name')) {
            $key = explode(' ', $request->input('search_by_name'));
            $product_ids = Product::where(function ($q) use ($key) {
                foreach ($key as $value) {
                    $q->orWhere('name', 'like', "%{$value}%")
                        ->orWhereHas('tags',function($query)use($value){
                            $query->where('tag', 'like', "%{$value}%");
                        });
                }
            })->pluck('id');

            if($product_ids->count()==0)
            {
                $product_ids = Translation::where('translationable_type', 'App\Model\Product')
                    ->where('key', 'name')
                    ->where(function ($q) use ($key) {
                        foreach ($key as $value) {
                            $q->orWhere('value', 'like', "%{$value}%");
                        }
                    })
                    ->pluck('translationable_id');


            }

            $fetched= $query = $porduct_data->WhereIn('id', $product_ids);

        }
        // $fetched=$fetched->paginate(20);
      
        $final_data['fetched']=[$fetched->paginate(20)];
        if(Helpers::get_customer() != 'offline')
        {
            $customer_id=Helpers::get_customer()->id;
            $final_data['wishlist']=Wishlist::where('customer_id',$customer_id)->pluck('product_id');
            
        } else {
            $final_data['wishlist'] = collect(); 
        }
       
        return $final_data;
        return [$fetched];
        $filtered_products=[];
        $filtered_products['fetched']=$fetched->paginate(20);

        if ($request->has('category_id')) {
            $brand_products = Product::active()
                ->where('category_id', $request->input('category_id'))
                ->orWhere('sub_category_id', $request->input('category_id'))
                ->orWhere('sub_sub_category_id', $request->input('category_id'))
                ->pluck('brand_id');
        
            if ($brand_products->isNotEmpty()) {
                $filtered_products['brands'] = Brand::active()
                    ->whereIn('id', $brand_products)
                    ->has('brandProducts')
                    ->withCount('brandProducts')
                    ->take(15)
                    ->get();
            } else {
                $filtered_products['brands'] = null;
            }
            $category = Category::findOrFail($request->input('category_id'));

            $mainCategoryId = null;
            // \Log::info('vamsikrishna');
            // Check the position of the category
            if ($category->position == 0) {
                // Main category
                // \Log::info('category');
                $mainCategoryId = $category->id;
            } elseif ($category->position == 1) {
                // Subcategory
                // Fetch the main category ID
                \Log::info('sub_category');
                $mainCategoryId=Category::where('id',$category->parent_id)->first()->id;
               
            } elseif ($category->position == 2) {
                // Sub-subcategory
                // Fetch the subcategory first
                \Log::info('s_sub_category');
                $subCategory='';
                $subCategory = Category::where('id',$category->parent_id)->first()->parent_id;
                $mainCategoryId=Category::where('id',$subCategory)->first()->id;
             }
            
            \Log::info($mainCategoryId);
            $filtered_products['categories'] = Category::where('id',$mainCategoryId)->with('childes.childes')->get();

            // Assign fetched data to separate keys in an array
            
        }

        return $filtered_products;

        
        
    }
    public function get_filter_page(Request $request){
        $brands = Brand::active()->has('brandProducts')->withCount('brandProducts')->take(15)->get();
        $categories =Category::where(['position' => 0])->with('childes.childes')->get();

        // $categories = Category::where(['position' => 0])->with('childes:id,name,position.childes')->priority()->take(11)->select('id','name')->get();
        return view(VIEW_FILE_NAMES['products_view_page'],compact('brands','categories'));
        
    }
    public function get_filter_page_category_based(Request $request,Category $category_id){
        if ($category_id) {
            $brand_products = Product::active()
                ->where('category_id', $category_id->id)
                ->orWhere('sub_category_id', $category_id->id)
                ->orWhere('sub_sub_category_id', $category_id->id)
                ->pluck('brand_id');
        
            if ($brand_products->isNotEmpty()) {
                $categoryid=$category_id->id;
                $brands = Brand::active()
                    ->whereIn('id', $brand_products)
                    ->has('brandProducts')
                    ->withCount(['brandProducts' => function ($query) use ($categoryid) {
                        $query->where('category_id', $categoryid)
                        ->orWhere('sub_category_id', $categoryid)
                        ->orWhere('sub_sub_category_id', $categoryid);
                    }])
                    ->take(15)
                    ->get();
            } else {
                $brands = null;
            }
       
            $category = Category::findOrFail($category_id->id);

            $mainCategoryId = null;
            // \Log::info('vamsikrishna');
            // Check the position of the category
            if ($category->position == 0) {
                // Main category
                // \Log::info('category');
                $mainCategoryId = $category->id;
            } elseif ($category->position == 1) {
                // Subcategory
                // Fetch the main category ID
                // \Log::info('sub_category');
                $mainCategoryId=Category::where('id',$category->parent_id)->first()->id;
               
            } elseif ($category->position == 2) {
                // Sub-subcategory
                // Fetch the subcategory first
                // \Log::info('s_sub_category');
                $subCategory='';
                $subCategory = Category::where('id',$category->parent_id)->first()->parent_id;
                $mainCategoryId=Category::where('id',$subCategory)->first()->id;
             }
            
            // \Log::info($mainCategoryId);
            $categories = Category::where('id',$mainCategoryId)->with('childes.childes')->get();

            // Assign fetched data to separate keys in an array
            
        }
        // $categories = Category::where(['position' => 0])->with('childes:id,name,position.childes')->priority()->take(11)->select('id','name')->get();
        return view('web-views.products.view_category_based_products',compact('brands','categories'));
        
    }
    
    public function default_theme($request){
        \Log::error($request);
        \Log::error(  VIEW_FILE_NAMES['products_view_page']);
      
        $request['sort_by'] == null ? $request['sort_by'] == 'latest' : $request['sort_by'];

        $porduct_data = Product::with(['reviews']);

        if ($request['data_from'] == 'category') {
            $products = $porduct_data->get();
            $product_ids = [];
            foreach ($products as $product) {
                foreach (json_decode($product['category_ids'], true) as $category) {
                    if ($category['id'] == $request['id']) {
                        array_push($product_ids, $product['id']);
                    }
                }
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'brand') {
            $query = $porduct_data->where('brand_id', $request['id']);
        }

        // if (!$request->has('data_from') || $request['data_from'] == 'latest') {
        //     $query = $porduct_data;
        // }

        // if ($request['data_from'] == 'top-rated') {
        //     $reviews = Review::select('product_id', DB::raw('AVG(rating) as count'))
        //         ->groupBy('product_id')
        //         ->orderBy("count", 'desc')->get();
        //     $product_ids = [];
        //     foreach ($reviews as $review) {
        //         array_push($product_ids, $review['product_id']);
        //     }
        //     $query = $porduct_data->whereIn('id', $product_ids);
        // }

        // if ($request['data_from'] == 'best-selling') {
        //     $details = OrderDetail::with('product')
        //         ->select('product_id', DB::raw('COUNT(product_id) as count'))
        //         ->groupBy('product_id')
        //         ->orderBy("count", 'desc')
        //         ->get();
        //     $product_ids = [];
        //     foreach ($details as $detail) {
        //         array_push($product_ids, $detail['product_id']);
        //     }
        //     $query = $porduct_data->whereIn('id', $product_ids);
        // }

        // if ($request['data_from'] == 'most-favorite') {
        //     $details = Wishlist::with('product')
        //         ->select('product_id', DB::raw('COUNT(product_id) as count'))
        //         ->groupBy('product_id')
        //         ->orderBy("count", 'desc')
        //         ->get();
        //     $product_ids = [];
        //     foreach ($details as $detail) {
        //         array_push($product_ids, $detail['product_id']);
        //     }
        //     $query = $porduct_data->whereIn('id', $product_ids);
        // }

        // if ($request['data_from'] == 'featured') {
        //     $query = Product::with(['reviews'])->active()->where('featured', 1);
        // }

        // if ($request['data_from'] == 'featured_deal') {
        //     $featured_deal_id = FlashDeal::where(['status'=>1])->where(['deal_type'=>'feature_deal'])->pluck('id')->first();
        //     $featured_deal_product_ids = FlashDealProduct::where('flash_deal_id',$featured_deal_id)->pluck('product_id')->toArray();
        //     $query = Product::with(['reviews'])->active()->whereIn('id', $featured_deal_product_ids);
        // }

        if ($request['data_from'] == 'search') {
            $key = explode(' ', $request['name']);
            $product_ids = Product::where(function ($q) use ($key) {
                foreach ($key as $value) {
                    $q->orWhere('name', 'like', "%{$value}%")
                        ->orWhereHas('tags',function($query)use($value){
                            $query->where('tag', 'like', "%{$value}%");
                        });
                }
            })->pluck('id');

            if($product_ids->count()==0)
            {
                $product_ids = Translation::where('translationable_type', 'App\Model\Product')
                    ->where('key', 'name')
                    ->where(function ($q) use ($key) {
                        foreach ($key as $value) {
                            $q->orWhere('value', 'like', "%{$value}%");
                        }
                    })
                    ->pluck('translationable_id');


            }

            $query = $porduct_data->WhereIn('id', $product_ids);

        }

        // if ($request['data_from'] == 'discounted') {
        //     $query = Product::with(['reviews'])->active()->where('discount', '!=', 0);
        // }

        // if ($request['sort_by'] == 'latest') {
        //     $fetched = $query->latest();
        // } elseif ($request['sort_by'] == 'low-high') {
        //     $fetched = $query->orderBy('unit_price', 'ASC');
        // } elseif ($request['sort_by'] == 'high-low') {
        //     $fetched = $query->orderBy('unit_price', 'DESC');
        // } elseif ($request['sort_by'] == 'a-z') {
        //     $fetched = $query->orderBy('name', 'ASC');
        // } elseif ($request['sort_by'] == 'z-a') {
        //     $fetched = $query->orderBy('name', 'DESC');
        // } else {
        //     $fetched = $query->latest();
        // }

        // if ($request['min_price'] != null || $request['max_price'] != null) {
        //     $fetched = $fetched->whereBetween('unit_price', [Helpers::convert_currency_to_usd($request['min_price']), Helpers::convert_currency_to_usd($request['max_price'])]);
        // }

        $data = [
            'id' => $request['id'],
            'name' => $request['name'],
            'data_from' => $request['data_from'],
            'sort_by' => $request['sort_by'],
            'page_no' => $request['page'],
            'min_price' => $request['min_price'],
            'max_price' => $request['max_price'],
        ];

        $products = $fetched->paginate(20)->appends($data);

        if ($request->ajax()) {

            return response()->json([
                'total_product'=>$products->total(),
                'view' => view('web-views.products._ajax-products', compact('products'))->render()
            ], 200);
        }
        if ($request['data_from'] == 'category') {
            $data['brand_name'] = Category::find((int)$request['id'])->name;
        }
        if ($request['data_from'] == 'brand') {
            $brand_data = Brand::active()->find((int)$request['id']);
            if($brand_data) {
                $data['brand_name'] = $brand_data->name;
            }else {
                Toastr::warning(translate('not_found'));
                return redirect('/');
            }
        }
        //get products
        return $products;
        return view(VIEW_FILE_NAMES['products_view_page'], compact('products', 'data'));
    }

    public function theme_aster($request){
        $request['sort_by'] == null ? $request['sort_by'] == 'latest' : $request['sort_by'];

        $porduct_data = Product::active()->with([
            'reviews','rating',
            'seller.shop',
            'wish_list'=>function($query){
                return $query->where('customer_id', Auth::guard('customer')->user()->id ?? 0);
            },
            'compare_list'=>function($query){
                return $query->where('user_id', Auth::guard('customer')->user()->id ?? 0);
            }
        ]);

        $product_ids = [];
        if ($request['data_from'] == 'category') {
            $products = $porduct_data->get();
            $product_ids = [];
            foreach ($products as $product) {
                foreach (json_decode($product['category_ids'], true) as $category) {
                    if ($category['id'] == $request['id']) {
                        array_push($product_ids, $product['id']);
                    }
                }
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request->has('search_category_value') && $request['search_category_value'] != 'all') {
            $products = $porduct_data->get();
            $product_ids = [];
            foreach ($products as $product) {
                foreach (json_decode($product['category_ids'], true) as $category) {
                    if ($category['id'] == $request['search_category_value']) {
                        array_push($product_ids, $product['id']);
                    }
                }
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'brand') {
            $query = $porduct_data->where('brand_id', $request['id']);
        }

        if (!$request->has('data_from') || $request['data_from'] == 'latest') {
            $query = $porduct_data;
        }

        if ($request['data_from'] == 'top-rated') {
            $reviews = Review::select('product_id', DB::raw('AVG(rating) as count'))
                ->groupBy('product_id')
                ->orderBy("count", 'desc')->get();
            $product_ids = [];
            foreach ($reviews as $review) {
                array_push($product_ids, $review['product_id']);
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'best-selling') {
            $details = OrderDetail::with('product')
                ->select('product_id', DB::raw('COUNT(product_id) as count'))
                ->groupBy('product_id')
                ->orderBy("count", 'desc')
                ->get();
            $product_ids = [];
            foreach ($details as $detail) {
                array_push($product_ids, $detail['product_id']);
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'most-favorite') {
            $details = Wishlist::with('product')
                ->select('product_id', DB::raw('COUNT(product_id) as count'))
                ->groupBy('product_id')
                ->orderBy("count", 'desc')
                ->get();
            $product_ids = [];
            foreach ($details as $detail) {
                array_push($product_ids, $detail['product_id']);
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'featured') {
            $query = Product::with([
                'reviews','seller.shop',
                'wish_list'=>function($query){
                    return $query->where('customer_id', Auth::guard('customer')->user()->id ?? 0);
                },
                'compare_list'=>function($query){
                    return $query->where('user_id', Auth::guard('customer')->user()->id ?? 0);
                }
            ])->active()->where('featured', 1);
        }

        if ($request['data_from'] == 'featured_deal') {
            $featured_deal_id = FlashDeal::where(['status'=>1])->where(['deal_type'=>'feature_deal'])->pluck('id')->first();
            $featured_deal_product_ids = FlashDealProduct::where('flash_deal_id',$featured_deal_id)->pluck('product_id')->toArray();
            $query = Product::with([
                'reviews','seller.shop',
                'wish_list'=>function($query){
                    return $query->where('customer_id', Auth::guard('customer')->user()->id ?? 0);
                },
                'compare_list'=>function($query){
                    return $query->where('user_id', Auth::guard('customer')->user()->id ?? 0);
                }
            ])->active()->whereIn('id', $featured_deal_product_ids);
        }

        if ($request['data_from'] == 'search') {
            $key = explode(' ', $request['name']);
                $product_ids = Product::with([
                    'seller.shop',
                    'wish_list'=>function($query){
                        return $query->where('customer_id', Auth::guard('customer')->user()->id ?? 0);
                    },
                    'compare_list'=>function($query){
                        return $query->where('user_id', Auth::guard('customer')->user()->id ?? 0);
                    }
                ])
                ->where(function ($q) use ($key) {
                    foreach ($key as $value) {
                        $q->orWhere('name', 'like', "%{$value}%")
                            ->orWhereHas('tags',function($query)use($value){
                                $query->where('tag', 'like', "%{$value}%");
                            });
                    }
                })->pluck('id');

            if($product_ids->count()==0)
            {
                $product_ids = Translation::where('translationable_type', 'App\Model\Product')
                    ->where('key', 'name')
                    ->where(function ($q) use ($key) {
                        foreach ($key as $value) {
                            $q->orWhere('value', 'like', "%{$value}%");
                        }
                    })
                    ->pluck('translationable_id');
            }

            $query = $porduct_data->WhereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'discounted') {
            $query = Product::with([
                'reviews','seller.shop',
                'wish_list'=>function($query){
                    return $query->where('customer_id', Auth::guard('customer')->user()->id ?? 0);
                },
                'compare_list'=>function($query){
                    return $query->where('user_id', Auth::guard('customer')->user()->id ?? 0);
                }
            ])->active()->where('discount', '!=', 0);
        }

        if(!$request['data_from'] && !$request['name'] && $request['ratings']){
            $query = $query ?? $porduct_data;
        }

        if ($request['sort_by'] == 'latest') {
            $fetched = $query->latest();
        } elseif ($request['sort_by'] == 'low-high') {
            $fetched = $query->orderBy('unit_price', 'ASC');
        } elseif ($request['sort_by'] == 'high-low') {
            $fetched = $query->orderBy('unit_price', 'DESC');
        } elseif ($request['sort_by'] == 'a-z') {
            $fetched = $query->orderBy('name', 'ASC');
        } elseif ($request['sort_by'] == 'z-a') {
            $fetched = $query->orderBy('name', 'DESC');
        } else {
            $fetched = $query->latest();
        }

        if ($request['min_price'] != null || $request['max_price'] != null) {
            $fetched = $fetched->whereBetween('unit_price', [Helpers::convert_currency_to_usd($request['min_price']), Helpers::convert_currency_to_usd($request['max_price'])]);
        }

        if ($request['ratings'] != null)
        {
            $fetched->with('rating')->whereHas('rating', function($query) use($request){
                return $query;
            });
        }

        $data = [
            'id' => $request['id'],
            'name' => $request['name'],
            'data_from' => $request['data_from'],
            'sort_by' => $request['sort_by'],
            'page_no' => $request['page'],
            'min_price' => $request['min_price'],
            'max_price' => $request['max_price'],
        ];
        $common_query = $fetched;

        $rating_1 = 0;
        $rating_2 = 0;
        $rating_3 = 0;
        $rating_4 = 0;
        $rating_5 = 0;

        foreach($common_query->get() as $rating){
            if(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] >0 && $rating->rating[0]['average'] <2)){
                $rating_1 += 1;
            }elseif(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] >=2 && $rating->rating[0]['average'] <3)){
                $rating_2 += 1;
            }elseif(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] >=3 && $rating->rating[0]['average'] <4)){
                $rating_3 += 1;
            }elseif(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] >=4 && $rating->rating[0]['average'] <5)){
                $rating_4 += 1;
            }elseif(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] == 5)){
                $rating_5 += 1;
            }
        }
        $ratings = [
            'rating_1'=>$rating_1,
            'rating_2'=>$rating_2,
            'rating_3'=>$rating_3,
            'rating_4'=>$rating_4,
            'rating_5'=>$rating_5,
        ];

        $products = $common_query->paginate(20)->appends($data);

        if ($request['ratings'] != null)
        {
            $products = $products->map(function($product) use($request){
                $product->rating = $product->rating->pluck('average')[0];
                return $product;
            });
            $products = $products->where('rating','>=',$request['ratings'])
                ->where('rating','<',$request['ratings']+1)
                ->paginate(20)->appends($data);
        }

        if ($request->ajax()) {
            return response()->json([
                'total_product'=>$products->total(),
                'view' => view(VIEW_FILE_NAMES['products__ajax_partials'], compact('products','product_ids'))->render(),
            ], 200);
        }
        if ($request['data_from'] == 'category') {
            $data['brand_name'] = Category::find((int)$request['id'])->name;
        }
        if ($request['data_from'] == 'brand') {
            $brand_data = Brand::active()->find((int)$request['id']);
            if($brand_data) {
                $data['brand_name'] = $brand_data->name;
            }else {
                Toastr::warning(translate('not_found'));
                return redirect('/');
            }
        }

        return view(VIEW_FILE_NAMES['products_view_page'], compact('products', 'data', 'ratings', 'product_ids'));
    }

    public function theme_fashion(Request $request)
    {

        $tag_category = [];
        if($request->data_from == 'category')
        {
            $tag_category = Category::where('id',$request->id)->select('id', 'name')->get();
        }

        $tag_brand = [];
        if($request->data_from == 'brand')
        {
            $tag_brand = Brand::where('id', $request->id)->select('id','name')->get();
        }
        $request['sort_by'] == null ? $request['sort_by'] == 'latest' : $request['sort_by'];

        $porduct_data = Product::active()->withSum('order_details', 'qty', function ($query) {
                            $query->where('delivery_status', 'delivered');
                        })
                        ->with(['category','reviews','rating','wish_list'=>function($query){
                            return $query->where('customer_id', Auth::guard('customer')->user()->id ?? 0);
                        },
                        'compare_list'=>function($query){
                            return $query->where('user_id', Auth::guard('customer')->user()->id ?? 0);
                        }]);

        $product_ids = [];
        if ($request['data_from'] == 'category') {
            $products = $porduct_data->get();
            $product_ids = [];
            foreach ($products as $product) {
                foreach (json_decode($product['category_ids'], true) as $category) {
                    if ($category['id'] == $request['id']) {
                        array_push($product_ids, $product['id']);
                    }
                }
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request->has('search_category_value') && $request['search_category_value'] != 'all') {
            $products = $porduct_data->get();
            $product_ids = [];
            foreach ($products as $product) {
                foreach (json_decode($product['category_ids'], true) as $category) {
                    if ($category['id'] == $request['search_category_value']) {
                        array_push($product_ids, $product['id']);
                    }
                }
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'brand') {
            $query = $porduct_data->where('brand_id', $request['id']);
        }

        if ($request['data_from'] == 'latest') {
            $query = $porduct_data;
        }
        if (!$request->has('data_from') || $request['data_from'] == 'default') {
            $query = $porduct_data->orderBy('order_details_sum_qty', 'DESC');
        }

        if ($request['data_from'] == 'top-rated') {
            $reviews = Review::select('product_id', DB::raw('AVG(rating) as count'))
                ->groupBy('product_id')
                ->orderBy("count", 'desc')->get();
            $product_ids = [];
            foreach ($reviews as $review) {
                array_push($product_ids, $review['product_id']);
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'best-selling') {
            $details = OrderDetail::with('product')
                ->select('product_id', DB::raw('COUNT(product_id) as count'))
                ->groupBy('product_id')
                ->orderBy("count", 'desc')
                ->get();
            $product_ids = [];
            foreach ($details as $detail) {
                array_push($product_ids, $detail['product_id']);
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'most-favorite') {
            $details = Wishlist::with('product')
                ->select('product_id', DB::raw('COUNT(product_id) as count'))
                ->groupBy('product_id')
                ->orderBy("count", 'desc')
                ->get();
            $product_ids = [];
            foreach ($details as $detail) {
                array_push($product_ids, $detail['product_id']);
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'featured') {
            $query = Product::with(['reviews'])->active()->where('featured', 1);
        }

        if ($request->has('shop_id') && $request['shop_id'] == 0) {
            $query = Product::active()
                    ->with(['reviews'])
                    ->where(['added_by'=>'admin','featured'=>1]);
        }elseif($request->has('shop_id') && $request['shop_id'] != 0){
            $query = Product::active()
                        ->where(['added_by' => 'seller', 'featured' => 1])
                        ->with(['reviews', 'seller.shop' => function($query) use ($request) {
                            $query->where('id', $request->shop_id);
                        }])
                        ->whereHas('seller.shop', function($query) use ($request) {
                            $query->where('id', $request->shop_id)->whereNotNull('id');
                        });
        }

        if ($request['data_from'] == 'featured_deal') {
            $featured_deal_id = FlashDeal::where(['status'=>1])->where(['deal_type'=>'feature_deal'])->pluck('id')->first();
            $featured_deal_product_ids = FlashDealProduct::where('flash_deal_id',$featured_deal_id)->pluck('product_id')->toArray();
            $query = Product::with(['reviews'])->active()->whereIn('id', $featured_deal_product_ids);
        }

        if ($request['data_from'] == 'search') {
            $key = explode(' ', $request['name']);
            $product_ids = Product::where(function ($q) use ($key) {
                foreach ($key as $value) {
                    $q->orWhere('name', 'like', "%{$value}%")
                        ->orWhereHas('tags',function($query)use($value){
                            $query->where('tag', 'like', "%{$value}%");
                        });
                }
            })->pluck('id');

            $sellers = Shop::where(function ($q) use ($request) {
                $q->orWhere('name', 'like', "%{$request['name']}%");
            })->whereHas('seller', function ($query) {
                return $query->where(['status' => 'approved']);
            })->with('product', function($query){
                return $query->active()->where('added_by', 'seller');
            })->get();

            $seller_products = [];
            foreach($sellers as $seller){
                if(isset($seller->product) && $seller->product->count() > 0)
                {
                    $ids = $seller->product->pluck('id');
                    array_push($seller_products, ...$ids);
                }
            }

            $inhouse_product = [];
            $company_name = Helpers::get_business_settings('company_name');

            if (strpos($request['name'], $company_name) !== false) {
                $inhouse_product = Product::active()->Where('added_by', 'admin')->pluck('id');
            }

            $product_ids = $product_ids->merge($seller_products)->merge($inhouse_product);


            if($product_ids->count()==0)
            {
                $product_ids = Translation::where('translationable_type', 'App\Model\Product')
                    ->where('key', 'name')
                    ->where(function ($q) use ($key) {
                        foreach ($key as $value) {
                            $q->orWhere('value', 'like', "%{$value}%");
                        }
                    })
                    ->pluck('translationable_id');
            }

            $query = $porduct_data->WhereIn('id', $product_ids);

        }

        if ($request['data_from'] == 'discounted') {
            $query = Product::with(['reviews'])->active()->where('discount', '!=', 0);
        }

        if ($request['sort_by'] == 'latest') {
            $fetched = $query->latest();
        } elseif ($request['sort_by'] == 'low-high') {
            $fetched = $query->orderBy('unit_price', 'ASC');
        } elseif ($request['sort_by'] == 'high-low') {
            $fetched = $query->orderBy('unit_price', 'DESC');
        } elseif ($request['sort_by'] == 'a-z') {
            $fetched = $query->orderBy('name', 'ASC');
        } elseif ($request['sort_by'] == 'z-a') {
            $fetched = $query->orderBy('name', 'DESC');
        } else {
            $fetched = $query->latest();
        }

        if ($request['min_price'] != null || $request['max_price'] != null) {
            $fetched = $fetched->whereBetween('unit_price', [Helpers::convert_currency_to_usd($request['min_price']), Helpers::convert_currency_to_usd($request['max_price'])]);
        }
        $common_query = $fetched;

        $products = $common_query->paginate(20);

        if ($request['ratings'] != null)
        {
            $products = $products->map(function($product) use($request){
                $product->rating = $product->rating->pluck('average')[0];
                return $product;
            });
            $products = $products->where('rating','>=',$request['ratings'])
                ->where('rating','<',$request['ratings']+1)
                ->paginate(20);
        }

        // Categories start
        $categories = Category::withCount(['product'=>function($query){
                $query->active();
            }])->with(['childes' => function ($query) {
                $query->with(['childes' => function ($query) {
                    $query->withCount(['sub_sub_category_product'])->where('position', 2);
                }])->withCount(['sub_category_product'])->where('position', 1);
            }, 'childes.childes'])
            ->where('position', 0)->get();
        // Categories End

        // Colors Start
        $colors_in_shop_merge = [];
        $colors_collection = Product::active()
            ->where('colors', '!=', '[]')
            ->pluck('colors')
            ->unique()
            ->toArray();

        foreach ($colors_collection as $color_json) {
            $color_array = json_decode($color_json, true);
            $colors_in_shop_merge = array_merge($colors_in_shop_merge, $color_array);
        }
        $colors_in_shop = array_unique($colors_in_shop_merge);
        // Colors End
        $banner = \App\Model\BusinessSetting::where('type', 'banner_product_list_page')->whereJsonContains('value', ['status' => '1'])->first();

        if ($request->ajax()) {
            return response()->json([
                'total_product'=>$products->total(),
                'view' => view(VIEW_FILE_NAMES['products__ajax_partials'], compact('products','product_ids'))->render(),
            ], 200);
        }

        if ($request['data_from'] == 'brand') {
            $brand_data = Brand::active()->find((int)$request['id']);
            if(!$brand_data) {
                Toastr::warning(translate('not_found'));
                return redirect('/');
            }
        }

        return view(VIEW_FILE_NAMES['products_view_page'], compact('products','tag_category','tag_brand','product_ids','categories','colors_in_shop','banner'));
    }

    public function theme_all_purpose(Request $request)
    {
        $request['sort_by'] == null ? $request['sort_by'] == 'latest' : $request['sort_by'];

        $porduct_data = Product::active()->with(['reviews','rating']);

        $product_ids = [];
        if ($request['data_from'] == 'category') {
            $products = $porduct_data->get();
            $product_ids = [];
            foreach ($products as $product) {
                foreach (json_decode($product['category_ids'], true) as $category) {
                    if ($category['id'] == $request['id']) {
                        array_push($product_ids, $product['id']);
                    }
                }
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request->has('search_category_value') && $request['search_category_value'] != 'all') {
            $products = $porduct_data->get();
            $product_ids = [];
            foreach ($products as $product) {
                foreach (json_decode($product['category_ids'], true) as $category) {
                    if ($category['id'] == $request['search_category_value']) {
                        array_push($product_ids, $product['id']);
                    }
                }
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'brand') {
            $query = $porduct_data->where('brand_id', $request['id']);
        }

        if (!$request->has('data_from') || $request['data_from'] == 'latest') {
            $query = $porduct_data;
        }

        if ($request['data_from'] == 'top-rated') {
            $reviews = Review::select('product_id', DB::raw('AVG(rating) as count'))
                ->groupBy('product_id')
                ->orderBy("count", 'desc')->get();
            $product_ids = [];
            foreach ($reviews as $review) {
                array_push($product_ids, $review['product_id']);
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'best-selling') {
            $details = OrderDetail::with('product')
                ->select('product_id', DB::raw('COUNT(product_id) as count'))
                ->groupBy('product_id')
                ->orderBy("count", 'desc')
                ->get();
            $product_ids = [];
            foreach ($details as $detail) {
                array_push($product_ids, $detail['product_id']);
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'most-favorite') {
            $details = Wishlist::with('product')
                ->select('product_id', DB::raw('COUNT(product_id) as count'))
                ->groupBy('product_id')
                ->orderBy("count", 'desc')
                ->get();
            $product_ids = [];
            foreach ($details as $detail) {
                array_push($product_ids, $detail['product_id']);
            }
            $query = $porduct_data->whereIn('id', $product_ids);
        }

        if ($request['data_from'] == 'featured') {
            $query = Product::with(['reviews'])->active()->where('featured', 1);
        }

        if ($request['data_from'] == 'featured_deal') {
            $featured_deal_id = FlashDeal::where(['status'=>1])->where(['deal_type'=>'feature_deal'])->pluck('id')->first();
            $featured_deal_product_ids = FlashDealProduct::where('flash_deal_id',$featured_deal_id)->pluck('product_id')->toArray();
            $query = Product::with(['reviews'])->active()->whereIn('id', $featured_deal_product_ids);
        }

        if ($request['data_from'] == 'search') {
            $key = explode(' ', $request['name']);
            $product_ids = Product::where(function ($q) use ($key) {
                foreach ($key as $value) {
                    $q->orWhere('name', 'like', "%{$value}%")
                        ->orWhereHas('tags',function($query)use($value){
                            $query->where('tag', 'like', "%{$value}%");
                        });
                }
            })->pluck('id');

            if($product_ids->count()==0)
            {
                $product_ids = Translation::where('translationable_type', 'App\Model\Product')
                    ->where('key', 'name')
                    ->where(function ($q) use ($key) {
                        foreach ($key as $value) {
                            $q->orWhere('value', 'like', "%{$value}%");
                        }
                    })
                    ->pluck('translationable_id');
            }

            $query = $porduct_data->WhereIn('id', $product_ids);

        }

        if ($request['data_from'] == 'discounted') {
            $query = Product::with(['reviews'])->active()->where('discount', '!=', 0);
        }

        if ($request['sort_by'] == 'latest') {
            $fetched = $query->latest();
        } elseif ($request['sort_by'] == 'low-high') {
            $fetched = $query->orderBy('unit_price', 'ASC');
        } elseif ($request['sort_by'] == 'high-low') {
            $fetched = $query->orderBy('unit_price', 'DESC');
        } elseif ($request['sort_by'] == 'a-z') {
            $fetched = $query->orderBy('name', 'ASC');
        } elseif ($request['sort_by'] == 'z-a') {
            $fetched = $query->orderBy('name', 'DESC');
        } else {
            $fetched = $query->latest();
        }

        if ($request['min_price'] != null || $request['max_price'] != null) {
            $fetched = $fetched->whereBetween('unit_price', [Helpers::convert_currency_to_usd($request['min_price']), Helpers::convert_currency_to_usd($request['max_price'])]);
        }
        $common_query = $fetched;

        $rating_1 = 0;
        $rating_2 = 0;
        $rating_3 = 0;
        $rating_4 = 0;
        $rating_5 = 0;

        foreach($common_query->get() as $rating){
            if(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] >0 && $rating->rating[0]['average'] <2)){
                $rating_1 += 1;
            }elseif(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] >=2 && $rating->rating[0]['average'] <3)){
                $rating_2 += 1;
            }elseif(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] >=3 && $rating->rating[0]['average'] <4)){
                $rating_3 += 1;
            }elseif(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] >=4 && $rating->rating[0]['average'] <5)){
                $rating_4 += 1;
            }elseif(isset($rating->rating[0]['average']) && ($rating->rating[0]['average'] == 5)){
                $rating_5 += 1;
            }
        }
        $ratings = [
            'rating_1'=>$rating_1,
            'rating_2'=>$rating_2,
            'rating_3'=>$rating_3,
            'rating_4'=>$rating_4,
            'rating_5'=>$rating_5,
        ];
        $data = [
            'id' => $request['id'],
            'name' => $request['name'],
            'data_from' => $request['data_from'],
        ];
        $products_count = $common_query->count();
        $products = $common_query->paginate(4)->appends($data);
        $categories = Category::withCount(['product'=>function($query){
                        $query->where(['status'=>'1']);
                    }])->with(['childes' => function ($sub_query) {
                        $sub_query->with(['childes' => function ($sub_sub_query) {
                            $sub_sub_query->withCount(['sub_sub_category_product'])->where('position', 2);
                        }])->withCount(['sub_category_product'])->where('position', 1);
                    }, 'childes.childes'])
                    ->where('position', 0)->get();
        // Categories End
        // Colors Start
        $colors_in_shop_merge = [];
        $colors_collection = Product::active()
            ->where('colors', '!=', '[]')
            ->pluck('colors')
            ->unique()
            ->toArray();

        foreach ($colors_collection as $color_json) {
            $color_array = json_decode($color_json, true);
            $colors_in_shop_merge = array_merge($colors_in_shop_merge, $color_array);
        }
        $colors_in_shop = array_unique($colors_in_shop_merge);
        // Colors End
        $banner = \App\Model\BusinessSetting::where('type', 'banner_product_list_page')->whereJsonContains('value', ['status' => '1'])->first();

        if ($request->ajax()) {
            return response()->json([
                'total_product'=>$products->total(),
                'view' => view(VIEW_FILE_NAMES['products__ajax_partials'], compact('products','product_ids'))->render(),
            ], 200);
        }

        if ($request['data_from'] == 'brand') {
            $brand_data = Brand::active()->find((int)$request['id']);
            if(!$brand_data) {
                Toastr::warning(translate('not_found'));
                return redirect('/');
            }
        }
        return view(VIEW_FILE_NAMES['products_view_page'], compact('products','product_ids','products_count','categories','colors_in_shop','banner','ratings'));
    }
}
