<?php

namespace App\Http\Controllers\Admin;

use App\CPU\Helpers;
use App\CPU\ImageManager;
use App\Exports\BrandListExport;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Models\WhatsTrending;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Translation;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Model\Brand;
class WhatsTrendingController extends Controller
{
    public function add_new()
    {
        

        return view('admin-views.whats-trending.add-new');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
        ]);

        $brand = new WhatsTrending;
        $brand->title = $request->title;
        $brand->url = $request->url;
    
        $brand->status = 1;
        $brand->save();

       
        Toastr::success('Added Succesfully');
        return back();
    }

    /**
     * Brand list show, search
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    function list(Request $request)
    {
        $search      = $request['search'];
        $query_param = $search ? ['search' => $request['search']] : '';

        $br = WhatsTrending::
            
            when($request['search'], function ($q) use($request){
                $key = explode(' ', $request['search']);
                foreach ($key as $value) {
                    $q->Where('title', 'like', "%{$value}%")
                      ->orWhere('id', $value);
                }
            })
            ->latest()->paginate(Helpers::pagination_limit())->appends($query_param);

        return view('admin-views.whats-trending.list', compact('br','search'));
    }

    /**
     * Export brand list by excel
     * @return string|\Symfony\Component\HttpFoundation\StreamedResponse
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function export(Request $request){
        $search = $request['search'];
        $brands = Brand::withCount('brandAllProducts')
            ->with(['brandAllProducts'=> function($query){
                $query->withCount('order_details');
            }])
            ->when($search, function ($q) use($search){
                $key = explode(' ', $search);
                foreach ($key as $value) {
                    $q->Where('name', 'like', "%{$value}%")
                      ->orWhere('id', $value);
                }
            })->orderBy('id', 'DESC')->get();

        // $data = array();
        // foreach($brands as $brand){
        //     $data[] = array(
        //         'Brand Name'      => $brand->name,
        //         'Total Product'   => $brand->brand_all_products_count,
        //         'Total Order' => $brand->brandAllProducts->sum('order_details_count'),
        //     );
        // }
        // dd($brands);
        $active = $brands->where('status',1)->count();
        $inactive = $brands->where('status',0)->count();
        $data = [
            'brands'=>$brands,
            'search' =>$search ,
            'active' => $active,
            'inactive' => $inactive,
        ];
        return Excel::download(new BrandListExport($data), 'Brand-list.xlsx') ;
        // return (new FastExcel($data))->download('brand_list.xlsx');
    }

    public function edit($id)
    {
        $b = WhatsTrending::where(['id' => $id])->withoutGlobalScopes()->first();
        return view('admin-views.whats-trending.edit', compact('b'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
        ]);

        $brand = WhatsTrending::find($id);
      
        $brand->title = $request->title;
        $brand->url = $request->url;
    
       
        $brand->save();
       

        Toastr::success('Updated Succesfully');
        return back();
    }

    public function status_update(Request $request)
    {
        $brand = WhatsTrending::find($request['id']);
        $brand->status = $request['status'] ?? 0;

        if($brand->save()){
            $success = 1;
        }else{
            $success = 0;
        }
        return response()->json([
            'success' => $success,
        ], 200);
    }

    public function delete(Request $request)
    {
        
        $brand = WhatsTrending::find($request->id);
       
        $brand->delete();
        return response()->json();
    }
}
