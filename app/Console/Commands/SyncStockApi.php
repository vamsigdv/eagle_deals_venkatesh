<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Product;
use Illuminate\Support\Str;
class SyncStockApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fetch from api if there update otherwise create';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = $this->curlGet();
        
        if (!$response || !isset($response['Data'])) {
            $this->error('Stock API request failed or response does not contain data.');
            return;
        }
        $response_data= $response['Data'];
        
     
        foreach ($response_data as $stockData) {
          
            // Product::updateOrCreate([
            //     'item_code' => $stockData['ITEM_CODE'],
            // ], [
            //     'current_stock' => $stockData['SALEABLE_STOCK'],
            //     'name' => $stockData['ITEM_NAME'],
            //     'slug' => Str::slug($stockData['ITEM_NAME'], '-') . '-' . Str::random(6),
            //     'added_by' => 'admin'  // Set "added_by" only for newly created products
            // ]);
            $product = Product::firstOrNew(['item_code' => $stockData['ITEM_CODE']]);

            $product->current_stock = $stockData['SALEABLE_STOCK'];
            

            // Set "added_by" only for newly created products
            if (!$product->exists) {
                $product->added_by = 'admin';
                $product->name = $stockData['ITEM_NAME'];
                $product->slug = Str::slug($stockData['ITEM_NAME'], '-') . '-' . Str::random(6);
                $product->status = '0';
            }

            $product->save();

        }

        $this->info('stock api synchronised successfully.');
    }

    public function curlGet()
    {   
        $url ="http://183.82.242.33:85/api/apxapi/GetStockInfo?CompanyCode=EE&ItemCode=0&BranchCode=0&AsonDate=0&Brand=0&BranchCategory=0&BranchPinCode=0&ConsolidateStock=False&GroupBy=NONE&Product=0&ItemClassificationType=NONE&ItemClassificationValue=0";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'UserId: Website',
            'SecurityCode: 7247-1338-1885-5393',
        ]);

        $response = curl_exec($ch);

        curl_close($ch);

        return json_decode($response, true);
    }
    
}
