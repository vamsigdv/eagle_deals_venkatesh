<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\CurlRequestTrait;
use App\Model\Order;

class UpdateOrders extends Command
{
    use CurlRequestTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the order status from Shiprocket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::all();
        $token = $this->getToken()['token'];
        $url = "courier/track/shipment/";

        foreach ($orders as $order) {
            if ($order->shipment_id) {
                $url .= "{$order->shipment_id}";
                $result = $this->curlShipGet($url, $token);
                Order::where('shipment_id', $order->shipment_id)->update(['order_status' => $result['status']]);
            }
        }
    }
}
