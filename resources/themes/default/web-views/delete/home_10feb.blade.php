
           @include('layouts.ui.header')
        <span id="authentication-status" data-auth="false"></span>
        <div class="row">
            <div class="col-12" style="margin-top: 10rem; position: fixed; z-index: 9999;">
                <div id="loading" style="display: none;">
                    <center><img width="200" src="#" onerror='this.src="{{ asset("public/deals/assets/front-end/img/loader.gif") }}"' /></center>

                </div>
            </div>
        </div>
        <!-- <b>main</b>
        {{$main_banner}}
        </br>
        <b>main sectiont</b>
        {{$main_section_banner}}
        </br>
        <b>footer </b>
        {{$footer_banner}} -->
      
        <secction class="banner-home">
            <div class="container">
                <div class="banner">
                    <img src=" {{asset('storage/app/public/banner')}}/{{$main_banner[0]->photo}}" alt="Los Angeles" />
                </div>
            </div>
       </secction>
     
        <section class="flash-dealing">
            <div class="container">
           

                @if ($flash_deals->count() > 0)
                    <div class="row flash-dealing-bg">
                        <div class="col-sm-4">
                            <div class="flash-dealing-left">
                                <h1>Flash Deal</h1>
                                <p>Hurry Up! The offer is limited. Grab while it lasts</p>
                                <div id="time_wrapper">
                                    <ul id="time_input">
                                        <li for="hours"><span class="value-count">{{$flash_deals->days}}</span><span class="label lbl-hrs">Days</span><span class="colum">:</span></li>
                                        <li for="minutes"><span class="value-count">{{$flash_deals->hours}}</span><span class="label lbl-min">Hours</span><span class="colum">:</span></li>
                                        <li for="seconds"><span class="value-count">{{$flash_deals->minutes}}</span><span class="label lbl-sec">Minutes</span><span class="colum">:</span></li>
                                        <li for="seconds"><span class="value-count">{{$flash_deals->seconds}}</span><span class="label lbl-sec">Seconds</span></li>
                                        <div class="w3-light-grey"><div class="w3-grey" style="height: 4px; width: 75%;"></div></div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="flash-dealing-right">
                                <div class="view-all">
                                    <a class="text-capitalize view-all-text" style="color: #ec1c24 !important;" href="products?search_by_filter_value=featured_deal&page=1">View all<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                </div>
                                <div class="row">
                                    <!-- @foreach($flash_deals->products as $flash_deal_product)
                                   
                                    <div class="col-sm-3">
                                        <div class="flash-deal-list">
                                            <a href="#"><img src="{{asset('public/deals/assets/images/deal-off.jpg')}}" alt="deals" /></a><span class="dicount-perstg">10% Off</span>
                                            <span class="favrate-icon">
                                                <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                            </span>
                                            <div class="deal-info">
                                                <div class="star-rating" style="margin-right: 10px;">
                                                    <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                    <i class="tio-star-outlined text-warning"></i>
                                                </div>
                                                <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                <h2><a href="#">{{$flash_deal_product->product->name}}</a></h2>
                                              
                                                <h3><span class="mrp-price">₹500</span> <span class="disc-price"> ₹450</span></h3>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach -->
                                    @foreach($flash_deals->products as $flash_deal_product)
                                        <div class="col-sm-3">
                                            <div class="flash-deal-list">
                                                <a href="#"><img src="{{ asset('storage/app/public/product/thumbnail') }}/{{ $flash_deal_product->product->thumbnail }}" alt="deals" /></a>
                                                <span class="dicount-perstg">{{ number_format((($flash_deal_product->product->purchase_price - $flash_deal_product->product->unit_price)/($flash_deal_product->product->unit_price)) * 100, 0) }}% Off</span>
                                                <span class="favrate-icon">
                                                    <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                </span>
                                                <div class="deal-info">
                                              
                                                    <div class="star-rating" style="margin-right: 10px;">
                                                        <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                        <i class="tio-star-outlined text-warning"></i>
                                                    </div>
                                                    <h2><a href="#">{{ $flash_deal_product->product->name }}</a></h2>
                                                    
                                                    <!-- Check if purchase_price and unit_price are available -->
                                                    @if (isset($flash_deal_product->product->purchase_price) && isset($flash_deal_product->product->unit_price))
                                                        <h3>
                                                            <span class="mrp-price">₹{{ number_format($flash_deal_product->product->unit_price, 2) }}</span>
                                                            <span class="disc-price"> ₹{{ number_format($flash_deal_product->product->purchase_price - $flash_deal_product->discount, 2) }}</span>
                                                        </h3>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </section>
        <section class="new-arrival-section featured_products">
            <div class="container rtl mt-4">
                <div class="card-body">
                    <div class="section-header">
                        <div class="arrival-title d-block">
                            <div class="text-capitalize"><h1>Featured Products</h1></div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="categories-title m-0"></div>
                            <div>
                                <a class="text-capitalize view-all-text" style="color: #ec1c24 !important;" href="products?search_by_filter_value=featured&page=1">View all<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="py-2">
                        <div class="new_arrival_product">
                            <div class="carousel-wrap">
                                <div class="new-arrivals-product_my">
                                    <ul class="my_new-arrivals-product">
                                        @if ($featured_products->count() > 0)
                                            @foreach($featured_products as $featured_product)
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src=" {{asset('storage/app/public/product/thumbnail')}}/{{$featured_product->thumbnail}}" alt="deals" /></a><span class="dicount-perstg">{{number_format((($featured_product->purchase_price-$featured_product->unit_price)/($featured_product->unit_price))*100, 0)}}% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">{{$featured_product->name}}</a></h2>
                                                            <h3><span class="mrp-price">₹{{$featured_product->unit_price}}</span><span class="disc-price"> ₹{{$featured_product->purchase_price}}</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <br />
        <div class="__inline-61">
            <section class="pb-4 rtl">
                <div class="container">
                    <div>
                        <div class="card __shadow h-100 max-md-shadow-0">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <div class="categories-title m-0"><span class="font-semibold">Categories</span></div>
                                    <div>
                                        <a class="text-capitalize view-all-text" href="categories" style="color: #ec1c24 !important;"  >View all<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                    </div>
                                </div>
                                <div class="d-none d-md-block">
                                    <div class="row mt-3">
                                        @if ($home_categories->count() > 0)
                                            @foreach($home_categories as $category)
                                                <div class="text-center __m-5px __cate-item">
                                                    <a href="{{route('products',['id'=> $category['id'],'data_from'=>'category','page'=>1])}}">
                                                        <div class="__img">
                                                            <img 
                                                                onerror='this.src="{{ asset("public/deals/assets/front-end/img/image-place-holder.png") }}"' 
                                                                src="{{asset('storage/app/public/category')}}/{{$category->icon}}" 
                                                                alt="{{$category->name}}" 
                                                            />
                                                        </div>
                                                        <p class="text-center small mt-2">{{$category->name}}</p>
                                                    </a>
                                                </div>
                                            @endforeach
                                        @endif
                                      
                                        
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <br />
    
        <section class="promotion">
            <div class="container">
                <div class="promo-img">
                    <img src=" {{asset('storage/app/public/banner')}}/{{$main_section_banner->photo}}" alt="Los Angeles" />    
               
                </div>
            </div>
        </section>
        <br />
        <section class="__inline-61">
            <div class="container rtl">
                <div class="row g-4 pt-2 mt-0 pb-2 __deal-of align-items-start">
                    <div class="col-xl-3 col-md-4">
                        <div class="deal_of_the_day h-100 bg--light">
                            <div class="d-flex justify-content-center align-items-center py-4"><h4 class="font-bold m-0 align-items-center text-uppercase text-center px-2" style="color: #1455ac;">Deal of the Day</h4></div>
                            @if ($deal_of_the_day->product->count() > 0)
                                <div class="recomanded-product-card mt-0">
                                    <div class="d-flex justify-content-center align-items-center __pt-20 __m-20-r">
                                        <div class="position-relative">
                                            <img src=" {{asset('storage/app/public/product/thumbnail')}}/{{$product->thumbnail}}" alt=""/>
                                                               
                                            <span class="dicount-perstg">{{number_format((($product->purchase_price-$product->unit_price)/($product->unit_price))*100, 0)}}% Off</span>
                                            <span class="favrate-icon">
                                                <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="__i-1 bg-transparent text-center mb-0">
                                        <div class="__p-20px px-0 pb-0">
                                            <h6 class="font-semibold pt-2">{{$deal_of_the_day->product->name}}</h6>
                                            <div class="mb-4 pt-1 d-flex flex-wrap justify-content-center align-items-center text-center gap-8">
                                                <span class="mrp-price">₹{{$deal_of_the_day->product->unit_price}}</span>
                                                <span class="text-accent __text-22px text-dark">₹{{$deal_of_the_day->product->purchase_price}}</span>
                                            </div>
                                            <button class="btn btn--primary font-bold px-4 rounded-10 text-uppercase" onclick='location.href="#"'>Buy now</button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-xl-9 col-md-8">
                        <div class="latest-product-margin">
                            <div class="d-flex justify-content-between mb-14px">
                                <div class="text-center"><span class="for-feature-title __text-22px font-bold text-center">Latest Products</span></div>
                                <div class="mr-1">
                                    <a class="text-capitalize view-all-text" style="color: #ec1c24 !important;" href="products?sort_by=latest&page=1">View all<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                </div>
                            </div>
                            <div class="row mt-0 g-2">
                                @if ($latest_products->count() > 0)
                                    @foreach($latest_products as $latest)
                                        <div class="col-xl-3 col-sm-4 col-md-6 col-lg-4 col-6">
                                            <div>
                                                <div class="product-single-hover style--card">
                                                    <div class="overflow-hidden position-relative">
                                                        <div class="inline_product clickable d-flex justify-content-center">
                                                            <div class="d-flex justify-content-end for-dicount-div-null"><span class="for-discoutn-value-null"></span></div>
                                                            <div class="p-10px pb-0">
                                                                <a href="#" class="w-100">
                                                                    <img src=" {{asset('storage/app/public/product/thumbnail')}}/{{$latest->thumbnail}}" alt=""/>
                                                                </a>
                                                                <span class="dicount-perstg">{{number_format((($latest->purchase_price-$latest->unit_price)/($latest->unit_price))*100, 0)}}% Off</span>
                                                                <span class="favrate-icon">
                                                                    <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                                </span>
                                                            </div>
                                                            <div class="quick-view">
                                                                <!-- <a class="btn-circle stopPropagation" href="javascript:" onclick='quickView("1")'><i class="czi-eye align-middle"></i></a> -->
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="single-product-details">
                                                            <div class="text-center"><a href="#">{{$latest->name}}</a></div>
                                                            <div class="justify-content-between text-center">
                                                                <div class="product-price text-center d-flex flex-wrap justify-content-center align-items-center gap-8"><span class="mrp-price">₹{{$latest->unit_price}}</span><span class="disc-price"> ₹{{$latest->purchase_price}}</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                
                               
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <br />
        <!--Start-->
        <div class="__inline-61">
            <section class="new-arrival-section">
                <div class="container">
                    <div class="arrivals-main">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="arrivals-left">
                                    <img src="{{asset('public/deals/assets/images/power-light.jpg')}}" alt="">
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="rtl">
                                    <div class="section-header gopi">
                                        <div class="text-black font-bold __text-22px"><span>New arrivals</span></div>
                                        <div class="__mr-2px">
                                            <a class="text-capitalize view-all-text" href="products?sort_by=latest" style="color: #ec1c24 !important;">View all<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="rtl mb-3 overflow-hidden">
                                    <div class="py-2">
                                        <div class="new_arrival_product">
                                            <div class="carousel-wrap">
                                                <div>
                                                    <ul class="my_new-arrivals-product gopi-basam">
                                                        @if ($new_arrival_products->count() > 0)
                                                            @foreach($new_arrival_products as $latest)
                                                                <li>
                                                                    <div class="flash-deal-list">
                                                                        <a href="#"><img src="{{asset('storage/app/public/product/thumbnail')}}/{{$latest->thumbnail}}" alt="deals" /></a><span class="dicount-perstg">{{number_format((($latest->purchase_price-$latest->unit_price)/($latest->unit_price))*100, 0)}}% Off</span>
                                                                        <span class="favrate-icon">
                                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                                        </span>
                                                                        <div class="deal-info">
                                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                                <i class="tio-star-outlined text-warning"></i>
                                                                            </div>
                                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                                            <h2><a href="#">{{$latest->name}}</a></h2>
                                                                            <h3><span class="mrp-price">₹{{$latest->unit_price}}</span><span class="disc-price"> ₹{{$latest->purchase_price}}</span></h3>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        @endif
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!--End-->
        <br/>
        <!-- <section class="promotion">
            <div class="container">
                <div class="promo-img"><img src="{{asset('public/deals/assets/images/promo.jpg')}}" alt="Promoation" /></div>
            </div>
        </section> -->
        <section class="banner-home">
            <div class="container">
                <div class="banner">
                    <img src=" {{asset('storage/app/public/banner')}}/{{$main_banner[0]->photo}}" alt="Los Angeles" />
                </div>
            </div>
       </section>
        <br/>
        <!--Start-->
            
            @foreach($home_categories as $category)
        
                <div class="__inline-61">
                    <section class="new-arrival-section">
                        <div class="container">
                            <div class="arrivals-main">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="arrivals-left">
                                            <img src="{{asset('storage/app/public/category')}}/{{$category->icon}}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="rtl">
                                            <div class="section-header gopi">
                                                <div class="text-black font-bold __text-22px"><span>{{$category->name}}</span></div>
                                                <div class="__mr-2px">
                                                    <a class="text-capitalize view-all-text" href="products?category_id={{$category->id}}" style="color: #ec1c24 !important;">View all<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rtl mb-3 overflow-hidden">
                                            <div class="py-2">
                                                <div class="new_arrival_product">
                                                    <div class="carousel-wrap">
                                                        <ul class="my_new-arrivals-product gopi-basam">
                                                            @foreach($category->products as $product)    
                                                        
                                                            
                                                                <li>
                                                                    <div class="flash-deal-list">
                                                                        <a href="#"><img src="{{asset('storage/app/public/product/thumbnail')}}/{{$product->thumbnail}}" alt="deals" /></a>
                                                                        
                                                                        <span class="dicount-perstg">{{ number_format((($product->purchase_price - $product->unit_price)/($product->unit_price)) * 100, 0) }}% Off</span>
                                                
                                                                        <span class="favrate-icon">
                                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                                        </span>
                                                                        <div class="deal-info">
                                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                                <i class="tio-star-outlined text-warning"></i>
                                                                            </div>
                                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                                            <h2><a href="#">{{$product->name}}</a></h2>
                                                                            <h3>
                                                                                <span class="mrp-price">₹{{$product->unit_price}}</span>
                                                                                <span class="disc-price"> ₹{{$product->purchase_price}}</span>
                                                                            </h3>
                                                       
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <br/>
            @endforeach
      
            <section class="promotion">
            <div class="container">
                <div class="promo-img">
                    <img src=" {{asset('storage/app/public/banner')}}/{{$footer_banner[0]->photo}}" alt="Los Angeles" />    
               
                </div>
            </div>
        </section>
        
        <br/>
        
      
        <div class="__inline-61">
            <section class="new-arrival-section">
                <div class="container">
                    <div class="arrivals-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="rtl">
                                    <div class="section-header gopi">
                                        <div class="text-black font-bold __text-22px"><span>What's Trending</span></div>
                                        <div class="__mr-2px">
                                            <a class="text-capitalize view-all-text" href="#" style="color: #ec1c24 !important;">View all<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="col-sm-3">
                                <div class="trending">
                                    <a href="#">
                                        <img src="{{asset('public/deals/assets/images/wt1.jpg')}}" alt="">
                                        <p>Its the best laptop if you are a 3d artist video editor or a programmer..</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="trending">
                                    <a href="#">
                                        <img src="{{asset('public/deals/assets/images/wt2.jpg')}}" alt="">
                                        <p>Its the best laptop if you are a 3d artist video editor or a programmer..</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="trending">
                                    <a href="#">
                                        <img src="{{asset('public/deals/assets/images/wt3.jpg')}}" alt="">
                                        <p>Its the best laptop if you are a 3d artist video editor or a programmer..</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="trending">
                                    <a href="#">
                                        <img src="{{asset('public/deals/assets/images/wt4.jpg')}}" alt="">
                                        <p>Its the best laptop if you are a 3d artist video editor or a programmer..</p>
                                    </a>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!--End-->
 

        @if($web_config['brand_setting'] && $brands->count() > 0)
            <div class="__inline-61"></div>
                <section class="container rtl pt-4">
                    <div class="section-header gopi">
                        <div class="text-black font-bold __text-22px"><span>{{translate('brands')}}</span></div>
                        <div class="__mr-2px">
                            <a class="text-capitalize view-all-text" href="#" style="color: #ec1c24 !important;">{{ translate('view_all')}}<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                        </div>
                    </div>
                    <div class="row as-gopi">
                        
                        @foreach($brands as $brand)
                            <div class="col-sm-1 text-center">
                                <a href="products?brand={{$brand->id}}&page=1" class="__brand-item">
                                    <img 
                                        onerror='this.src="{{ asset("public/deals/assets/front-end/img/image-place-holder.png") }}"' 
                                        src="{{asset('storage/app/public/brand')}}/{{$brand->image}}" 
                                        alt="{{$brand->name}}" 
                                    />
                                </a>
                            </div>
                        @endforeach
                    </div>
                </section>
            </div>
        @endif
        <br/>
        <!--Start-->
        @include('layouts.ui.social_media')
        <!--End-->

        <br/>
        @include('layouts.ui.delivery')
        @include('layouts.ui.footer') 
        
        <div class="modal fade" id="remove-wishlist-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/remove-wishlist.png')}}" alt="" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220">Product has been removed from wishlist</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="outof-stock-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/out-of-stock.png')}}" alt="" class="mw-100px" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220" id="outof-stock-modal-message">Out of stock</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="add-wishlist-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/added-wishlist.png')}}" alt="" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220">Product added to wishlist</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="login-alert-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/locked-icon.svg')}}" alt="" />
                            <h6 class="font-semibold mt-3 mb-1">Please Login</h6>
                            <p class="mb-4"><small>You need to login to view this feature</small></p>
                        </div>
                        <div class="d-flex gap-3 justify-content-center">
                            <button class="btn btn-soft-secondary bg--secondary __rounded-10" data-dismiss="modal">Cancel</button><a href="#" class="btn btn-primary __rounded-10">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="remove-address">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/remove-address.png')}}" alt="" />
                            <h6 class="font-semibold mt-3 mb-1">Delete this address?</h6>
                            <p class="mb-4"><small>This address will be removed from this list</small></p>
                        </div>
                        <div class="d-flex gap-3 justify-content-center">
                            <a href="javascript:" class="btn btn-primary __rounded-10" id="remove-address-link">Remove</a><button class="btn btn-soft-secondary bg--secondary __rounded-10" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn-scroll-top btn--primary" href="#top" data-scroll><span class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Top</span><i class="btn-scroll-top-icon czi-arrow-up"></i></a>
        <script src="{{asset('public/deals/assets/vendor/jquery/dist/jquery-2.2.4.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/bs-custom-file-input/dist/bs-custom-file-input.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/simplebar/dist/simplebar.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/tiny-slider/dist/min/tiny-slider.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/public/js/lightbox.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/drift-zoom/dist/Drift.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/lightgallery.js/dist/js/lightgallery.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/lg-video.js/dist/lg-video.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/back-end/js/toastr.js')}}"></script>
        <script src="{{asset('public/deals/assets/front-end/js/theme.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/front-end/js/custom.js')}}"></script>
        <script src="{{asset('public/deals/assets/front-end/js/slick.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/front-end/js/sweet_alert.js')}}"></script>
        <script src="{{asset('public/deals/assets/back-end/js/toastr.js')}}"></script>
        <script type="text/javascript"></script>
        
<script>
    ₹(document).ready(function() {
        const ₹stickyElement = ₹('.bottom-sticky_ele');
        const ₹offsetElement = ₹('.bottom-sticky_offset');

        if(₹stickyElement.length !== 0){
            ₹(window).on('scroll', function() {
                const elementOffset = ₹offsetElement.offset().top - (₹(window).height()/1.2);
                const scrollTop = ₹(window).scrollTop();

                if (scrollTop >= elementOffset) {
                    ₹stickyElement.addClass('stick');
                } else {
                    ₹stickyElement.removeClass('stick');
                }
            });
        }
    });
</script>
<script>
    ₹(document).ready(function () {
        ₹('.password-toggle-btn').on('click',function(){
            let checkbox = ₹(this).find('input[type=checkbox]');
            let eyeIcon = ₹(this).find('i');
            checkbox.change(function () {
                if (checkbox.is(':checked')) {
                    eyeIcon.removeClass('tio-hidden').addClass('tio-invisible');
                } else {
                    eyeIcon.removeClass('tio-invisible').addClass('tio-hidden');
                }
            });
        })

    });
</script>

<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>

<script>
    function addWishlist(product_id, modalId) {
        ₹.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': ₹('meta[name="_token"]').attr('content')
            }
        });
        ₹.ajax({
            url: "{{config('app.url')}}/store-wishlist",
            method: 'POST',
            data: {
                product_id: product_id
            },
            success: function (data) {
                if (data.value == 1) {
                    ₹('.countWishlist').html(data.count);
                    ₹('.countWishlist-' + product_id).text(data.product_count);
                    ₹('.tooltip').html('');
                    ₹(`.wishlist_icon_₹{product_id}`).removeClass('fa fa-heart-o').addClass('fa fa-heart');
                    ₹('#add-wishlist-modal').modal('show');
                    ₹(`#₹{modalId}`).modal('show');
                } else if (data.value == 2) {
                    ₹('#remove-wishlist-modal').modal('show');
                    ₹('.countWishlist').html(data.count);
                    ₹('.countWishlist-' + product_id).text(data.product_count);
                    ₹(`.wishlist_icon_₹{product_id}`).removeClass('fa fa-heart').addClass('fa fa-heart-o');
                } else {
                    ₹('#login-alert-modal').modal('show');
                }
            }
        });
    }

    function removeWishlist(product_id, modalId) {
        ₹.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': ₹('meta[name="_token"]').attr('content')
            }
        });
        ₹.ajax({
            url: "{{config('app.url')}}/delete-wishlist",
            method: 'POST',
            data: {
                id: product_id
            },
            beforeSend: function () {
                ₹('#loading').show();
            },
            success: function (data) {
                ₹(`#₹{modalId}`).modal('show');

                ₹('.countWishlist').html(parseInt(₹('.countWishlist').html())-1);
                ₹('#row_id'+product_id).hide();
                ₹('.tooltip').html('');
                if (parseInt(₹('.countWishlist').html()) % 15 === 0) {
                    if (₹('#wishlist_paginated_page').val() == 1) {
                        ₹("#set-wish-list").empty().append(`
                            <center>
                                <h6 class="text-muted">
                                    No data found.
                                </h6>
                            </center>
                        `);
                    } else {
                        let page_value =₹('#wishlist_paginated_page').val();
                        window.location.href = '{{config('app.url')}}/wishlists?page=' + (page_value - 1);
                    }
                }
            },
            complete: function () {
                ₹('#loading').hide();
            },
        });


    }

    function quickView(product_id) {
        ₹.get({
            url: '{{config('app.url')}}/quick-view',
            dataType: 'json',
            data: {
                product_id: product_id
            },
            beforeSend: function () {
                ₹('#loading').show();
            },
            success: function (data) {
                console.log("success...")
                ₹('#quick-view').modal('show');
                ₹('#quick-view-modal').empty().html(data.view);
            },
            complete: function () {
                ₹('#loading').hide();
            },
        });
    }

    function addToCart(form_id = 'add-to-cart-form', redirect_to_checkout=false) {
        if (checkAddToCartValidity()) {
            ₹.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': ₹('meta[name="_token"]').attr('content')
                }
            });
            ₹.post({
                url: '{{config('app.url')}}/cart/add',
                data: ₹('#' + form_id).serializeArray(),
                beforeSend: function () {
                    ₹('#loading').show();
                },
                success: function (response) {
                    console.log(response);
                    if (response.status == 1) {
                        updateNavCart();
                        toastr.success(response.message, {
                            CloseButton: true,
                            ProgressBar: true
                        });
                        ₹('.call-when-done').click();
                        if(redirect_to_checkout)
                        {
                            location.href = "index.html')}}";
                        }
                        return false;
                    } else if (response.status == 0) {
                        ₹('#outof-stock-modal-message').html(response.message);
                        ₹('#outof-stock-modal').modal('show');
                        return false;
                    }
                },
                complete: function () {
                    ₹('#loading').hide();

                }
            });
        } else {
            Swal.fire({
                type: 'info',
                title: 'Cart',
                text: 'Please choose all the options'
            });
        }
    }

    function buy_now() {
        addToCart('add-to-cart-form',true);
        /* location.href = "{{config('app.url')}}/checkout-details"; */
    }

    function currency_change(currency_code) {
        ₹.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': ₹('meta[name="_token"]').attr('content')
            }
        });
        ₹.ajax({
            type: 'POST',
            url: '{{config('app.url')}}/currency',
            data: {
                currency_code: currency_code
            },
            success: function (data) {
                toastr.success('Currency changed to' + data.name);
                location.reload();
            }
        });
    }

    function removeFromCart(key) {
        ₹.post('cart/remove.html', {_token: 'OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO', key: key}, function (response) {
            ₹('#cod-for-cart').hide();
            updateNavCart();
            ₹('#cart-summary').empty().html(response.data);
            toastr.info('Item has been removed from cart', {
                CloseButton: true,
                ProgressBar: true
            });
            let segment_array = window.location.pathname.split('index.html');
            let segment = segment_array[segment_array.length - 1];
            if(segment === 'checkout-payment' || segment === 'checkout-details'){
                location.reload();
            }
        });
    }

    function updateNavCart() {
        ₹.post('cart/nav-cart-items.html', {_token: 'OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO'}, function (response) {
            ₹('#cart_items').html(response.data);
        });
    }
    /*new*/
    ₹("#add-to-cart-form").on("submit", function (e) {
        e.preventDefault();
    });

    /*new*/
    function cartQuantityInitialize() {
        ₹('.btn-number').click(function (e) {
            e.preventDefault();

                fieldName = ₹(this).attr('data-field');
                type = ₹(this).attr('data-type');
                productType = ₹(this).attr('product-type');
                var input = ₹("input[name='" + fieldName + "']");
                var currentVal = parseInt(₹('.input-number').val());
                // alert(currentVal);
            if (!isNaN(currentVal)) {
                    // console.log(productType)
                    if (type == 'minus') {

                        if (currentVal > ₹('.input-number').attr('min')) {
                        ₹('.input-number').val(currentVal - 1).change();
                    }
                        if (parseInt(₹('.input-number').val()) == ₹('.input-number').attr('min')) {
                            ₹(this).attr('disabled', true);
                    }

                    } else if (type == 'plus') {
                        // alert('ok out of stock');
                        if (currentVal < ₹('.input-number').attr('max') || (productType === 'digital')) {
                        ₹('.input-number').val(currentVal + 1).change();
                    }

                        if ((parseInt(input.val()) == ₹('.input-number').attr('max')) && (productType === 'physical')) {
                            ₹(this).attr('disabled', true);
                    }

                }
            } else {
                ₹('.input-number').val(0);
            }
        });

            ₹('.input-number').focusin(function () {
                ₹(this).data('oldValue', ₹(this).val());
        });

            ₹('.input-number').change(function () {
                productType = ₹(this).attr('product-type');
                minValue = parseInt(₹(this).attr('min'));
                maxValue = parseInt(₹(this).attr('max'));
                valueCurrent = parseInt(₹(this).val());
                var name = ₹(this).attr('name');
                if (valueCurrent >= minValue) {
                        ₹(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
                } else {
                    Swal.fire({
                            icon: 'error',
                            title: 'Cart',
                            text: 'Sorry the minimum order quantity does not match'
                    });
                    ₹(this).val(₹(this).data('oldValue'));
                }
                if (productType === 'digital' || valueCurrent <= maxValue) {
                    ₹(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
                } else {
                    Swal.fire({
                            icon: 'error',
                            title: 'Cart',
                            text: 'Sorry stock limit exceeded.'
                    });
                    ₹(this).val(₹(this).data('oldValue'));
                }


        });
        ₹(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if (
                ₹.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)
            ) {
                // let it happen, don't do anything
                 return;
            }
            // Ensure that it is a number and stop the keypress
            if (
                (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) &&
                (e.keyCode < 96 || e.keyCode > 105)
            ) {
                e.preventDefault();
            }
        });
    }

    function updateQuantity(key, element) {
        ₹.post('cart/updateQuantity.html', {
            _token: 'OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO',
            key: key,
            quantity: element.value
        }, function (data) {
            updateNavCart();
            ₹('#cart-summary').empty().html(data);
        });
    }



    function updateCartQuantity(cart_id, product_id, action, event) {
        let remove_url = ₹("#remove_from_cart_url").data("url");
        let update_quantity_url = ₹("#update_quantity_url").data("url");
        let token = ₹('meta[name="_token"]').attr("content");
        let product_qyt =
            parseInt(₹(`.cartQuantity₹{cart_id}`).val()) + parseInt(action);
        let cart_quantity_of = ₹(`.cartQuantity₹{cart_id}`);
        let segment_array = window.location.pathname.split("index.html')}}");
        let segment = segment_array[segment_array.length - 1];

        if (cart_quantity_of.val() == 0) {
            toastr.info(₹('.cannot_use_zero').data('text'), {
                CloseButton: true,
                ProgressBar: true,
            });
            cart_quantity_of.val(cart_quantity_of.data("min"));
        }else if (
            (cart_quantity_of.val() == cart_quantity_of.data("min") &&
                event == "minus")
        ) {
            ₹.post(
                remove_url,
                {
                    _token: token,
                    key: cart_id,
                },
                function (response) {
                    updateNavCart();
                    toastr.info(response.message, {
                        CloseButton: true,
                        ProgressBar: true,
                    });
                    if (
                        segment === "shop-cart" ||
                        segment === "checkout-payment" ||
                        segment === "checkout-details"
                    ) {
                        location.reload();
                    }
                }
            );
        } else {
            if(cart_quantity_of.val() < cart_quantity_of.data("min")){
                let min_value = cart_quantity_of.data("min");
                toastr.error('Minimum order quantity cannot be less than '+min_value);
                cart_quantity_of.val(min_value)
                updateCartQuantity(cart_id, product_id, action, event)
            }else{
                ₹(`.cartQuantity₹{cart_id}`).html(product_qyt);
                ₹.post(
                    update_quantity_url,
                    {
                        _token: token,
                        key: cart_id,
                        product_id: product_id,
                        quantity: product_qyt,
                    },
                    function (response) {
                        if (response["status"] == 0) {
                            toastr.error(response["message"]);
                        } else {
                            toastr.success(response["message"]);
                        }
                        response["qty"] <= 1
                            ? ₹(`.quantity__minus₹{cart_id}`).html(
                                '<i class="tio-delete-outlined text-danger fs-10"></i>'
                            )
                            : ₹(`.quantity__minus₹{cart_id}`).html(
                                '<i class="tio-remove fs-10"></i>'
                            );

                        ₹(`.cartQuantity₹{cart_id}`).val(response["qty"]);
                        ₹(`.cartQuantity₹{cart_id}`).html(response["qty"]);
                        ₹(`.cart_quantity_multiply₹{cart_id}`).html(response["qty"]);
                        ₹(".cart_total_amount").html(response.total_price);
                        ₹(`.discount_price_of_₹{cart_id}`).html(
                            response["discount_price"]
                        );
                        ₹(`.quantity_price_of_₹{cart_id}`).html(
                            response["quantity_price"]
                        );
                        ₹(`.total_discount`).html(
                            response["total_discount_price"]
                        );
                        ₹(`.free_delivery_amount_need`).html(
                            response.free_delivery_status.amount_need
                        );
                        if(response.free_delivery_status.amount_need <=0){
                            ₹('.amount_fullfill').removeClass('d-none');
                            ₹('.amount_need_to_fullfill').addClass('d-none');
                        }else{
                            ₹('.amount_fullfill').addClass('d-none');
                            ₹('.amount_need_to_fullfill').removeClass('d-none');
                        }
                        const progressBar = document.querySelector('.progress-bar');
                        progressBar.style.width = response.free_delivery_status.percentage + '%';
                        if (response["qty"] == cart_quantity_of.data("min")) {
                            cart_quantity_of
                                .parent()
                                .find(".quantity__minus")
                                .html(
                                    '<i class="tio-delete-outlined text-danger fs-10"></i>'
                                );
                        } else {
                            cart_quantity_of
                                .parent()
                                .find(".quantity__minus")
                                .html('<i class="tio-remove fs-10"></i>');
                        }
                        if (
                            segment === "shop-cart" ||
                            segment === "checkout-payment" ||
                            segment === "checkout-details"
                        ) {
                            location.reload();
                        }
                    }
                );
            }
        }
    }
    ₹('#add-to-cart-form input').on('change', function () {
        getVariantPrice();
    });

    function getVariantPrice() {
        if (₹('#add-to-cart-form input[name=quantity]').val() > 0 && checkAddToCartValidity()) {
            ₹.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': ₹('meta[name="_token"]').attr('content')
                }
            });
            ₹.ajax({
                type: "POST",
                url: '{{config('app.url')}}/cart/variant_price',
                data: ₹('#add-to-cart-form').serializeArray(),
                success: function (data) {
                    ₹('#add-to-cart-form #chosen_price_div').removeClass('d-none');
                    ₹('#add-to-cart-form #chosen_price_div #chosen_price').html(data.price);
                    ₹('#chosen_price_mobile').html(data.price);
                    ₹('#set-tax-amount-mobile').html(data.tax);
                    ₹('#set-tax-amount').html(data.tax);
                    ₹('#set-discount-amount').html(data.discount);
                    ₹('#available-quantity').html(data.quantity);
                    ₹('.cart-qty-field').attr('max', data.quantity);
                }
            });
        }
    }

    function checkAddToCartValidity() {
        var names = {};
        ₹('#add-to-cart-form input:radio').each(function () { // find unique names
            names[₹(this).attr('name')] = true;
        });
        var count = 0;
        ₹.each(names, function () { // then count them
            count++;
        });
        if (₹('input:radio:checked').length == count) {
            return true;
        }
        return false;
    }

        ₹(document).ready(function () {
        ₹('#popup-modal').appendTo("body").modal('show');
    });
        
    ₹(".clickable").click(function () {
        window.location = ₹(this).find("a").attr("href");
        return false;
    });
</script>


<script>
    function couponCode() {
        ₹.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': ₹('meta[name="_token"]').attr('content')
            }
        });
        ₹.ajax({
            type: "POST",
            url: '{{config('app.url')}}/coupon/apply',
            data: ₹('#coupon-code-ajax').serializeArray(),
            success: function (data) {
                /* console.log(data);
                return false; */
                if (data.status == 1) {
                    let ms = data.messages;
                    ms.forEach(
                        function (m, index) {
                            toastr.success(m, index, {
                                CloseButton: true,
                                ProgressBar: true
                            });
                        }
                    );
                } else {
                    let ms = data.messages;
                    ms.forEach(
                        function (m, index) {
                            toastr.error(m, index, {
                                CloseButton: true,
                                ProgressBar: true
                            });
                        }
                    );
                }
                setInterval(function () {
                    location.reload();
                }, 2000);
            }
        });
    }

    jQuery(".search-bar-input").keyup(function () {
        ₹(".search-card").css("display", "block");
        let name = ₹(".search-bar-input").val();
        if (name.length > 0) {
            ₹.get({
                url: '{{config('app.url')}}/searched-products',
                dataType: 'json',
                data: {
                    name: name
                },
                beforeSend: function () {
                    ₹('#loading').show();
                },
                success: function (data) {
                    ₹('.search-result-box').empty().html(data.result)
                },
                complete: function () {
                    ₹('#loading').hide();
                },
            });
        } else {
            ₹('.search-result-box').empty();
        }
    });

    jQuery(".search-bar-input-mobile").keyup(function () {
        ₹(".search-card").css("display", "block");
        let name = ₹(".search-bar-input-mobile").val();
        if (name.length > 0) {
            ₹.get({
                url: '{{config('app.url')}}/searched-products',
                dataType: 'json',
                data: {
                    name: name
                },
                beforeSend: function () {
                    ₹('#loading').show();
                },
                success: function (data) {
                    ₹('.search-result-box').empty().html(data.result)
                },
                complete: function () {
                    ₹('#loading').hide();
                },
            });
        } else {
            ₹('.search-result-box').empty();
        }
    });

    jQuery(document).mouseup(function (e) {
        var container = ₹(".search-card");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.hide();
        }
    });

    function route_alert(route, message) {
        Swal.fire({
            title: 'Are you sure?',
            text: message,
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: 'default',
            confirmButtonColor: '#ec1c24',
            cancelButtonText: 'No',
            confirmButtonText: 'Yes',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                location.href = route;
            }
        })
    }

    function order_again(order_id) {
        ₹.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": ₹('meta[name="_token"]').attr("content"),
            },
        });
        ₹.ajax({
            type: "POST",
            url: ₹("#order_again_url").data("url"),
            data: {
                order_id,
            },
            beforeSend: function () {
                ₹('#loading').show();
            },
            success: function (response) {
                if (response.status === 1) {
                    updateNavCart();
                    toastr.success(response.message, {
                        CloseButton: true,
                        ProgressBar: true,
                        timeOut: 3000, // duration
                    });
                    location.href = response.redirect_url;
                    return false;
                } else if (response.status === 0) {
                    toastr.warning(response.message, {
                        CloseButton: true,
                        ProgressBar: true,
                        timeOut: 2000, // duration
                    });
                    return false;
                }
            },
            complete: function () {
                ₹('#loading').hide();
            },
        });
    }
</script>
<script>
    ₹('.filter-show-btn').on('click', function(){
        ₹('#shop-sidebar').toggleClass('show active');
    })
    ₹('.cz-sidebar-header .close').on('click', function(){
        ₹('#shop-sidebar').removeClass('show active');
    })
    ₹('.remove-address-by-modal').on('click', function (){
        let link = ₹(this).data('link');
        ₹('#remove-address-link').attr('href', link);
        ₹('#remove-address').modal('show');
    });
</script>

<script>
        let cookie_content = `
        <div class="cookie-section">
            <div class="container">
                <div class="d-flex flex-wrap align-items-center justify-content-between column-gap-4 row-gap-3">
                    <div class="text-wrapper">
                        <h5 class="title">Your Privacy Matter</h5>
                        <div></div>
                    </div>
                    <div class="btn-wrapper">
                        <span class="text-white cursor-pointer" id="cookie-reject">No thanks</span>
                        <button class="btn btn-success cookie-accept" id="cookie-accept">Yes i Accept</button>
                    </div>
                </div>
            </div>
        </div>
    `;
    ₹(document).on('click','#cookie-accept',function() {
        document.cookie = '6valley_cookie_consent=accepted; max-age=' + 60 * 60 * 24 * 30;
        ₹('#cookie-section').hide();
    });
    ₹(document).on('click','#cookie-reject',function() {
        document.cookie = '6valley_cookie_consent=reject; max-age=' + 60 * 60 * 24;
        ₹('#cookie-section').hide();
    });

    ₹(document).ready(function() {
        if (document.cookie.indexOf("6valley_cookie_consent=accepted") !== -1) {
            ₹('#cookie-section').hide();
        }else{
            ₹('#cookie-section').html(cookie_content).show();
        }
    });
</script>

<script>
    ₹(document).ready(function() {
        const currentUrl = new URL(window.location.html);
        const referral_code_parameter = new URLSearchParams(currentUrl.search).get("referral_code");
        if (referral_code_parameter) {
                            window.location.href = "{{config('app.url')}}/customer/auth/sign-up?referral_code=" + referral_code_parameter;
                    }
    });
</script>

<script>
    /*========================
    04: Background Image
    ==========================*/
    var ₹bgImg = ₹("[data-bg-img]");
    ₹bgImg
        .css("background-image", function () {
            return 'url("' + ₹(this).data("bg-img") + '")';
        })
        .removeAttr("data-bg-img")
        .addClass("bg-img");
</script>

    <script>
        /*--flash deal Progressbar --*/
        function update_flash_deal_progress_bar(){
            const current_time_stamp = new Date().getTime();
            const start_date = new Date('').getTime();
            const countdownElement = document.querySelector('.cz-countdown');
            const get_end_time = countdownElement.getAttribute('data-countdown');
            const end_time = new Date(get_end_time).getTime();
            let time_progress = ((current_time_stamp - start_date) / (end_time - start_date))*100;
            const progress_bar = document.querySelector('.flash-deal-progress-bar');
            progress_bar.style.width = time_progress + '%';
        }
            update_flash_deal_progress_bar();
            setInterval(update_flash_deal_progress_bar, 10000);
        /*-- end flash deal Progressbar --*/
    </script>

    <!-- Owl Carousel -->
    <script src="{{asset('public/deals/assets/front-end/js/owl.carousel.min.js')}}"></script>

    <script>
        ₹('.flash-deal-slider').owlCarousel({
            loop: false,
            autoplay: true,
            center:false,
            margin: 10,
            nav: true,
            navText: ["<i class='czi-arrow-left'></i>", "<i class='czi-arrow-right'></i>"],
            dots: false,
            autoplayHoverPause: true,
            'ltr': false,
            // center: true,
            responsive: {
                //X-Small
                0: {
                    items: 1.1
                },
                360: {
                    items: 1.2
                },
                375: {
                    items: 1.4
                },
                480: {
                    items: 1.8
                },
                //Small
                576: {
                    items: 2
                },
                //Medium
                768: {
                    items: 3
                },
                //Large
                992: {
                    items: 4
                },
                //Extra large
                1200: {
                    items: 4
                },
            }
        })
        ₹('.flash-deal-slider-mobile').owlCarousel({
            loop: false,
            autoplay: true,
            center:true,
            margin: 10,
            nav: true,
            navText: ["<i class='czi-arrow-left'></i>", "<i class='czi-arrow-right'></i>"],
            dots: false,
            autoplayHoverPause: true,
            'ltr': false,
            // center: true,
            responsive: {
                //X-Small
                0: {
                    items: 1.1
                },
                360: {
                    items: 1.2
                },
                375: {
                    items: 1.4
                },
                480: {
                    items: 1.8
                },
                //Small
                576: {
                    items: 2
                },
                //Medium
                768: {
                    items: 3
                },
                //Large
                992: {
                    items: 4
                },
                //Extra large
                1200: {
                    items: 4
                },
            }
        })

        ₹('#web-feature-deal-slider').owlCarousel({
            loop: false,
            autoplay: true,
            margin: 20,
            nav: false,
            //navText: ["<i class='czi-arrow-left'></i>", "<i class='czi-arrow-right'></i>"],
            dots: false,
            autoplayHoverPause: true,
            'ltr': true,
            // center: true,
            responsive: {
                //X-Small
                0: {
                    items: 1
                },
                360: {
                    items: 1
                },
                375: {
                    items: 1
                },
                540: {
                    items: 2
                },
                //Small
                576: {
                    items: 2
                },
                //Medium
                768: {
                    items: 2
                },
                //Large
                992: {
                    items: 2
                },
                //Extra large
                1200: {
                    items: 2
                },
                //Extra extra large
                1400: {
                    items: 2
                }
            }
        })

        ₹('.new-arrivals-product').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 20,
            nav: true,
            navText: ["<i class='czi-arrow-left'></i>", "<i class='czi-arrow-right'></i>"],
            dots: false,
            autoplayHoverPause: true,
            'ltr': true,
            // center: true,
            responsive: {
                //X-Small
                0: {
                    items: 1.1
                },
                360: {
                    items: 1.2
                },
                375: {
                    items: 1.4
                },
                540: {
                    items: 2
                },
                //Small
                576: {
                    items: 3
                },
                //Medium
                768: {
                    items: 3
                },
                //Large
                992: {
                    items: 4
                },
                //Extra large
                1200: {
                    items: 6
                },
                //Extra extra large
                1400: {
                    items: 6
                }
            }
        })

        ₹('.new-arrivals-product0').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 20,
            nav: true,
            navText: ["<i class='czi-arrow-left'></i>", "<i class='czi-arrow-right'></i>"],
            dots: false,
            autoplayHoverPause: true,
            'ltr': true,
            // center: true,
            responsive: {
                //X-Small
                0: {
                    items: 1.1
                },
                360: {
                    items: 1.2
                },
                375: {
                    items: 1.4
                },
                540: {
                    items: 2
                },
                //Small
                576: {
                    items: 3
                },
                //Medium
                768: {
                    items: 3
                },
                //Large
                992: {
                    items: 4
                },
                //Extra large
                1200: {
                    items: 5
                },
                //Extra extra large
                1400: {
                    items: 5
                }
            }
        })

        ₹('.category-wise-product-slider').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 20,
            nav: true,
            navText: ["<i class='czi-arrow-left'></i>", "<i class='czi-arrow-right'></i>"],
            dots: false,
            autoplayHoverPause: true,
            'ltr': true,
            responsive: {
                0: {
                    items: 1.2
                },
                375: {
                    items: 1.4
                },
                425: {
                    items: 2
                },
                576: {
                    items: 3
                },
                768: {
                    items: 4
                },
                992: {
                    items: 5
                },
                1200: {
                    items: 5
                },
            }
        })
    </script>
    <script>
        ₹('#featured_products_list').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 20,
            nav: true,
            navText: ["<i class='czi-arrow-left'></i>", "<i class='czi-arrow-right'></i>"],
            dots: false,
            autoplayHoverPause: true,
            'ltr': false,
            // center: true,
            responsive: {
                //X-Small
                0: {
                    items: 1
                },
                360: {
                    items: 1
                },
                375: {
                    items: 1
                },
                540: {
                    items: 2
                },
                //Small
                576: {
                    items: 2
                },
                //Medium
                768: {
                    items: 3
                },
                //Large
                992: {
                    items: 4
                },
                //Extra large
                1200: {
                    items: 5
                },
            }
        });
    </script>
    <script>
        ₹('.hero-slider').owlCarousel({
            loop: false,
            autoplay: true,
            margin: 20,
            nav: true,
            navText: ["<i class='czi-arrow-left'></i>", "<i class='czi-arrow-right'></i>"],
            dots: true,
            autoplayHoverPause: true,
            // 'ltr': false,
            // center: true,
            items: 1
        });
    </script>
    <script>
        ₹('.brands-slider').owlCarousel({
            loop: false,
            autoplay: true,
            margin: 10,
            nav: false,
            'ltr': true,
            autoplayHoverPause: true,
            // center: true,
            responsive: {
                //X-Small
                0: {
                    items: 4,
                    dots: true,
                },
                360: {
                    items: 5,
                    dots: true,
                },
                //Small
                576: {
                    items: 6,
                    dots: false,
                },
                //Medium
                768: {
                    items: 7,
                    dots: false,
                },
                //Large
                992: {
                    items: 9,
                    dots: false,
                },
                //Extra large
                1200: {
                    items: 11,
                    dots: false,
                },
                //Extra extra large
                1400: {
                    items: 12,
                    dots: false,
                }
            }
        })
    </script>

    <script>
        ₹('#category-slider, #top-seller-slider').owlCarousel({
            loop: false,
            autoplay: true,
            margin: 20,
            nav: false,
            // navText: ["<i class='czi-arrow-left'></i>","<i class='czi-arrow-right'></i>"],
            dots: true,
            autoplayHoverPause: true,
            'ltr': true,
            // center: true,
            responsive: {
                //X-Small
                0: {
                    items: 2
                },
                360: {
                    items: 3
                },
                375: {
                    items: 3
                },
                540: {
                    items: 4
                },
                //Small
                576: {
                    items: 5
                },
                //Medium
                768: {
                    items: 6
                },
                //Large
                992: {
                    items: 8
                },
                //Extra large
                1200: {
                    items: 10
                },
                //Extra extra large
                1400: {
                    items: 11
                }
            }
        })
        ₹('.categories--slider').owlCarousel({
            loop: false,
            autoplay: true,
            margin: 20,
            nav: false,
            // navText: ["<i class='czi-arrow-left'></i>","<i class='czi-arrow-right'></i>"],
            dots: false,
            autoplayHoverPause: true,
            'ltr': true,
            // center: true,
            responsive: {
                //X-Small
                0: {
                    items: 3
                },
                360: {
                    items: 3.2
                },
                375: {
                    items: 3.5
                },
                540: {
                    items: 4
                },
                //Small
                576: {
                    items: 5
                },
                //Medium
                768: {
                    items: 6
                },
                //Large
                992: {
                    items: 8
                },
                //Extra large
                1200: {
                    items: 10
                },
                //Extra extra large
                1400: {
                    items: 11
                }
            }
        })
    </script>
    <script>
        // Others Store Slider
        const othersStore = ₹(".others-store-slider").owlCarousel({
            responsiveClass: true,
            nav: false,
            dots: false,
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            smartSpeed: 600,
            rtl: false,
            responsive: {
                0: {
                    items: 1.3,
                    margin: 10,
                },
                480: {
                    items: 2,
                    margin: 26,
                },
                768: {
                    items: 2,
                    margin: 26,
                },
                992: {
                    items: 3,
                    margin: 26,
                },
                1200: {
                    items: 4,
                    margin: 26,
                },
            },
        });
        ₹(".store-next").on("click", function () {
            othersStore.trigger("next.owl.carousel", [600]);
        });
        ₹(".store-prev").on("click", function () {
            othersStore.trigger("prev.owl.carousel", [600]);
        });
    </script>
    <script>
        function myFunction() {
            ₹('#anouncement').slideUp(300);
        }
        ₹(".category-menu").find(".mega_menu").parents("li").addClass("has-sub-item").find("> a").append("<i class='czi-arrow-right'></i>");

        ₹('.category-menu-toggle-btn').on('click', function() {
            ₹('.megamenu-wrap').toggleClass('show')
        });

        ₹('.navbar-tool-icon-box').on('click', function() {
            ₹('.megamenu-wrap').removeClass('show')
        })

        // mega menu will remove when window reload
        ₹(window).on('scroll', function() {
            ₹('.megamenu-wrap').removeClass('show')
        });



    </script>

    <script>
        ₹('.close-search-form-mobile').on('click', function(){
            ₹('.search-form-mobile').removeClass('active')
        })
        ₹('.open-search-form-mobile').on('click', function(){
            ₹('.search-form-mobile').addClass('active')
        })
    </script>
    </body>
</html>
