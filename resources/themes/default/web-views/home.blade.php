
@include('layouts.ui.header')
        <span id="authentication-status" data-auth="false"></span>
        <div class="row">
            <div class="col-12" style="margin-top: 10rem; position: fixed; z-index: 9999;">
                <div id="loading" style="display: none;">
                    <center><img width="200" src="#" onerror='this.src="{{ asset("public/deals/assets/front-end/img/loader.gif") }}"' /></center>

                </div>
            </div>
        </div>
        <!-- <b>main</b>
        {{$main_banner}}
        </br>
        <b>main sectiont</b>
        {{$main_section_banner}}
        </br>
        <b>footer </b>
        {{$footer_banner}} -->
        @if(isset($main_banner[0]->photo))      
            <section class="banner-home">
                <div class="container">
                    <div class="banner">
                        <img src=" {{asset('storage/app/public/banner')}}/{{$main_banner[0]->photo}}" alt="Los Angeles" />
                    </div>
                </div>
            </section>
        @endif
     
        <section class="flash-dealing">
            <div class="container">
                @if($flash_deals)
                    @if($flash_deals->products->count() > 0)
                        @if ($flash_deals->count() > 0)
                            <div class="row flash-dealing-bg">
                                <div class="col-sm-4">
                                    <div class="flash-dealing-left">
                                        <h1>Flash Deal</h1>
                                        <p>Hurry Up! The offer is limited. Grab while it lasts</p>
                                        <div id="time_wrapper">
                                            <ul id="time_input">
                                                <li for="hours"><span class="value-count">{{$flash_deals->days}}</span><span class="label lbl-hrs">Days</span><span class="colum">:</span></li>
                                                <li for="minutes"><span class="value-count">{{$flash_deals->hours}}</span><span class="label lbl-min">Hours</span><span class="colum">:</span></li>
                                                <li for="seconds"><span class="value-count">{{$flash_deals->minutes}}</span><span class="label lbl-sec">Minutes</span><span class="colum">:</span></li>
                                                <li for="seconds"><span class="value-count">{{$flash_deals->seconds}}</span><span class="label lbl-sec">Seconds</span></li>
                                                <div class="w3-light-grey"><div class="w3-grey" style="height: 4px; width: 75%;"></div></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="flash-dealing-right">
                                        <div class="view-all">
                                            <a class="text-capitalize view-all-text" style="color: #ec1c24 !important;" href="products?search_by_filter_value=featured_deal&page=1">
                                            <!-- View all -->
                                            <i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                        </div>
                                    
                                        <div class="row">
                                            <!-- @foreach($flash_deals->products as $flash_deal_product)
                                        
                                            <div class="col-sm-3">
                                                <div class="flash-deal-list">
                                                    <a href="#"><img src="{{asset('public/deals/assets/images/deal-off.jpg')}}" alt="deals" /></a><span class="dicount-perstg">10% Off</span>
                                                    <span class="favrate-icon">
                                                        <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                    </span>
                                                    <div class="deal-info">
                                                        <div class="star-rating" style="margin-right: 10px;">
                                                            <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                            <i class="tio-star-outlined text-warning"></i>
                                                        </div>
                                                        <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                        <h2><a href="#">{{$flash_deal_product->product->name}}</a></h2>
                                                    
                                                        <h3><span class="mrp-price">₹500</span> <span class="disc-price"> ₹450</span></h3>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach -->
                    
                                            @foreach($flash_deals->products as $flash_deal_product)
                                            
                                                <div class="col-sm-3">
                                                    <div class="flash-deal-list">
                                                        <a href="product/{{$flash_deal_product->product->slug}}"><img src="{{ asset('storage/app/public/product/thumbnail') }}/{{ $flash_deal_product->product->thumbnail }}" alt="deals" /></a>
                                                        <span class="dicount-perstg">
                                                            @if ($flash_deal_product->product->discount_type == 'percent')
                                                                {{round($flash_deal_product->product->discount, 0)}}%
                                                            @elseif($flash_deal_product->product->discount_type =='flat')
                                                                {{\App\CPU\Helpers::currency_converter($flash_deal_product->product->discount)}}
                                                            @endif
                                                            {{translate('off')}}
                                                        </span>
                                                        
                                                        <span class="favrate-icon">
                                                            <a onclick="addToWishlist({{$flash_deal_product->product->id}})">
                                                                @if(in_array($flash_deal_product->product->id, $wishlist->toArray()))
                                                                    <i class="navbar-tool-icon czi-heart filled"></i>
                                                                @else
                                                                    <i class="navbar-tool-icon czi-heart"></i>
                                                                @endif
                                                               
                                                            </a>
                                                        
                                                        </span>
                                                        <div class="deal-info">
                                                    

                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                @php
                                                                    $average = isset($flash_deal_product->product['rating'][0]['average']) ? $flash_deal_product->product['rating'][0]['average'] : 0;
                                                                    $full_stars = round($average);
                                                                    $empty_stars = 5 - $full_stars;
                                                                @endphp

                                                                @for ($i = 0; $i < $full_stars; $i++)
                                                                    <i class="tio-star text-warning"></i>
                                                                @endfor

                                                                @for ($i = 0; $i < $empty_stars; $i++)
                                                                    <i class="tio-star-outlined text-warning"></i>
                                                                @endfor
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">({{$flash_deal_product->product->reviews_count}})</span>
                                                            <h2><a href="product/{{$flash_deal_product->product->slug}}">{{ $flash_deal_product->product->name }}</a></h2>
                                                        
                                                            @if (isset($flash_deal_product->product->purchase_price) && isset($flash_deal_product->product->unit_price))
                                                                <h3>
                                                                    <span class="mrp-price">₹{{ number_format($flash_deal_product->product->unit_price, 2) }}</span>
                                                                    <span class="disc-price"> {{\App\CPU\Helpers::currency_converter(
                                                                    $flash_deal_product->product->unit_price-(\App\CPU\Helpers::get_product_discount($flash_deal_product->product,$flash_deal_product->product->unit_price))
                                                                            )}}</span>
                                                                </h3>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                @endif
            </div>
        </section>
        @if ($featured_products->count() > 0)
            <section class="new-arrival-section featured_products">
                <div class="container rtl mt-4">
                    <div class="card-body">
                        <div class="section-header">
                            <div class="arrival-title d-block">
                                <div class="text-capitalize"><h1>Featured Products</h1></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="categories-title m-0"></div>
                                <div>
                                    <a class="text-capitalize view-all-text" style="color: #ec1c24 !important;" href="products?search_by_filter_value=featured&page=1">
                                    <!-- View all -->
                                    <i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="py-2">
                            <div class="new_arrival_product">
                                <div class="carousel-wrap">
                                    <div class="new-arrivals-product_my">
                                        <ul class="my_new-arrivals-product">
                                          
                                           @if ($featured_products->count() > 0)
                                                @foreach($featured_products as $featured_product)
                                                    @include('layouts.ui.product', ['wishlist' => $wishlist])
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
        <br />
        @if ($home_categories->count() > 0)
            <div class="__inline-61">
                <section class="pb-4 rtl">
                    <div class="container">
                        <div>
                            <div class="card __shadow h-100 max-md-shadow-0">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                        <div class="categories-title m-0"><span class="font-semibold">Categories</span></div>
                                        <div>
                                            <a class="text-capitalize view-all-text" href="categories" style="color: #ec1c24 !important;"  ><i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                        </div>
                                    </div>
                                    <div class="d-none d-md-block">
                                        <div class="row mt-3">
                                  
                                            @if ($home_categories->count() > 0)
                                                @foreach($home_categories as $category)
                                                    @if ($category->products->count() > 0)               
                                                        <div class="text-center __m-5px __cate-item">
                                                            <a href="category_based_products/{{$category->id}}">
                                                                <div class="__img">
                                                                    <img 
                                                                        onerror='this.src="{{ asset("public/deals/assets/front-end/img/image-place-holder.png") }}"' 
                                                                        src="{{asset('storage/app/public/category')}}/{{$category->icon}}" 
                                                                        alt="{{$category->name}}" 
                                                                    />
                                                                </div>
                                                                <p class="text-center small mt-2">{{$category->name}}</p>
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        
                                            
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <br />
        @endif
        @if(isset($main_section_banner->photo))                                                            
            <section class="promotion">
                <div class="container">
                    <div class="promo-img">
                        <img src=" {{asset('storage/app/public/banner')}}/{{$main_section_banner->photo}}" alt="Los Angeles" />    
                
                    </div>
                </div>
            </section>
        @endif
        <br />
        
       
        <!--Start-->
        <div class="__inline-61">
            <section class="new-arrival-section">
                <div class="container">
                    <div class="arrivals-main">
                        <div class="row">
                            @if ($deal_of_the_day->product->count() > 0)
                                <div class="col-xl-3 col-md-4">
                                    <div class="deal_of_the_day h-100 bg--light">
                                        <div class="d-flex justify-content-center align-items-center py-4"><h4 class="font-bold m-0 align-items-center text-uppercase text-center px-2" style="color: #1455ac;">Deal of the Day</h4></div>
                                        
                                            <div class="recomanded-product-card mt-0">
                                                <div class="d-flex justify-content-center align-items-center __pt-20 __m-20-r">
                                                    <div class="position-relative">
                                                        <img src=" {{asset('storage/app/public/product/thumbnail')}}/{{$deal_of_the_day->product->thumbnail}}" alt=""/>
                                                                        
                                                        <span class="dicount-perstg">
                                                            @if($deal_of_the_day->product->discount !== 'null')
                                                            @if ($deal_of_the_day->product->discount_type == 'percent')
                                                                {{round($deal_of_the_day->product->discount, 0)}}%
                                                            @elseif($deal_of_the_day->product->discount_type =='flat')
                                                                {{\App\CPU\Helpers::currency_converter($deal_of_the_day->product->discount)}}
                                                            @endif
                                                            @endif
                                                            {{translate('off')}}
                                
                                                        </span>
                                                        <span class="favrate-icon">
                                                            <a onclick="addToWishlist({{$deal_of_the_day->product->id}})"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="__i-1 bg-transparent text-center mb-0">
                                                    <div class="__p-20px px-0 pb-0">
                                                        <h6 class="font-semibold pt-2">{{$deal_of_the_day->product->name}}</h6>
                                                        <div class="mb-4 pt-1 d-flex flex-wrap justify-content-center align-items-center text-center gap-8">
                                                            <span class="mrp-price">₹{{$deal_of_the_day->product->unit_price}}</span>
                                                            <span class="text-accent __text-22px text-dark">₹{{$deal_of_the_day->product->purchase_price}}</span>
                                                        </div>
                                                        <button class="btn btn--primary font-bold px-4 rounded-10 text-uppercase" onclick='addToCart({{$deal_of_the_day->product->id}})'>Buy now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                    </div>
                                </div>
                            @endif
                            @if ($new_arrival_products->count() > 0)
                                <div class="col-sm-9">
                                    <div class="rtl">
                                        <div class="section-header gopi">
                                            <div class="text-black font-bold __text-22px"><span>New arrivals</span></div>
                                            <div class="__mr-2px">
                                                <a class="text-capitalize view-all-text" href="products?sort_by=latest" style="color: #ec1c24 !important;">
                                                <!-- View all -->
                                                <i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rtl mb-3 overflow-hidden">
                                        <div class="py-2">
                                            <div class="new_arrival_product">
                                                <div class="carousel-wrap">
                                                    <div>
                                                        <ul class="my_new-arrivals-product gopi-basam">
                                                            @if ($new_arrival_products->count() > 0)
                                                                @foreach($new_arrival_products as $featured_product)
                                                                    @include('layouts.ui.product')
                                                                
                                                                @endforeach
                                                            @endif
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif  
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!--End-->
        <br/>
        <!-- <section class="promotion">
            <div class="container">
                <div class="promo-img"><img src="{{asset('public/deals/assets/images/promo.jpg')}}" alt="Promoation" /></div>
            </div>
        </section> -->
        @if(isset($new_arrival_banner[0]->photo))         
            <section class="banner-home">
                <div class="container">
                    <div class="banner">
                        <img src=" {{asset('storage/app/public/banner')}}/{{$new_arrival_banner[0]->photo}}" alt="Los Angeles" />
                    </div>
                </div>
            </section>
        @endif
        <br/>
        <!--Start-->

            @foreach($home_categories as $index => $category)
                @if($category->products->count() > 0)                                               
                    <div class="__inline-61">
                        <section class="new-arrival-section">
                            <div class="container">
                                <div class="arrivals-main">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="arrivals-left">
                                                <img src="{{asset('storage/app/public/category_banner')}}/{{$category->banner_image}}" alt="">
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="rtl">
                                                <div class="section-header gopi">
                                                    <div class="text-black font-bold __text-22px"><span>{{$category->name}}</span></div>
                                                    <div class="__mr-2px">
                                                        <a class="text-capitalize view-all-text" href="category_based_products/{{$category->id}}" style="color: #ec1c24 !important;">View all<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rtl mb-3 overflow-hidden">
                                                <div class="py-2">
                                                    <div class="new_arrival_product">
                                                        <div class="carousel-wrap">
                                                            <ul class="my_new-arrivals-product gopi-basam">
                                                                @foreach($category->products as $featured_product)    
                                                                    @include('layouts.ui.product')
                                                                
                                                                
                                                                @endforeach
                                                            
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <br/>
                    @if($index === 1)
                        @if(!empty($home_category_banner))
                            @if(isset($home_category_banner[0]->photo))                                         
                                <section class="promotion">
                                    <div class="container">
                                        <div class="promo-img">
                                            <img src="{{asset('storage/app/public/banner')}}/{{$home_category_banner[0]->photo}}" alt="Los Angeles" />    
                                        </div>
                                    </div>
                                </section>
                            @endif
                        @endif
                    @endif
                @endif                                              
                <br/>
            @endforeach
            @if(isset($footer_banner[0]->photo))                                                           
                <section class="promotion">
                    <div class="container">
                        <div class="promo-img">
                            <img src=" {{asset('storage/app/public/banner')}}/{{$footer_banner[0]->photo}}" alt="Los Angeles" />    
                    
                        </div>
                    </div>
                </section>
            @endif

        
        <br/>
        
        @if ($whats_trending->count() > 0)
        <div class="__inline-61">
            <section class="new-arrival-section">
                <div class="container">
                    <div class="arrivals-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="rtl">
                                    <div class="section-header gopi">
                                        <div class="text-black font-bold __text-22px"><span>What's Trending</span></div>
                                        <div class="__mr-2px">
                                            <a class="text-capitalize view-all-text" href="#" style="color: #ec1c24 !important;">View all<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @foreach($whats_trending as $trending)
                            <div class="col-sm-3">
                                <div class="trending">
                                    <a href="#">
                                        <iframe width="560" height="315" src="{{$trending->url}}" frameborder="0" allowfullscreen></iframe>
    
                                        <p>{{$trending->title}}</p>
                                    </a>
                                </div>
                            </div>
                            @endforeach
                             
                        </div>
                    </div>
                </div>
            </section>
        </div>
        @endif
        <!--End-->
 

        @if($web_config['brand_setting'] && $brands->count() > 0)
            <div class="__inline-61"></div>
                <section class="container rtl pt-4">
                    <div class="section-header gopi">
                        <div class="text-black font-bold __text-22px"><span>{{translate('brands')}}</span></div>
                        <div class="__mr-2px">
                            <!-- <a class="text-capitalize view-all-text" href="#" style="color: #ec1c24 !important;">{{ translate('view_all')}}<i class="czi-arrow-right ml-1 mr-n1"></i></a> -->
                        </div>
                    </div>
                    <div class="row as-gopi">
                       
                            @foreach($brands as $brand)
                            
                              
                                @if($brand && $brand->brand_products_count && $brand->brand_products_count > 0)                      
                                <div class="col-sm-1 text-center">
                                    <a href="products?brand={{$brand->id}}&page=1" class="__brand-item">
                                        <img 
                                            onerror='this.src="{{ asset("public/deals/assets/front-end/img/image-place-holder.png") }}"' 
                                            src="{{asset('storage/app/public/brand')}}/{{$brand->image}}" 
                                            alt="{{$brand->name}}" 
                                        />
                                    </a>
                                </div>
                                @endif
                               
                            @endforeach
                       
                    </div>
                </section>
            </div>
        @endif
        <br/>
        <!--Start-->
        @include('layouts.ui.social_media')
        <!--End-->

        <br/>
        @include('layouts.ui.delivery')
        @include('layouts.ui.footer') 
        
        <div class="modal fade" id="remove-wishlist-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/remove-wishlist.png')}}" alt="" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220">Product has been removed from wishlist</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="outof-stock-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/out-of-stock.png')}}" alt="" class="mw-100px" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220" id="outof-stock-modal-message">Out of stock</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="add-wishlist-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/added-wishlist.png')}}" alt="" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220">Product added to wishlist</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="login-alert-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/locked-icon.svg')}}" alt="" />
                            <h6 class="font-semibold mt-3 mb-1">Please Login</h6>
                            <p class="mb-4"><small>You need to login to view this feature</small></p>
                        </div>
                        <div class="d-flex gap-3 justify-content-center">
                            <button class="btn btn-soft-secondary bg--secondary __rounded-10" data-dismiss="modal">Cancel</button><a href="{{route('customer.auth.login')}}" class="btn btn-primary __rounded-10">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="remove-address">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{asset('public/deals/assets/front-end/img/icons/remove-address.png')}}" alt="" />
                            <h6 class="font-semibold mt-3 mb-1">Delete this address?</h6>
                            <p class="mb-4"><small>This address will be removed from this list</small></p>
                        </div>
                        <div class="d-flex gap-3 justify-content-center">
                            <a href="javascript:" class="btn btn-primary __rounded-10" id="remove-address-link">Remove</a><button class="btn btn-soft-secondary bg--secondary __rounded-10" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn-scroll-top btn--primary" href="#top" data-scroll><span class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Top</span><i class="btn-scroll-top-icon czi-arrow-up"></i></a>
        <script type="text/javascript">
            function add_tocart(){
                console.log('vamsi');
            }
        
           
        </script>  


   
       

        <script type="text/javascript">
            function addToWishlist(product_id, modalId) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    url: "{{config('app.url')}}/store-wishlist",
                    method: "POST",
                    data: {
                        product_id: product_id,
                    },
                    success: function (data) {
                        if (data.value == 1) {
                            $(".countWishlist").html(data.count);
                            $(".countWishlist-" + product_id).text(data.product_count);
                            $(".tooltip").html("");
                            $(`.wishlist_icon_${product_id}`).removeClass("fa fa-heart-o").addClass("fa fa-heart");
                            $("#add-wishlist-modal").modal("show");
                            $(`#${modalId}`).modal("show");
                        } else if (data.value == 2) {
                            $("#remove-wishlist-modal").modal("show");
                            $(".countWishlist").html(data.count);
                            $(".countWishlist-" + product_id).text(data.product_count);
                            $(`.wishlist_icon_${product_id}`).removeClass("fa fa-heart").addClass("fa fa-heart-o");
                        } else {
                            $("#login-alert-modal").modal("show");
                        }
                    },
                });
            }
            function add_to_cart(form_id = "add-to-cart-form"){
                console.log(form_id);
            }
            function checkAddToCartValidity() {
                var names = {};
                $("#add-to-cart-form input:radio").each(function () {
                    // find unique names
                    names[$(this).attr("name")] = true;
                });
                var count = 0;
                $.each(names, function () {
                    // then count them
                    count++;
                });
                if ($("input:radio:checked").length == count) {
                    return true;
                }
                return false;
            }
            function addToCart(form_id = "add-to-cart-form", redirect_to_checkout = true) {
                if (checkAddToCartValidity()) {
                    var data_json = {
                        id: form_id,
                        quantity: '1'
                    };

                    $.ajaxSetup({
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                        }
                    });

                    // $("#loading").show(); // Show loading indicator before AJAX request

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('cart.add') }}',
                        data: data_json,
                        success: function (response) {
                            console.log(response);
                            if (response.status == 1) {
                                updateNavCart();
                                toastr.success(response.message, {
                                    CloseButton: true,
                                    ProgressBar: true,
                                });
                                $(".call-when-done").click();
                                if (redirect_to_checkout) {
                                    location.href = "{{route('checkout-details')}}";
                                }
                            } else if (response.status == 0) {
                                $("#outof-stock-modal-message").html(response.message);
                                $("#outof-stock-modal").modal("show");
                            }
                        },
                        error: function () {
                            Swal.fire({
                                type: "error",
                                title: "Error",
                                text: "An error occurred while processing your request.",
                            });
                        },
                        // complete: function () {
                        //     $("#loading").hide(); // Hide loading indicator after AJAX request completes
                        // }
                    });
                } else {
                    Swal.fire({
                        type: "info",
                        title: "Cart",
                        text: "Please choose all the options",
                    });
                }
            }

            function isValidForm(form_id) {
                // Implement your form validation logic here
                // For example, check if all required fields are filled
                return true; // Change this to your validation check
            }

            
        </script>
            

    </body>
</html>
