<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8" />
        <title>Products</title>
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <link rel="apple-touch-icon" sizes="180x180" href="storage/app/public/company/2021-03-02-603df1634614f.html" />
        <link rel="icon" type="image/png" sizes="32x32" href="storage/app/public/company/2021-03-02-603df1634614f.html" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/simplebar/dist/simplebar.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/tiny-slider/dist/tiny-slider.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/drift-zoom/dist/drift-basic.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/lightgallery.js/dist/css/lightgallery.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/back-end/css/toastr.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/theme.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/slick.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/back-end/css/toastr.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/master.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/css/lightbox.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/back-end/vendor/icon-set/style.css" />
        <meta property="og:image" content='{{config('app.url')}}/storage/app/public/company/{"id":12,"type":"company_web_logo","value":"2024-01-10-659e6d9ba8af2.webp","created_at":null,"updated_at":"2024-01-10T10:12:43.000000Z"}' />
        <meta property="og:title" content='Products of {"id":11,"type":"company_name","value":"Sameer Traders","created_at":null,"updated_at":"2021-02-27T18:11:53.000000Z"} ' />
        <meta property="og:url" content="index.html" />
        <meta property="og:description" content="this is about us page. hello and hi from about page description.." />
        <meta property="twitter:card" content='{{config('app.url')}}/storage/app/public/company/{"id":12,"type":"company_web_logo","value":"2024-01-10-659e6d9ba8af2.webp","created_at":null,"updated_at":"2024-01-10T10:12:43.000000Z"}' />
        <meta property="twitter:title" content='Products of {"id":11,"type":"company_name","value":"Sameer Traders","created_at":null,"updated_at":"2021-02-27T18:11:53.000000Z"}' />
        <meta property="twitter:url" content="index.html" />
        <meta property="twitter:description" content="this is about us page. hello and hi from about page description.." />
        <style>
            .for-count-value {
                right: 0.6875 rem;
            }
            .for-count-value {
                right: 0.6875 rem;
            }
            .for-brand-hover:hover {
                color: #ec1c24;
            }
            .for-hover-lable:hover {
                color: #ec1c24 !important;
            }
            .page-item.active .page-link {
                background-color: #ec1c24 !important;
            }
            .sidepanel {
                left: 0;
            }
            .sidepanel .closebtn {
                right: 25 px;
            }
        </style>
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/home.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/responsive1.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/style.css" />
        <meta name="_token" content="OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO" />
        <style>
            .rtl {
                direction: ltr;
            }
            .dropdown-item:focus,
            .dropdown-item:hover {
                color: #ec1c24;
            }
            .dropdown-item.active,
            .dropdown-item:active {
                color: #000;
            }
            .navbar-light .navbar-tool-icon-box {
                color: #ec1c24;
            }
            .search_button {
                background-color: #ec1c24;
            }
            .mega-nav .nav-item .nav-link {
                color: #ec1c24 !important;
            }
            .checkbox-alphanumeric label:hover,
            .owl-theme .owl-nav [class*="owl-"] {
                border-color: #ec1c24;
            }
            ::-webkit-scrollbar-thumb:hover {
                background: #000 !important;
            }
            [type="radio"] {
                border: 0;
                clip: rect(0 0 0 0);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 1px;
            }
            [type="radio"] + span:after {
                box-shadow: 0 0 0 0.1em#000;
            }
            [type="radio"]:checked + span:after {
                background: #000;
                box-shadow: 0 0 0 0.1em#000;
            }
            .navbar-tool .navbar-tool-label {
                background-color: #ec1c24 !important;
            }
            .btn--primary {
                color: #fff;
                background-color: #ec1c24 !important;
                border-color: #ec1c24 !important;
            }
            .btn--primary:hover {
                color: #fff;
                background-color: #ec1c24 !important;
                border-color: #ec1c24 !important;
            }
            .btn-secondary {
                background-color: #000 !important;
                border-color: #000 !important;
            }
            .btn-outline-accent:hover {
                color: #fff;
                background-color: #ec1c24;
                border-color: #ec1c24;
            }
            .btn-outline-accent {
                color: #ec1c24;
                border-color: #ec1c24;
            }
            .text-accent {
                color: #ec1c24;
            }
            .text-base-2,
            a:hover {
                color: #000;
            }
            .active-menu,
            .text-base,
            .text-primary {
                color: #ec1c24 !important;
            }
            .page-item.active > .page-link {
                box-shadow: 0 0.5rem 1.125rem -0.425rem#ec1c24;
            }
            .page-item.active .page-link {
                background-color: #ec1c24;
            }
            .btn-outline-accent:not(:disabled):not(.disabled).active,
            .btn-outline-accent:not(:disabled):not(.disabled):active,
            .show > .btn-outline-accent.dropdown-toggle {
                background-color: #000;
                border-color: #000;
            }
            .btn-outline-primary {
                color: #ec1c24;
                border-color: #ec1c24;
            }
            .btn-outline-primary:hover {
                background-color: #000;
                border-color: #000;
            }
            .btn-outline-primary.focus,
            .btn-outline-primary:focus {
                box-shadow: 0 0 0 0#000;
            }
            .btn-outline-primary:not(:disabled):not(.disabled).active,
            .btn-outline-primary:not(:disabled):not(.disabled):active,
            .show > .btn-outline-primary.dropdown-toggle {
                background-color: #ec1c24;
                border-color: #ec1c24;
            }
            .btn-outline-primary:not(:disabled):not(.disabled).active:focus,
            .btn-outline-primary:not(:disabled):not(.disabled):active:focus,
            .show > .btn-outline-primary.dropdown-toggle:focus {
                box-shadow: 0 0 0 0#ec1c24;
            }
            .for-discoutn-value {
                background: #ec1c24;
            }
            .dropdown-menu {
                margin-left: -8px !important;
            }
            :root {
                --base: #ec1c24;
                --base-2: #000000;
            }
            span.badge-accent {
                color: var(--base);
                background-color: #ec1c2440;
            }
            span.badge-accent:hover {
                color: var(--base) !important;
            }
        </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap" rel="stylesheet">
    </head>
    <body class="toolbar-enabled">
        <span id="order_again_url" data-url="{{config('app.url')}}/cart/order-again"></span>
        <div class="modal-quick-view modal fade" id="quick-view" tabindex="-1">
            <div class="modal-dialog modal-xl"><div class="modal-content" id="quick-view-modal"></div></div>
        </div>
        <style>
            .for-count-value {
                color: #ec1c24;
            }
            .count-value {
                color: #ec1c24;
            }
            @media (min-width: 768px) {}
            @media (max-width: 767px) {
                .search_button .input-group-text i {
                    color: #ec1c24 !important;
                }
                .mega-nav1 {
                    color: #ec1c24 !important;
                }
                .mega-nav1 .nav-link {
                    color: #ec1c24 !important;
                }
            }
            @media (max-width: 471px) {
                .mega-nav1 {
                    color: #ec1c24 !important;
                }
                .mega-nav1 .nav-link {
                    color: #ec1c24 !important;
                }
            }
        </style>
         @include('layouts.ui.header')
        <span id="authentication-status" data-auth="false"></span>
        <div class="row">
            <div class="col-12" style="margin-top: 10rem; position: fixed; z-index: 9999;">
                <div id="loading" style="display: none;">
                    <center><img width="200" src="storage/app/public/company/index.html" onerror='this.src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/loader.gif"' /></center>
                </div>
            </div>
        </div>
        <div class="container py-3" dir="ltr">
            <div class="search-page-header">
                <div>
                    <h5 class="font-semibold mb-1">Category Products (Cars)</h5>
                    <div class="view-page-item-count">1 Items found</div>
                </div>
                <form id="search-form" class="d-none d-lg-block" action="{{config('app.url')}}/products" method="GET">
                    <input hidden name="data_from" value="category" />
                    <div class="sorting-item">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="21" viewBox="0 0 20 21" fill="none">
                            <path d="M11.6667 7.80078L14.1667 5.30078L16.6667 7.80078" stroke="#D9D9D9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            <path
                                d="M7.91675 4.46875H4.58341C4.3533 4.46875 4.16675 4.6553 4.16675 4.88542V8.21875C4.16675 8.44887 4.3533 8.63542 4.58341 8.63542H7.91675C8.14687 8.63542 8.33341 8.44887 8.33341 8.21875V4.88542C8.33341 4.6553 8.14687 4.46875 7.91675 4.46875Z"
                                stroke="#D9D9D9"
                                stroke-width="2"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                            />
                            <path
                                d="M7.91675 11.9688H4.58341C4.3533 11.9688 4.16675 12.1553 4.16675 12.3854V15.7188C4.16675 15.9489 4.3533 16.1354 4.58341 16.1354H7.91675C8.14687 16.1354 8.33341 15.9489 8.33341 15.7188V12.3854C8.33341 12.1553 8.14687 11.9688 7.91675 11.9688Z"
                                stroke="#D9D9D9"
                                stroke-width="2"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                            />
                            <path d="M14.1667 5.30078V15.3008" stroke="#D9D9D9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                        <label class="for-shoting" for="sorting"><span>Sort product</span></label>
                        <select onchange="filter(this.value)">
                            <option value="latest">Latest</option>
                            <option value="low-high">Low to High Price</option>
                            <option value="high-low">High to Low Price</option>
                            <option value="a-z">A to Z Order</option>
                            <option value="z-a">Z to A Order</option>
                        </select>
                    </div>
                </form>
                <div class="d-lg-none">
                    <div class="filter-show-btn btn btn--primary py-1 px-2 m-0"><i class="tio-filter"></i></div>
                </div>
            </div>
        </div>
        <div class="container pb-5 mb-2 mb-md-4 rtl __inline-35" dir="ltr">
            <div class="row">
                <aside class="col-lg-3 hidden-xs col-md-3 col-sm-4 SearchParameters __search-sidebar pr-2" id="SearchParameters">
                    <div class="cz-sidebar __inline-35" id="shop-sidebar">
                        <div class="cz-sidebar-header bg-light">
                            <button class="close ml-auto" type="button" data-dismiss="sidebar" aria-label="Close"><i class="tio-clear"></i></button>
                        </div>
                        <div class="pb-0">
                            <div class="text-center">
                                <div class="__cate-side-title border-bottom"><span class="widget-title font-semibold">Filter</span></div>
                                <div class="__p-25-10 w-100 pt-4">
                                    <label class="w-100 opacity-75 text-nowrap for-shoting d-block mb-0" for="sorting" style="padding-right: 0;">
                                        <select class="form-control custom-select" id="searchByFilterValue">
                                            <option selected="selected" disabled="disabled">Choose</option>
                                            <option value="">Best Selling Products</option>
                                            <option value="">Top rated</option>
                                            <option value="">Most favorite</option>
                                            <option value="">Featured deal</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="__p-25-10 w-100 pt-0 d-lg-none">
                                    <form id="search-form" action="#" method="GET">
                                        <input hidden name="data_from" value="category" />
                                        <select class="form-control" onchange="filter(this.value)">
                                            <option value="latest">Latest</option>
                                            <option value="low-high">Low to High Price</option>
                                            <option value="high-low">High to Low Price</option>
                                            <option value="a-z">A to Z Order</option>
                                            <option value="z-a">Z to A Order</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="text-center">
                                <div class="__cate-side-title pt-0"><span class="widget-title font-semibold">Price</span></div>
                                <div class="d-flex justify-content-between align-items-center __cate-side-price">
                                    <div class="__w-35p"><input class="bg-white cz-filter-search form-control form-control-sm appended-form-control" type="number" value="0" min="0" max="1000000" id="min_price" placeholder="Min" /></div>
                                    <div class="__w-10p"><p class="m-0">To</p></div>
                                    <div class="__w-35p"><input value="100" min="100" max="1000000" class="bg-white cz-filter-search form-control form-control-sm appended-form-control" type="number" id="max_price" placeholder="Max" /></div>
                                    <div class="d-flex justify-content-center align-items-center __number-filter-btn">
                                        <a class="" onclick="searchByPrice()"><i class="__inline-37 czi-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="text-center">
                                <div class="__cate-side-title"><span class="widget-title font-semibold">Brands</span></div>
                                <div class="__cate-side-price pb-3">
                                    <div class="input-group-overlay input-group-sm">
                                        <input style="" placeholder="Search by brands" class="__inline-38 cz-filter-search form-control form-control-sm appended-form-control" type="text" id="search-brand" />
                                        <div class="input-group-append-overlay">
                                            <span class="input-group-text"><i class="czi-search"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <ul id="lista1" class="__brands-cate-wrap" data-simplebar data-simplebar-auto-hide="false">
                                    <div class="brand mt-2 for-brand-hover" id="brand">
                                        <li class="flex-between __inline-39" onclick='location.href="#"'>
                                            <div>Samsung</div>
                                            <div class="__brands-cate-badge"><span>14</span></div>
                                        </li>
                                    </div>    
                                    <div class="brand mt-2 for-brand-hover" id="brand">    
                                        <li class="flex-between __inline-39" onclick='location.href="#"'>
                                            <div>VIVO</div>
                                            <div class="__brands-cate-badge"><span>16</span></div>
                                        </li>
                                    </div>    
                                    <div class="brand mt-2 for-brand-hover" id="brand"> 
                                        <li class="flex-between __inline-39" onclick='location.href="#"'>
                                            <div>Apple</div>
                                            <div class="__brands-cate-badge"><span>12</span></div>
                                        </li>
                                    </div>    
                                    <div class="brand mt-2 for-brand-hover" id="brand"> 
                                        <li class="flex-between __inline-39" onclick='location.href="#"'>
                                            <div>Realme</div>
                                            <div class="__brands-cate-badge"><span>20</span></div>
                                        </li>
                                    </div>    
                                    <div class="brand mt-2 for-brand-hover" id="brand"> 
                                        <li class="flex-between __inline-39" onclick='location.href="#"'>
                                            <div>Poco</div>
                                            <div class="__brands-cate-badge"><span>13</span></div>
                                        </li>
                                    </div>    
                                    <div class="brand mt-2 for-brand-hover" id="brand"> 
                                        <li class="flex-between __inline-39" onclick='location.href="#"'>
                                            <div>Oneplus</div>
                                            <div class="__brands-cate-badge"><span>8</span></div>
                                        </li>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <div class="mt-3 __cate-side-arrordion">
                            <div>
                                <div class="text-center __cate-side-title"><span class="widget-title font-semibold">Categories</span></div>
                                <div class="accordion mt-n1 __cate-side-price" id="shop-categories">
                                    <div class="menu--caret-accordion">
                                        <div class="card-header flex-between">
                                            <div><label class="for-hover-lable cursor-pointer" href="#">Customer Ratings</label></div>
                                            <div class="px-2 cursor-pointer menu--caret">
                                                <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                            </div>
                                        </div>
                                        <div class="card-body p-0 ml-2" id="collapse-1" style="display: none;">
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" ><input type="checkbox" id="rating" name="rating" value="4"> 4 <i class="fa fa-star" aria-hidden="true"></i> & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-11" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 3 <i class="fa fa-star" aria-hidden="true"></i> & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-12" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 2 <i class="fa fa-star" aria-hidden="true"></i> & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-13" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 1 <i class="fa fa-star" aria-hidden="true"></i> & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-14" style="display: none;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="menu--caret-accordion">
                                        <div class="card-header flex-between">
                                            <div><label class="for-hover-lable cursor-pointer" href="#">Ram</label></div>
                                            <div class="px-2 cursor-pointer menu--caret">
                                                <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                            </div>
                                        </div>
                                        <div class="card-body p-0 ml-2" id="collapse-1" style="display: none;">
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" ><input type="checkbox" id="rating" name="rating" value="4"> 16 GB & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-11" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 8 GB & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-12" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 4 GB & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-13" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 3 GB & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-14" style="display: none;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="menu--caret-accordion">
                                        <div class="card-header flex-between">
                                            <div><label class="for-hover-lable cursor-pointer" href="#">Stock Status</label></div>
                                            <div class="px-2 cursor-pointer menu--caret">
                                                <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                            </div>
                                        </div>
                                        <div class="card-body p-0 ml-2" id="collapse-1" style="display: none;">
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" ><input type="checkbox" id="rating" name="rating" value="4"> In Stock</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-11" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> Out Stock</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-12" style="display: none;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="menu--caret-accordion">
                                        <div class="card-header flex-between">
                                            <div><label class="for-hover-lable cursor-pointer" href="#">Discount</label></div>
                                            <div class="px-2 cursor-pointer menu--caret">
                                                <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                            </div>
                                        </div>
                                        <div class="card-body p-0 ml-2" id="collapse-1" style="display: none;">
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" ><input type="checkbox" id="rating" name="rating" value="4"> 50% & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-11" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 40% & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-12" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 30% & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-12" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 20% & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-12" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer"><input type="checkbox" id="rating" name="rating" value="4"> 10% & above</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-12" style="display: none;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="menu--caret-accordion">
                                        <div class="card-header flex-between">
                                            <div><label class="for-hover-lable cursor-pointer" href="#">Electronics</label></div>
                                            <div class="px-2 cursor-pointer menu--caret">
                                                <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                            </div>
                                        </div>
                                        <div class="card-body p-0 ml-2" id="collapse-1" style="display: none;">
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Mixer</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-11" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Grinder</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-12" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Cooker</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-13" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">TV</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-14" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Washing machine</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-16" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Fridge</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-15" style="display: none;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="menu--caret-accordion">
                                        <div class="card-header flex-between">
                                            <div><label class="for-hover-lable cursor-pointer" href="#">Cars</label></div>
                                            <div class="px-2 cursor-pointer menu--caret">
                                                <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                            </div>
                                        </div>
                                        <div class="card-body p-0 ml-2" id="collapse-3" style="display: none;">
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Swift</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret">
                                                        <strong class="pull-right"><i class="tio-next-ui fs-13"></i></strong>
                                                    </div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-10" style="display: none;">
                                                    <div class="card-header">
                                                        <label class="for-hover-lable d-block cursor-pointer text-left" href="#">VXI</label>
                                                    </div>
                                                    <div class="card-header">
                                                        <label class="for-hover-lable d-block cursor-pointer text-left" href="#">ZXI</label>
                                                    </div>
                                                    <div class="card-header">
                                                        <label class="for-hover-lable d-block cursor-pointer text-left" href="#">LXI</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="menu--caret-accordion">
                                        <div class="card-header flex-between">
                                            <div><label class="for-hover-lable cursor-pointer" href="#">Mobiles</label></div>
                                            <div class="px-2 cursor-pointer menu--caret">
                                                <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                            </div>
                                        </div>
                                        <div class="card-body p-0 ml-2" id="collapse-4" style="display: none;">
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Samsung</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret">
                                                        <strong class="pull-right"><i class="tio-next-ui fs-13"></i></strong>
                                                    </div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-7" style="display: none;">
                                                    <div class="card-header">
                                                        <label class="for-hover-lable d-block cursor-pointer text-left" href="#">S22 ultra</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Oppo</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-8" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Vivo</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret">
                                                        <strong class="pull-right"><i class="tio-next-ui fs-13"></i></strong>
                                                    </div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-9" style="display: none;">
                                                    <div class="card-header">
                                                        <label class="for-hover-lable d-block cursor-pointer text-left" href="#">V25</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="menu--caret-accordion">
                                        <div class="card-header flex-between">
                                            <div><label class="for-hover-lable cursor-pointer" href="#">Laptops</label></div>
                                            <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right for-brand-hover"></strong></div>
                                        </div>
                                        <div class="card-body p-0 ml-2" id="collapse-5" style="display: none;"></div>
                                    </div>
                                    <div class="menu--caret-accordion">
                                        <div class="card-header flex-between">
                                            <div><label class="for-hover-lable cursor-pointer" href="#">Furnitures</label></div>
                                            <div class="px-2 cursor-pointer menu--caret">
                                                <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                            </div>
                                        </div>
                                        <div class="card-body p-0 ml-2" id="collapse-6" style="display: none;">
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Tables</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-17" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Chairs</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-18" style="display: none;"></div>
                                            </div>
                                            <div class="menu--caret-accordion">
                                                <div class="for-hover-lable card-header flex-between">
                                                    <div><label class="cursor-pointer" href="#">Sofas</label></div>
                                                    <div class="px-2 cursor-pointer menu--caret"><strong class="pull-right"></strong></div>
                                                </div>
                                                <div class="card-body p-0 ml-2" id="collapse-19" style="display: none;"></div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
                <section class="col-lg-9">
                    <div class="row" id="ajax-products">
                        <div class="rtl mb-3 overflow-hidden">
                            <div class="py-2">
                                <div class="new_arrival_product">
                                    <div class="carousel-wrap">
                                        <div>
                                            <ul class="my_new-arrivals-product gopi-basam">
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp1.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp2.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp3.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp4.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp5.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp4.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp5.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp1.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp2.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp3.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp2.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp3.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp4.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="flash-deal-list">
                                                        <a href="#"><img src="{{config('app.asset_url')}}/public/deals/assets/images/fp5.jpg" alt="deals"></a><span class="dicount-perstg">10% Off</span>
                                                        <span class="favrate-icon">
                                                            <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                        </span>
                                                        <div class="deal-info">
                                                            <div class="star-rating" style="margin-right: 10px;">
                                                                <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                                <i class="tio-star-outlined text-warning"></i>
                                                            </div>
                                                            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                            <h2><a href="#">Exclusive &amp; Fashionable</a></h2>
                                                            <h3><span class="mrp-price">₹500</span><span class="disc-price"> ₹450</span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                </section>
            </div>
        </div>
        <!--Start-->
        @include('layouts.ui.social_media')
        <!--End-->

        <br/>
           @include('layouts.ui.delivery')
           @include('layouts.ui.footer')
        <div class="modal fade" id="remove-wishlist-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/remove-wishlist.png" alt="" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220">Product has been removed from wishlist</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="outof-stock-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/out-of-stock.png" alt="" class="mw-100px" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220" id="outof-stock-modal-message">Out of stock</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="add-wishlist-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/added-wishlist.png" alt="" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220">Product added to wishlist</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="login-alert-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/locked-icon.svg" alt="" />
                            <h6 class="font-semibold mt-3 mb-1">Please Login</h6>
                            <p class="mb-4"><small>You need to login to view this feature</small></p>
                        </div>
                        <div class="d-flex gap-3 justify-content-center">
                            <button class="btn btn-soft-secondary bg--secondary __rounded-10" data-dismiss="modal">Cancel</button><a href="{{route('customer.auth.login')}}" class="btn btn-primary __rounded-10">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="remove-address">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/remove-address.png" alt="" />
                            <h6 class="font-semibold mt-3 mb-1">Delete this address?</h6>
                            <p class="mb-4"><small>This address will be removed from this list</small></p>
                        </div>
                        <div class="d-flex gap-3 justify-content-center">
                            <a href="javascript:" class="btn btn-primary __rounded-10" id="remove-address-link">Remove</a><button class="btn btn-soft-secondary bg--secondary __rounded-10" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn-scroll-top btn--primary" href="#top" data-scroll><span class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Top</span><i class="btn-scroll-top-icon czi-arrow-up"></i></a>

        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/jquery/dist/jquery-2.2.4.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/simplebar/dist/simplebar.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/tiny-slider/dist/min/tiny-slider.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/js/lightbox.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/drift-zoom/dist/Drift.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/lightgallery.js/dist/js/lightgallery.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/lg-video.js/dist/lg-video.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/back-end/js/toastr.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/theme.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/custom.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/slick.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/sweet_alert.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/back-end/js/toastr.js"></script>
        <script type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                const $stickyElement = $(".bottom-sticky_ele");
                const $offsetElement = $(".bottom-sticky_offset");

                if ($stickyElement.length !== 0) {
                    $(window).on("scroll", function () {
                        const elementOffset = $offsetElement.offset().top - $(window).height() / 1.2;
                        const scrollTop = $(window).scrollTop();

                        if (scrollTop >= elementOffset) {
                            $stickyElement.addClass("stick");
                        } else {
                            $stickyElement.removeClass("stick");
                        }
                    });
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".password-toggle-btn").on("click", function () {
                    let checkbox = $(this).find("input[type=checkbox]");
                    let eyeIcon = $(this).find("i");
                    checkbox.change(function () {
                        if (checkbox.is(":checked")) {
                            eyeIcon.removeClass("tio-hidden").addClass("tio-invisible");
                        } else {
                            eyeIcon.removeClass("tio-invisible").addClass("tio-hidden");
                        }
                    });
                });
            });
        </script>
        <script>
            toastr.options = {
                closeButton: !1,
                debug: !1,
                newestOnTop: !1,
                progressBar: !1,
                positionClass: "toast-top-right",
                preventDuplicates: !1,
                onclick: null,
                showDuration: "300",
                hideDuration: "1000",
                timeOut: "5000",
                extendedTimeOut: "1000",
                showEasing: "swing",
                hideEasing: "linear",
                showMethod: "fadeIn",
                hideMethod: "fadeOut",
            };
        </script>
        <script>
            function addWishlist(product_id, modalId) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    url: "{{config('app.url')}}/store-wishlist",
                    method: "POST",
                    data: {
                        product_id: product_id,
                    },
                    success: function (data) {
                        if (data.value == 1) {
                            $(".countWishlist").html(data.count);
                            $(".countWishlist-" + product_id).text(data.product_count);
                            $(".tooltip").html("");
                            $(`.wishlist_icon_${product_id}`).removeClass("fa fa-heart-o").addClass("fa fa-heart");
                            $("#add-wishlist-modal").modal("show");
                            $(`#${modalId}`).modal("show");
                        } else if (data.value == 2) {
                            $("#remove-wishlist-modal").modal("show");
                            $(".countWishlist").html(data.count);
                            $(".countWishlist-" + product_id).text(data.product_count);
                            $(`.wishlist_icon_${product_id}`).removeClass("fa fa-heart").addClass("fa fa-heart-o");
                        } else {
                            $("#login-alert-modal").modal("show");
                        }
                    },
                });
            }

            function removeWishlist(product_id, modalId) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    url: "{{config('app.url')}}/delete-wishlist",
                    method: "POST",
                    data: {
                        id: product_id,
                    },
                    beforeSend: function () {
                        $("#loading").show();
                    },
                    success: function (data) {
                        $(`#${modalId}`).modal("show");

                        $(".countWishlist").html(parseInt($(".countWishlist").html()) - 1);
                        $("#row_id" + product_id).hide();
                        $(".tooltip").html("");
                        if (parseInt($(".countWishlist").html()) % 15 === 0) {
                            if ($("#wishlist_paginated_page").val() == 1) {
                                $("#set-wish-list").empty().append(`
                            <center>
                                <h6 class="text-muted">
                                    No data found.
                                </h6>
                            </center>
                        `);
                            } else {
                                let page_value = $("#wishlist_paginated_page").val();
                                window.location.href = "{{config('app.url')}}/wishlists?page=" + (page_value - 1);
                            }
                        }
                    },
                    complete: function () {
                        $("#loading").hide();
                    },
                });
            }

            function quickView(product_id) {
                $.get({
                    url: "{{config('app.url')}}/quick-view",
                    dataType: "json",
                    data: {
                        product_id: product_id,
                    },
                    beforeSend: function () {
                        $("#loading").show();
                    },
                    success: function (data) {
                        console.log("success...");
                        $("#quick-view").modal("show");
                        $("#quick-view-modal").empty().html(data.view);
                    },
                    complete: function () {
                        $("#loading").hide();
                    },
                });
            }

            function addToCart(form_id = "add-to-cart-form", redirect_to_checkout = false) {
                if (checkAddToCartValidity()) {
                    $.ajaxSetup({
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                        },
                    });
                    $.post({
                        url: "{{config('app.url')}}/cart/add",
                        data: $("#" + form_id).serializeArray(),
                        beforeSend: function () {
                            $("#loading").show();
                        },
                        success: function (response) {
                            console.log(response);
                            if (response.status == 1) {
                                updateNavCart();
                                toastr.success(response.message, {
                                    CloseButton: true,
                                    ProgressBar: true,
                                });
                                $(".call-when-done").click();
                                if (redirect_to_checkout) {
                                    location.href = "index.html";
                                }
                                return false;
                            } else if (response.status == 0) {
                                $("#outof-stock-modal-message").html(response.message);
                                $("#outof-stock-modal").modal("show");
                                return false;
                            }
                        },
                        complete: function () {
                            $("#loading").hide();
                        },
                    });
                } else {
                    Swal.fire({
                        type: "info",
                        title: "Cart",
                        text: "Please choose all the options",
                    });
                }
            }

            function buy_now() {
                addToCart("add-to-cart-form", true);
                /* location.href = "{{config('app.url')}}/checkout-details"; */
            }

            function currency_change(currency_code) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    type: "POST",
                    url: "{{config('app.url')}}/currency",
                    data: {
                        currency_code: currency_code,
                    },
                    success: function (data) {
                        toastr.success("Currency changed to" + data.name);
                        location.reload();
                    },
                });
            }

            function removeFromCart(key) {
                $.post("cart/remove.html", { _token: "OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO", key: key }, function (response) {
                    $("#cod-for-cart").hide();
                    updateNavCart();
                    $("#cart-summary").empty().html(response.data);
                    toastr.info("Item has been removed from cart", {
                        CloseButton: true,
                        ProgressBar: true,
                    });
                    let segment_array = window.location.pathname.split("index.html");
                    let segment = segment_array[segment_array.length - 1];
                    if (segment === "checkout-payment" || segment === "checkout-details") {
                        location.reload();
                    }
                });
            }

            function updateNavCart() {
                $.post("cart/nav-cart-items.html", { _token: "OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO" }, function (response) {
                    $("#cart_items").html(response.data);
                });
            }
            /*new*/
            $("#add-to-cart-form").on("submit", function (e) {
                e.preventDefault();
            });

            /*new*/
            function cartQuantityInitialize() {
                $(".btn-number").click(function (e) {
                    e.preventDefault();

                    fieldName = $(this).attr("data-field");
                    type = $(this).attr("data-type");
                    productType = $(this).attr("product-type");
                    var input = $("input[name='" + fieldName + "']");
                    var currentVal = parseInt($(".input-number").val());
                    // alert(currentVal);
                    if (!isNaN(currentVal)) {
                        // console.log(productType)
                        if (type == "minus") {
                            if (currentVal > $(".input-number").attr("min")) {
                                $(".input-number")
                                    .val(currentVal - 1)
                                    .change();
                            }
                            if (parseInt($(".input-number").val()) == $(".input-number").attr("min")) {
                                $(this).attr("disabled", true);
                            }
                        } else if (type == "plus") {
                            // alert('ok out of stock');
                            if (currentVal < $(".input-number").attr("max") || productType === "digital") {
                                $(".input-number")
                                    .val(currentVal + 1)
                                    .change();
                            }

                            if (parseInt(input.val()) == $(".input-number").attr("max") && productType === "physical") {
                                $(this).attr("disabled", true);
                            }
                        }
                    } else {
                        $(".input-number").val(0);
                    }
                });

                $(".input-number").focusin(function () {
                    $(this).data("oldValue", $(this).val());
                });

                $(".input-number").change(function () {
                    productType = $(this).attr("product-type");
                    minValue = parseInt($(this).attr("min"));
                    maxValue = parseInt($(this).attr("max"));
                    valueCurrent = parseInt($(this).val());
                    var name = $(this).attr("name");
                    if (valueCurrent >= minValue) {
                        $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr("disabled");
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: "Cart",
                            text: "Sorry the minimum order quantity does not match",
                        });
                        $(this).val($(this).data("oldValue"));
                    }
                    if (productType === "digital" || valueCurrent <= maxValue) {
                        $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr("disabled");
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: "Cart",
                            text: "Sorry stock limit exceeded.",
                        });
                        $(this).val($(this).data("oldValue"));
                    }
                });
                $(".input-number").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if (
                        $.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                        // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Allow: home, end, left, right
                        (e.keyCode >= 35 && e.keyCode <= 39)
                    ) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
            }

            function updateQuantity(key, element) {
                $.post(
                    "cart/updateQuantity.html",
                    {
                        _token: "OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO",
                        key: key,
                        quantity: element.value,
                    },
                    function (data) {
                        updateNavCart();
                        $("#cart-summary").empty().html(data);
                    }
                );
            }

            function updateCartQuantity(cart_id, product_id, action, event) {
                let remove_url = $("#remove_from_cart_url").data("url");
                let update_quantity_url = $("#update_quantity_url").data("url");
                let token = $('meta[name="_token"]').attr("content");
                let product_qyt = parseInt($(`.cartQuantity${cart_id}`).val()) + parseInt(action);
                let cart_quantity_of = $(`.cartQuantity${cart_id}`);
                let segment_array = window.location.pathname.split("index.html");
                let segment = segment_array[segment_array.length - 1];

                if (cart_quantity_of.val() == 0) {
                    toastr.info($(".cannot_use_zero").data("text"), {
                        CloseButton: true,
                        ProgressBar: true,
                    });
                    cart_quantity_of.val(cart_quantity_of.data("min"));
                } else if (cart_quantity_of.val() == cart_quantity_of.data("min") && event == "minus") {
                    $.post(
                        remove_url,
                        {
                            _token: token,
                            key: cart_id,
                        },
                        function (response) {
                            updateNavCart();
                            toastr.info(response.message, {
                                CloseButton: true,
                                ProgressBar: true,
                            });
                            if (segment === "shop-cart" || segment === "checkout-payment" || segment === "checkout-details") {
                                location.reload();
                            }
                        }
                    );
                } else {
                    if (cart_quantity_of.val() < cart_quantity_of.data("min")) {
                        let min_value = cart_quantity_of.data("min");
                        toastr.error("Minimum order quantity cannot be less than " + min_value);
                        cart_quantity_of.val(min_value);
                        updateCartQuantity(cart_id, product_id, action, event);
                    } else {
                        $(`.cartQuantity${cart_id}`).html(product_qyt);
                        $.post(
                            update_quantity_url,
                            {
                                _token: token,
                                key: cart_id,
                                product_id: product_id,
                                quantity: product_qyt,
                            },
                            function (response) {
                                if (response["status"] == 0) {
                                    toastr.error(response["message"]);
                                } else {
                                    toastr.success(response["message"]);
                                }
                                response["qty"] <= 1 ? $(`.quantity__minus${cart_id}`).html('<i class="tio-delete-outlined text-danger fs-10"></i>') : $(`.quantity__minus${cart_id}`).html('<i class="tio-remove fs-10"></i>');

                                $(`.cartQuantity${cart_id}`).val(response["qty"]);
                                $(`.cartQuantity${cart_id}`).html(response["qty"]);
                                $(`.cart_quantity_multiply${cart_id}`).html(response["qty"]);
                                $(".cart_total_amount").html(response.total_price);
                                $(`.discount_price_of_${cart_id}`).html(response["discount_price"]);
                                $(`.quantity_price_of_${cart_id}`).html(response["quantity_price"]);
                                $(`.total_discount`).html(response["total_discount_price"]);
                                $(`.free_delivery_amount_need`).html(response.free_delivery_status.amount_need);
                                if (response.free_delivery_status.amount_need <= 0) {
                                    $(".amount_fullfill").removeClass("d-none");
                                    $(".amount_need_to_fullfill").addClass("d-none");
                                } else {
                                    $(".amount_fullfill").addClass("d-none");
                                    $(".amount_need_to_fullfill").removeClass("d-none");
                                }
                                const progressBar = document.querySelector(".progress-bar");
                                progressBar.style.width = response.free_delivery_status.percentage + "%";
                                if (response["qty"] == cart_quantity_of.data("min")) {
                                    cart_quantity_of.parent().find(".quantity__minus").html('<i class="tio-delete-outlined text-danger fs-10"></i>');
                                } else {
                                    cart_quantity_of.parent().find(".quantity__minus").html('<i class="tio-remove fs-10"></i>');
                                }
                                if (segment === "shop-cart" || segment === "checkout-payment" || segment === "checkout-details") {
                                    location.reload();
                                }
                            }
                        );
                    }
                }
            }
            $("#add-to-cart-form input").on("change", function () {
                getVariantPrice();
            });

            function getVariantPrice() {
                if ($("#add-to-cart-form input[name=quantity]").val() > 0 && checkAddToCartValidity()) {
                    $.ajaxSetup({
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                        },
                    });
                    $.ajax({
                        type: "POST",
                        url: "{{config('app.url')}}/cart/variant_price",
                        data: $("#add-to-cart-form").serializeArray(),
                        success: function (data) {
                            $("#add-to-cart-form #chosen_price_div").removeClass("d-none");
                            $("#add-to-cart-form #chosen_price_div #chosen_price").html(data.price);
                            $("#chosen_price_mobile").html(data.price);
                            $("#set-tax-amount-mobile").html(data.tax);
                            $("#set-tax-amount").html(data.tax);
                            $("#set-discount-amount").html(data.discount);
                            $("#available-quantity").html(data.quantity);
                            $(".cart-qty-field").attr("max", data.quantity);
                        },
                    });
                }
            }

            function checkAddToCartValidity() {
                var names = {};
                $("#add-to-cart-form input:radio").each(function () {
                    // find unique names
                    names[$(this).attr("name")] = true;
                });
                var count = 0;
                $.each(names, function () {
                    // then count them
                    count++;
                });
                if ($("input:radio:checked").length == count) {
                    return true;
                }
                return false;
            }

            $(".clickable").click(function () {
                window.location = $(this).find("a").attr("href");
                return false;
            });
        </script>
        <script>
            function couponCode() {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    type: "POST",
                    url: "{{config('app.url')}}/coupon/apply",
                    data: $("#coupon-code-ajax").serializeArray(),
                    success: function (data) {
                        /* console.log(data);
                return false; */
                        if (data.status == 1) {
                            let ms = data.messages;
                            ms.forEach(function (m, index) {
                                toastr.success(m, index, {
                                    CloseButton: true,
                                    ProgressBar: true,
                                });
                            });
                        } else {
                            let ms = data.messages;
                            ms.forEach(function (m, index) {
                                toastr.error(m, index, {
                                    CloseButton: true,
                                    ProgressBar: true,
                                });
                            });
                        }
                        setInterval(function () {
                            location.reload();
                        }, 2000);
                    },
                });
            }

            jQuery(".search-bar-input").keyup(function () {
                $(".search-card").css("display", "block");
                let name = $(".search-bar-input").val();
                if (name.length > 0) {
                    $.get({
                        url: "{{config('app.url')}}/searched-products",
                        dataType: "json",
                        data: {
                            name: name,
                        },
                        beforeSend: function () {
                            $("#loading").show();
                        },
                        success: function (data) {
                            $(".search-result-box").empty().html(data.result);
                        },
                        complete: function () {
                            $("#loading").hide();
                        },
                    });
                } else {
                    $(".search-result-box").empty();
                }
            });

            jQuery(".search-bar-input-mobile").keyup(function () {
                $(".search-card").css("display", "block");
                let name = $(".search-bar-input-mobile").val();
                if (name.length > 0) {
                    $.get({
                        url: "{{config('app.url')}}/searched-products",
                        dataType: "json",
                        data: {
                            name: name,
                        },
                        beforeSend: function () {
                            $("#loading").show();
                        },
                        success: function (data) {
                            $(".search-result-box").empty().html(data.result);
                        },
                        complete: function () {
                            $("#loading").hide();
                        },
                    });
                } else {
                    $(".search-result-box").empty();
                }
            });

            jQuery(document).mouseup(function (e) {
                var container = $(".search-card");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    container.hide();
                }
            });

            function route_alert(route, message) {
                Swal.fire({
                    title: "Are you sure?",
                    text: message,
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonColor: "default",
                    confirmButtonColor: "#ec1c24",
                    cancelButtonText: "No",
                    confirmButtonText: "Yes",
                    reverseButtons: true,
                }).then((result) => {
                    if (result.value) {
                        location.href = route;
                    }
                });
            }

            function order_again(order_id) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    type: "POST",
                    url: $("#order_again_url").data("url"),
                    data: {
                        order_id,
                    },
                    beforeSend: function () {
                        $("#loading").show();
                    },
                    success: function (response) {
                        if (response.status === 1) {
                            updateNavCart();
                            toastr.success(response.message, {
                                CloseButton: true,
                                ProgressBar: true,
                                timeOut: 3000, // duration
                            });
                            location.href = response.redirect_url;
                            return false;
                        } else if (response.status === 0) {
                            toastr.warning(response.message, {
                                CloseButton: true,
                                ProgressBar: true,
                                timeOut: 2000, // duration
                            });
                            return false;
                        }
                    },
                    complete: function () {
                        $("#loading").hide();
                    },
                });
            }
        </script>
        <script>
            $(".filter-show-btn").on("click", function () {
                $("#shop-sidebar").toggleClass("show active");
            });
            $(".cz-sidebar-header .close").on("click", function () {
                $("#shop-sidebar").removeClass("show active");
            });
            $(".remove-address-by-modal").on("click", function () {
                let link = $(this).data("link");
                $("#remove-address-link").attr("href", link);
                $("#remove-address").modal("show");
            });
        </script>
        <script>
            let cookie_content = `
        <div class="cookie-section">
            <div class="container">
                <div class="d-flex flex-wrap align-items-center justify-content-between column-gap-4 row-gap-3">
                    <div class="text-wrapper">
                        <h5 class="title">Your Privacy Matter</h5>
                        <div></div>
                    </div>
                    <div class="btn-wrapper">
                        <span class="text-white cursor-pointer" id="cookie-reject">No thanks</span>
                        <button class="btn btn-success cookie-accept" id="cookie-accept">Yes i Accept</button>
                    </div>
                </div>
            </div>
        </div>
    `;
            $(document).on("click", "#cookie-accept", function () {
                document.cookie = "6valley_cookie_consent=accepted; max-age=" + 60 * 60 * 24 * 30;
                $("#cookie-section").hide();
            });
            $(document).on("click", "#cookie-reject", function () {
                document.cookie = "6valley_cookie_consent=reject; max-age=" + 60 * 60 * 24;
                $("#cookie-section").hide();
            });

            $(document).ready(function () {
                if (document.cookie.indexOf("6valley_cookie_consent=accepted") !== -1) {
                    $("#cookie-section").hide();
                } else {
                    $("#cookie-section").html(cookie_content).show();
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                const currentUrl = new URL(window.location.html);
                const referral_code_parameter = new URLSearchParams(currentUrl.search).get("referral_code");
                if (referral_code_parameter) {
                    window.location.href = "{{config('app.url')}}/customer/auth/sign-up?referral_code=" + referral_code_parameter;
                }
            });
        </script>
        <script>
            var $bgImg = $("[data-bg-img]");
            $bgImg
                .css("background-image", function () {
                    return 'url("' + $(this).data("bg-img") + '")';
                })
                .removeAttr("data-bg-img")
                .addClass("bg-img");
        </script>
        <script>
            function openNav() {
                document.getElementById("mySidepanel").style.width = "70%";
                document.getElementById("mySidepanel").style.height = "100vh";
            }

            function closeNav() {
                document.getElementById("mySidepanel").style.width = "0";
            }

            function filter(value) {
                $.get({
                    url: "{{config('app.url')}}/products",
                    data: {
                        id: "3",
                        name: "",
                        data_from: "category",
                        min_price: "",
                        max_price: "",
                        sort_by: value,
                    },
                    dataType: "json",
                    beforeSend: function () {
                        $("#loading").show();
                    },
                    success: function (response) {
                        $("#ajax-products").html(response.view);
                    },
                    complete: function () {
                        $("#loading").hide();
                    },
                });
            }

            function searchByPrice() {
                let min = $("#min_price").val();
                let max = $("#max_price").val();
                $.get({
                    url: "{{config('app.url')}}/products",
                    data: {
                        id: "3",
                        name: "",
                        data_from: "category",
                        sort_by: "",
                        min_price: min,
                        max_price: max,
                    },
                    dataType: "json",
                    beforeSend: function () {
                        $("#loading").show();
                    },
                    success: function (response) {
                        $("#ajax-products").html(response.view);
                        $("#paginator-ajax").html(response.paginator);
                        console.log(response.total_product);
                        $("#price-filter-count").text(response.total_product + " Items found");
                    },
                    complete: function () {
                        $("#loading").hide();
                    },
                });
            }

            $("#searchByFilterValue, #searchByFilterValue-m").change(function () {
                var url = $(this).val();
                if (url) {
                    window.location = url;
                }
                return false;
            });

            $("#search-brand").on("keyup", function () {
                var value = this.value.toLowerCase().trim();
                $("#lista1 div>li")
                    .show()
                    .filter(function () {
                        return $(this).text().toLowerCase().trim().indexOf(value) == -1;
                    })
                    .hide();
            });

            $(".menu--caret").on("click", function (e) {
                var element = $(this).closest(".menu--caret-accordion");
                if (element.hasClass("open")) {
                    element.removeClass("open");
                    element.find(".menu--caret-accordion").removeClass("open");
                    element.find(".card-body").slideUp(300, "swing");
                } else {
                    element.addClass("open");
                    element.children(".card-body").slideDown(300, "swing");
                    element.siblings(".menu--caret-accordion").children(".card-body").slideUp(300, "swing");
                    element.siblings(".menu--caret-accordion").removeClass("open");
                    element.siblings(".menu--caret-accordion").find(".menu--caret-accordion").removeClass("open");
                    element.siblings(".menu--caret-accordion").find(".card-body").slideUp(300, "swing");
                }
            });
        </script>
        <script>
            function myFunction() {
                $("#anouncement").slideUp(300);
            }
            $(".category-menu").find(".mega_menu").parents("li").addClass("has-sub-item").find("> a").append("<i class='czi-arrow-right'></i>"),
                $(".category-menu-toggle-btn").on("click", function () {
                    $(".megamenu-wrap").toggleClass("show");
                }),
                $(".navbar-tool-icon-box").on("click", function () {
                    $(".megamenu-wrap").removeClass("show");
                }),
                $(window).on("scroll", function () {
                    $(".megamenu-wrap").removeClass("show");
                });
        </script>
        <script>
            $(".close-search-form-mobile").on("click", function () {
                $(".search-form-mobile").removeClass("active");
            }),
                $(".open-search-form-mobile").on("click", function () {
                    $(".search-form-mobile").addClass("active");
                });
        </script>
    </body>
</html>
