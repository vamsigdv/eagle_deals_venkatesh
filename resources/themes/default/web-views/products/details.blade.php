<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8" />
        <title>Product Details</title>
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <link rel="apple-touch-icon" sizes="180x180" href="storage/app/public/company/2021-03-02-603df1634614f.html" />
        <link rel="icon" type="image/png" sizes="32x32" href="storage/app/public/company/2021-03-02-603df1634614f.html" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/simplebar/dist/simplebar.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/tiny-slider/dist/tiny-slider.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/drift-zoom/dist/drift-basic.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/lightgallery.js/dist/css/lightgallery.min.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/back-end/css/toastr.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/theme.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/slick.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/back-end/css/toastr.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/master.css" />
         <link rel="stylesheet" href="public/css/lightbox.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/back-end/vendor/icon-set/style.css" />
        <meta name="description" content="swift-zxi-buHqPf" />
        <meta name="keywords" content=" Swift ,   ZXI ,  " />
        <meta name="author" content="Eagle Deals" />
        <meta property="og:image" content="storage/app/public/product/meta/def.html" />
        <meta property="twitter:card" content="storage/app/public/product/meta/def.html" />
        <meta property="og:title" content="Swift ZXI" />
        <meta property="twitter:title" content="Swift ZXI" />
        <meta property="og:url" content="swift-zxi-buHqPf.html" />
        <meta property="og:description" content=" Swift ,   ZXI ,  " />
        <meta property="twitter:description" content=" Swift ,   ZXI ,  " />
        <meta property="twitter:url" content="swift-zxi-buHqPf.html" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/product-details.css" />
        <style>
            .btn-number:hover {
                color: #000;
            }
            .for-total-price {
                margin-left: -30%;
            }
            .feature_header span {
                padding-left: 15px;
            }
            .flash-deals-background-image {
                background: #ec1c2410;
            }
            @media (max-width: 768px) {
                .for-total-price {
                    padding-left: 30%;
                }
                .product-quantity {
                    padding-left: 4%;
                }
                .for-margin-bnt-mobile {
                    margin-right: 7px;
                }
            }
            @media (max-width: 375px) {
                .for-margin-bnt-mobile {
                    margin-right: 3px;
                }
                .for-discount {
                    margin-left: 10% !important;
                }
                .for-dicount-div {
                    margin-top: -5%;
                    margin-right: -7%;
                }
                .product-quantity {
                    margin-left: 4%;
                }
            }
            @media (max-width: 500px) {
                .for-dicount-div {
                    margin-right: -5%;
                }
                .for-total-price {
                    margin-left: -20%;
                }
                .view-btn-div {
                    float: right;
                }
                .for-discount {
                    margin-left: 7%;
                }
                .for-mobile-capacity {
                    margin-left: 7%;
                }
            }
        </style>
        <style>
            thead {
                background: #ec1c24 !important;
            }
        </style>
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/home.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/responsive1.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/style.css" />
        <style>
            .rtl {
                direction: ltr;
            }
            .dropdown-item:focus,
            .dropdown-item:hover {
                color: #ec1c24;
            }
            .dropdown-item.active,
            .dropdown-item:active {
                color: #000;
            }
            .navbar-light .navbar-tool-icon-box {
                color: #ec1c24;
            }
            .search_button {
                background-color: #ec1c24;
            }
            .navbar-stuck-menu {
                background-color: #ec1c24;
            }
            .mega-nav .nav-item .nav-link {
                color: #ec1c24 !important;
            }
            .checkbox-alphanumeric label:hover,
            .owl-theme .owl-nav [class*="owl-"] {
                border-color: #ec1c24;
            }
            ::-webkit-scrollbar-thumb:hover {
                background: #000 !important;
            }
            [type="radio"] {
                border: 0;
                clip: rect(0 0 0 0);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 1px;
            }
            [type="radio"] + span:after {
                box-shadow: 0 0 0 0.1em#000;
            }
            [type="radio"]:checked + span:after {
                background: #000;
                box-shadow: 0 0 0 0.1em#000;
            }
            .navbar-tool .navbar-tool-label {
                background-color: #ec1c24 !important;
            }
            .btn--primary {
                color: #fff;
                background-color: #ec1c24 !important;
                border-color: #ec1c24 !important;
            }
            .btn--primary:hover {
                color: #fff;
                background-color: #ec1c24 !important;
                border-color: #ec1c24 !important;
            }
            .btn-secondary {
                background-color: #000 !important;
                border-color: #000 !important;
            }
            .btn-outline-accent:hover {
                color: #fff;
                background-color: #ec1c24;
                border-color: #ec1c24;
            }
            .btn-outline-accent {
                color: #ec1c24;
                border-color: #ec1c24;
            }
            .text-accent {
                color: #ec1c24;
            }
            .text-base-2,
            a:hover {
                color: #000;
            }
            .active-menu,
            .text-base,
            .text-primary {
                color: #ec1c24 !important;
            }
            .page-item.active > .page-link {
                box-shadow: 0 0.5rem 1.125rem -0.425rem#ec1c24;
            }
            .page-item.active .page-link {
                background-color: #ec1c24;
            }
            .btn-outline-accent:not(:disabled):not(.disabled).active,
            .btn-outline-accent:not(:disabled):not(.disabled):active,
            .show > .btn-outline-accent.dropdown-toggle {
                background-color: #000;
                border-color: #000;
            }
            .btn-outline-primary {
                color: #ec1c24;
                border-color: #ec1c24;
            }
            .btn-outline-primary:hover {
                background-color: #000;
                border-color: #000;
            }
            .btn-outline-primary.focus,
            .btn-outline-primary:focus {
                box-shadow: 0 0 0 0#000;
            }
            .btn-outline-primary:not(:disabled):not(.disabled).active,
            .btn-outline-primary:not(:disabled):not(.disabled):active,
            .show > .btn-outline-primary.dropdown-toggle {
                background-color: #ec1c24;
                border-color: #ec1c24;
            }
            .btn-outline-primary:not(:disabled):not(.disabled).active:focus,
            .btn-outline-primary:not(:disabled):not(.disabled):active:focus,
            .show > .btn-outline-primary.dropdown-toggle:focus {
                box-shadow: 0 0 0 0#ec1c24;
            }
            .for-discoutn-value {
                background: #ec1c24;
            }
            .dropdown-menu {
                margin-left: -8px !important;
            }
            :root {
                --base: #ec1c24;
                --base-2: #000000;
            }
            span.badge-accent {
                color: var(--base);
                background-color: #ec1c2440;
            }
            span.badge-accent:hover {
                color: var(--base) !important;
            }
        </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
</head>
    <body class="toolbar-enabled">
        <span id="order_again_url" data-url="{{config('app.url')}}/cart/order-again"></span>
        <div class="modal-quick-view modal fade" id="quick-view" tabindex="-1">
            <div class="modal-dialog modal-xl"><div class="modal-content" id="quick-view-modal"></div></div>
        </div>
        <style>
            .for-count-value {
                color: #ec1c24;
            }
            .count-value {
                color: #ec1c24;
            }
            @media (min-width: 768px) {
                .navbar-stuck-menu {
                    background-color: #ec1c24;
                }
            }
            @media (max-width: 767px) {
                .search_button .input-group-text i {
                    color: #ec1c24 !important;
                }
                .mega-nav1 {
                    color: #ec1c24 !important;
                }
                .mega-nav1 .nav-link {
                    color: #ec1c24 !important;
                }
            }
            @media (max-width: 471px) {
                .mega-nav1 {
                    color: #ec1c24 !important;
                }
                .mega-nav1 .nav-link {
                    color: #ec1c24 !important;
                }
            }
        </style>
           @include('layouts.ui.header')
        <span id="authentication-status" data-auth="false"></span>
        <div class="row">
            <div class="col-12" style="margin-top: 10rem; position: fixed; z-index: 9999;">
                <div id="loading" style="display: none;">
                    <center><img width="200" src="storage/app/public/company/index.html" onerror='this.src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/loader.gif"' /></center>
                </div>
            </div>
        </div>

        <div class="__inline-23">
            <div class="container mt-4 rtl" style="text-align: left;">
                <div class="row">
                    <div class="col-lg-9 col-12">
                        <div class="row">
                       

                   
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="cz-product-gallery">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="cz product-thum-img">
                                                <div class="table-responsive __max-h-515px" data-simplebar>
                                                    <div class="d-flex">
                                                       
                                                        @if($product->images!=null && json_decode($product->images)>0)
                                                            @foreach (json_decode($product->images) as $key => $photo)
                                                               
                                                                <div class="cz-thumblist">
                                                                    <a class="cz-thumblist-item active d-flex align-items-center justify-content-center" id="preview-img{{$key}}" href="#image{{$key}}">
                                                                        <img src="{{ asset('storage/app/public/product/' . $photo) }}" alt="Product thumb" />
                                                                    </a>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                               
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="cz-preview">
                                               
                                                @if($product->images!=null && json_decode($product->images)>0)
                                                    @foreach (json_decode($product->images) as $key => $photo)
                                                        <div class="cz-preview-item d-flex align-items-center justify-content-center @if($key == 0) active @endif" id="image{{$key}}">
          
                                                            <img
                                                                class="cz-image-zoom img-responsive__max-h-323px"
                                                                src="{{ asset('storage/app/public/product/' . $photo) }}"
                                                                data-zoom="{{ asset('storage/app/public/product/' . $photo) }}"
                                                                alt="Product image"
                                                                width=""
                                                            />
                                                            <div class="cz-image-zoom-pane"></div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                               
                                            </div>
                                            <div class="d-flex flex-column gap-3">
                                                <button type="button" onclick='addWishlist("1")' class="btn __text-18px border wishlight-pos-btn d-sm-none">
                                                    <i class="fa fa-heart-o wishlist_icon_1" style="color: #ec1c24;" aria-hidden="true"></i>
                                                </button>
                                                <div style="text-align: left;" class="sharethis-inline-share-buttons share--icons"></div>
                                            </div>
                                            <div class="__btn-grp mt-2 mb-3 d-none d-sm-flex product-thum-img">
                                                <button class="btn btn--primary element-center btn-gap-right" onclick="addToCart({{ $product->id }})" type="button">
                                                    <span class="string-limit"><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</span>
                                                </button>
                                                <!-- <button type="button" onclick='addWishlist({{ $product->id }})' class="btn btn--primary element-center btn-gap-right">
                                                    <i class="fa fa-heart-o wishlist_icon_1" style="color: #ec1c24;" aria-hidden="true"></i>
                                                </button> -->
                                                <button class="btn btn-secondary element-center __iniline-26 btn-gap-right" onclick="buy_now({{ $product->id }})" type="button">
                                                    <span class="string-limit"><i class="fa fa-bolt" aria-hidden="true"></i> Buy now</span>
                                                </button>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
          
                            <div class="col-lg-6 col-md-6 col-12 mt-md-0 mt-sm-3" style="direction: ltr;">
                                <section class="product-item-details">
                                    <div class="pd-info">
                                        <!-- <h5>Apple</h5> -->
                                        <h1>{{$product->name}}</h1>
                                        <div class="pd-star">
                                           <p><span>{{$product->rating_average}} <i class="fa fa-star" aria-hidden="true"></i></span> {{$product->reviews_count}} Ratings</p>
                                        </div>
                                        @if($product->current_stock)
                                            <p class="availability">Availability: <span>{{$product->current_stock}} in stock</span></p>
                                        @else
                                            <h5 class="mt-3 text-danger">{{translate('out_of_stock')}}</h5>
                                        @endif
                                        @if (!is_null($product->colors) && count(json_decode($product->colors)) > 0)

                                            <div class="flex-start align-items-center mb-2">
                                                <div class="product-description-label m-0 text-dark font-bold">{{translate('color')}}:
                                                </div>
                                                <div>
                                                    <ul class="list-inline checkbox-color mb-0 flex-start {{Session::get('direction') === "rtl" ? 'mr-2' : 'ml-2'}}"
                                                        style="padding-{{Session::get('direction') === "rtl" ? 'right' : 'left'}}: 0;">
                                                        @foreach (json_decode($product->colors) as $key => $color)
                                                            <li>
                                                                <input type="radio"
                                                                    id="{{ $product->id }}-color-{{ str_replace('#','',$color) }}"
                                                                    name="color" value="{{ $color }}"
                                                                    @if($key == 0) checked @endif>
                                                                <label style="background: {{ $color }};"
                                                                    for="{{ $product->id }}-color-{{ str_replace('#','',$color) }}"
                                                                    data-toggle="tooltip" onclick="focus_preview_image_by_color('{{ str_replace('#','',$color) }}')">
                                                                <span class="outline"></span></label>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        @endif
                                     
                                        <div class="product-colors">
                                            <ul>
                                                @if($product->images!=null && json_decode($product->images)>0)
                                                    @foreach (json_decode($product->images) as $key => $photo)
                                                        <li class="cz-thumblist">
                                                            <a href="#">
                                                                <img src="{{ asset('storage/app/public/product/' . $photo) }}" alt="Product thumb" />
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                @endif
                                               
                                            </ul>
                                        </div>  
                                       
                                       
                                       
                                        @if($product_child_products!=null && $product_child_products->count() > 0)
                                            <p class="availability"><span id="availabilityText"></span></p>
                                            <ul class="memory-selct">
                                                @foreach ($product_child_products as $data)
                                                    <li><a href="{{config('app.url')}}/product/{{$data->slug}}" onmouseover="showAvailability('{{$data->name}}')">{{$data->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        @endif
                                       

                                        <script>
                                            function showAvailability(name) {
                                                document.getElementById("availabilityText").textContent = name;
                                            }
                                        </script>
                                         @if (json_decode($product->specifications) != null)
                                        <div class="key-spec">
                                            <h3>Key specifications</h3>
                                            <ul>
                                               
                                                    @foreach (json_decode($product->specifications) as $spec)
                                                        <li><i class="fa fa-check-circle" aria-hidden="true"></i> {{$spec->description}}</li>
                                                    @endforeach
                                         
                                             
                                            </ul>
                                        </div>
                                        @endif
                                        <ul class="memory-selct inf-det">
                                            @if (!is_null($product->tags) && count(json_decode($product->tags)) > 0)
                                                @foreach($product->tags as $product_tags)
                                                    <li><a href="#"> {{$product_tags->tag}}</a></li>
                                           
                                                @endforeach
                                           
                                            @endif
                                          
                                        </ul>
                                        <div class="deliver-opt">
                                            <form class="deliver-pincode" action="">
                                                <label class="deliv-lab">Deliver Option</label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="deliver_pincode" value="{{Session::get('delivery_postcode')}}" placeholder="Deliver Pincode">
                                                    <button class="subscribe-button btn" id="deliver_button" type="button">Check</button>
                                                    <span class="marker-loc"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                                </div>
                                                <div style="color:green" class="" id="delivery_status_message"></div>
                                                <div class="clear-fix"></div>
                                            </form>
                                            
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="product-info-right">
                            <input type="hidden" id="product_id" name="id" value="{{ $product->id }}">
                            <h2>Deal Price: <span>₹{{number_format((($product->unit_price-$product->discount)))}}</span></h2>
                            <h3>Offer Price: <span>₹{{$product->unit_price}}</span></h3>
                            <h3>MRP: <span>₹{{$product->purchase_price}}</span> <small>(Inclusive of all taxes)</small></h3>
                            @if($product->purchase_price && $product->unit_price && $product->discount )
                            <p class="your-save">You Save: {{number_format((($product->purchase_price-$product->unit_price-$product->discount)/($product->unit_price))*100, 0)}}% off (   {{$product->purchase_price - $product->unit_price - $product->discount}})</p>
                            @endif

                            <p class="no-stemi">EMI Starts at ₹ 3,094. No Cost EMI available <a href="#">EMI</a></p>
                            <div class="available-offer">
                                <p>Available offers</p>
                                <div class="offer-discount">
                                    <p class="offer-instant">Get 4,000 instant discount</p>
                                    <p class="on-card">On ICIC credit card </p>
                                    <a href="#" class="term-cnd">T&C</a>
                                </div>
                                <div class="offer-discount">
                                    <p class="offer-instant">Get 2,000 instant discount</p>
                                    <p class="on-card">On IOB credit card  </p>
                                    <a href="#" class="term-cnd">T&C</a>
                                </div>
                                <div class="offer-discount">
                                    <p class="offer-instant">Get 5,000 instant discount</p>
                                    <p class="on-card">On SBI credit card </p>
                                    <a href="#" class="term-cnd">T&C</a>
                                </div>
                                <div class="offer-discount">
                                    <form class="exchange-list" action="">
                                        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
                                        <label class="woex"> Buy without exchange  <span>₹105,900.00</span></label><br>
                                        <input type="checkbox" id="vehicle2" name="vehicle2" value="Car">
                                        <label class="wex"> Buy with exchange  <span>up to ₹23,300 off</span></label><br>
                                      </form>
                                </div>
                                <div class="clear-flex"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
            <br/>
        
        <div class="modal fade" id="chatting_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-faded-info">
                        <h5 class="modal-title" id="exampleModalLongTitle">Send Message to seller</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{config('app.url')}}/messages-store" method="post" id="chat-form">
                            <input type="hidden" name="_token" value="OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO" /><textarea name="message" class="form-control" required placeholder="Write here..."></textarea><br />
                            <div class="justify-content-end gap-2 d-flex flex-wrap">
                                <a href="{{route('customer.auth.login')}}" class="btn btn-soft-primary bg--secondary border">Go to chatbox</a><button class="btn btn--primary text-white" disabled="disabled">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Start Frequently-->
   
        @if($product_frequently_bought_together!=null && $product_frequently_bought_together->count() > 0)
            <section class="frequently-add">
                <div class="container">
                    <div class="frequently-tog">
                        <h2>Frequently bought Together</h2>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                
                                        @foreach ($product_frequently_bought_together as $data)
                                            <div class="col-sm-4">
                                                <div class="flash-deal-list">
                                                
                                                    <a href="product/{{$data->slug}}">
                                                        <img src=" {{asset('storage/app/public/product/thumbnail')}}/{{$data->thumbnail}}" alt="deals" />
                                                    </a>
                                                    <span class="dicount-perstg">
                                                        {{number_format((($data->purchase_price-$data->unit_price)/($data->unit_price))*100, 0)}}% Off
                                                    </span>
            
                                                
                                                    <span class="favrate-icon">
                                                        <a href="#"><i class="navbar-tool-icon czi-heart"></i></a>
                                                    </span>
                                                    <div class="deal-info">
                                                        <div class="star-rating" style="margin-right: 10px;">
                                                            <i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i><i class="tio-star-outlined text-warning"></i>
                                                            <i class="tio-star-outlined text-warning"></i>
                                                        </div>
                                                        <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">(0)</span>
                                                        <h2><a href="product/{{$data->slug}}">{{$data->name}}</a></h2>
                                                        <h3>
                                                            <span class="mrp-price">₹{{$data->unit_price}}</span>
                                                            <span class="disc-price"> ₹{{$data->purchase_price}}</span>
                                                        </h3>
                                                    </div>
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        @endforeach
                                    
                                    
                                    
                                </div>
                            </div>
                        
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-6">
                                        @if($product_frequently_bought_together!=null && $product_frequently_bought_together->count() > 0)
                                            @php
                                                $totalPrice = 0;
                                                foreach ($product_frequently_bought_together as $product_p) {
                                                    $totalPrice += $product_p['purchase_price'];
                                                }
                                                $formattedTotalPrice = number_format($totalPrice, 2); // Format the total price with 2 decimal places
                                            @endphp
                                            <div class="add-all-to-cart">
                                                <h4>₹{{ $formattedTotalPrice }}</h4>    
                                        
                                                <p>for {{$product_frequently_bought_together->count()}} Items</p>
                                                <button class="btn btn--primary element-center btn-gap-right" onclick="addToCart()" type="button">
                                                    <span class="string-limit"><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</span>
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="protect-plan">
                                            <p>Protect Your Product <span>₹900.00</span></p>
                                            <form class="extra-add-prot">
                                                <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
                                                <label for="html">Add to the cart </label>
                                                <p>Add warranty for 1 year </p>
                                            </form>
                                            <img src="{{config('app.asset_url')}}/public/deals/assets/images/osg.jpg" alt="">
                                        </div> 
                                    </div>
                                </div>
                            </div>

                        </div>
                    
                    </div>
                </div>        
            </section>
        @endif

        <!--End Frequently-->
      
        <!--Start faq-->
        <section class="faq-sec">
            <div class="container">
                <div class="mt-3 __cate-side-arrordion">
                    <div>
                        <div class="accordion mt-n1 __cate-side-price" id="shop-categories">
                            <div class="menu--caret-accordion">
                                <div class="card-header flex-between">
                                    <div><label class="for-hover-lable cursor-pointer" href="#">Specifications</label></div>
                                    <div class="px-2 cursor-pointer menu--caret">
                                        <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                    </div>
                                </div>
                                <div class="card-body p-0 ml-2" id="collapse-6" style="display: none;">
                                    <p class="gen">General</p>
                                    
                                    <ul class="expandible general-info">
                                        <li><span class="product_info">Brand </span>{{isset($product->brand) ? $product->brand->default_name : translate('brand_not_found')}}</li>
                                        <li><span class="product_info">Category </span>{{isset($product->category) ? $product->category->default_name : translate('category_not_found')}}</li>
                                        <li><span class="product_info">Product Type </span>{{translate($product->product_type)}}</li>
                                        <li><span class="product_info">Item Code </span>{{$product->item_code}}</li>
                                       
                                        @if($product->product_type == 'physical')
                                            <li><span class="product_info">Current Stock </span>{{$product->current_stock}}</li>
                                        @endif
                                        <li><span class="product_info">SKU </span>{{$product->code}}</li>
                                        @if (json_decode($product->specifications) != null)
                                            @foreach (json_decode($product->specifications) as $spec)
                                                <li><span class="product_info">{{$spec->name}} </span>{{$spec->description}}</li>
                                            @endforeach
                                        @endif 
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="accordion mt-n1 __cate-side-price" id="shop-categories">
                            <div class="menu--caret-accordion">
                                <div class="card-header flex-between">
                                    <div><label class="for-hover-lable cursor-pointer" href="#">Description</label></div>
                                    <div class="px-2 cursor-pointer menu--caret">
                                        <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                    </div>
                                </div>
                                <div class="card-body p-0 ml-2" id="collapse-6" style="display: none;">
                                  
                                    {!! $product->details !!}

                                </div>
                            </div>
                        </div>
                        <!-- <div class="accordion mt-n1 __cate-side-price" id="shop-categories">
                            <div class="menu--caret-accordion">
                                <div class="card-header flex-between">
                                    <div><label class="for-hover-lable cursor-pointer" href="#">Features</label></div>
                                    <div class="px-2 cursor-pointer menu--caret">
                                        <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                    </div>
                                </div>
                                <div class="card-body p-0 ml-2" id="collapse-6" style="display: none;">
                                    <p class="gen">General</p>
                                   <ul class="expandible general-info">
                                        <li><span class="product_info">Brand </span>{{isset($product->brand) ? $product->brand->default_name : translate('brand_not_found')}}</li>
                                        <li><span class="product_info">Category </span>{{isset($product->category) ? $product->category->default_name : translate('category_not_found')}}</li>
                                        <li><span class="product_info">Product Type </span>{{translate($product->product_type)}}</li>
                                        @if($product->product_type == 'physical')
                                            <li><span class="product_info">Current Stock </span>{{$product->current_stock}}</li>
                                        @endif
                                        <li><span class="product_info">SKU </span>{{$product->code}}</li>
                                        @if (json_decode($product->specifications) != null)
                                            @foreach (json_decode($product->specifications) as $spec)
                                                <li><span class="product_info">{{$spec->name}} </span>{{$spec->description}}</li>
                                            @endforeach
                                        @endif 
                                    </ul>
                                </div>
                            </div>
                        </div> -->
                        <div class="accordion mt-n1 __cate-side-price" id="shop-categories">
                            <div class="menu--caret-accordion">
                                <div class="card-header flex-between">
                                    <div><label class="for-hover-lable cursor-pointer" href="#">Review</label></div>
                                    <div class="px-2 cursor-pointer menu--caret">
                                        <strong class="pull-right for-brand-hover"><i class="tio-next-ui fs-13"></i></strong>
                                    </div>
                                </div>
                                <div class="card-body p-0 ml-2" id="collapse-6" style="display: none;">
                                <table>
                                    <tr>
                                        <th>Rating</th>
                                        <th>Customer</th>
                                        <th>Comment</th>
                                    </tr>
                                    @foreach($product->reviews as $product_review)
                                        <tr>
                                            <td>{{$product_review->rating}}</td>
                                            <td>{{$product_review->customer_id}}</td>
                                            <td>{{$product_review->comment}}</td>
                                        </tr>
                                    @endforeach
                                    
                                </table>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Faq-->

        <!--Start-->
        <section class="new-arrival-section featured_products you-like">
            <div class="container rtl mt-4">
                <div class="card-body">
                    <div class="section-header">
                        <div class="arrival-title d-block">
                            <div class="text-capitalize text-align-left"><h2>You may also like</h2></div>
                        </div>
                    </div>
            
                    <div class="py-2">
                        <div class="new_arrival_product">
                            <div class="carousel-wrap">
                                <div class="new-arrivals-product_my">
                                    <ul class="my_new-arrivals-product">
                                        @if ($relatedProducts->count() > 0)
                                            @foreach($relatedProducts as $featured_product)
                                                @include('layouts.ui.product')
                                            
                                            @endforeach
                                        @endif
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End-->
        
         <!--Start-->
         @include('layouts.ui.social_media')
        <!--End-->

        <br/>
           @include('layouts.ui.delivery')
           @include('layouts.ui.footer')
        
        <div class="modal fade" id="remove-wishlist-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/remove-wishlist.png" alt="" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220">Product has been removed from wishlist</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="outof-stock-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/out-of-stock.png" alt="" class="mw-100px" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220" id="outof-stock-modal-message">Out of stock</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="add-wishlist-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/added-wishlist.png" alt="" />
                            <h6 class="font-semibold mt-3 mb-4 mx-auto __max-w-220">Product added to wishlist</h6>
                        </div>
                        <div class="d-flex gap-3 justify-content-center"><a href="javascript:" class="btn btn--primary __rounded-10" data-dismiss="modal">Okay</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="login-alert-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/locked-icon.svg" alt="" />
                            <h6 class="font-semibold mt-3 mb-1">Please Login</h6>
                            <p class="mb-4"><small>You need to login to view this feature</small></p>
                        </div>
                        <div class="d-flex gap-3 justify-content-center">
                            <button class="btn btn-soft-secondary bg--secondary __rounded-10" data-dismiss="modal">Cancel</button><a href="{{route('customer.auth.login')}}" class="btn btn-primary __rounded-10">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="remove-address">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body pb-5">
                        <div class="text-center">
                            <img src="{{config('app.asset_url')}}/public/deals/assets/front-end/img/icons/remove-address.png" alt="" />
                            <h6 class="font-semibold mt-3 mb-1">Delete this address?</h6>
                            <p class="mb-4"><small>This address will be removed from this list</small></p>
                        </div>
                        <div class="d-flex gap-3 justify-content-center">
                            <a href="javascript:" class="btn btn-primary __rounded-10" id="remove-address-link">Remove</a><button class="btn btn-soft-secondary bg--secondary __rounded-10" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn-scroll-top btn--primary" href="#top" data-scroll><span class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Top</span><i class="btn-scroll-top-icon czi-arrow-up"></i></a>

        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/jquery/dist/jquery-2.2.4.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/simplebar/dist/simplebar.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/tiny-slider/dist/min/tiny-slider.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/js/lightbox.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/drift-zoom/dist/Drift.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/lightgallery.js/dist/js/lightgallery.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/lg-video.js/dist/lg-video.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/back-end/js/toastr.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/theme.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/custom.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/slick.min.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/sweet_alert.js"></script>
        <script src="{{config('app.asset_url')}}/public/deals/assets/back-end/js/toastr.js"></script>
        <script type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                const $stickyElement = $(".bottom-sticky_ele");
                const $offsetElement = $(".bottom-sticky_offset");

                if ($stickyElement.length !== 0) {
                    $(window).on("scroll", function () {
                        const elementOffset = $offsetElement.offset().top - $(window).height() / 1.2;
                        const scrollTop = $(window).scrollTop();

                        if (scrollTop >= elementOffset) {
                            $stickyElement.addClass("stick");
                        } else {
                            $stickyElement.removeClass("stick");
                        }
                    });
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".password-toggle-btn").on("click", function () {
                    let checkbox = $(this).find("input[type=checkbox]");
                    let eyeIcon = $(this).find("i");
                    checkbox.change(function () {
                        if (checkbox.is(":checked")) {
                            eyeIcon.removeClass("tio-hidden").addClass("tio-invisible");
                        } else {
                            eyeIcon.removeClass("tio-invisible").addClass("tio-hidden");
                        }
                    });
                });
            });
        </script>
        <script>
            toastr.options = {
                closeButton: !1,
                debug: !1,
                newestOnTop: !1,
                progressBar: !1,
                positionClass: "toast-top-right",
                preventDuplicates: !1,
                onclick: null,
                showDuration: "300",
                hideDuration: "1000",
                timeOut: "5000",
                extendedTimeOut: "1000",
                showEasing: "swing",
                hideEasing: "linear",
                showMethod: "fadeIn",
                hideMethod: "fadeOut",
            };
        </script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

        <script>
            function addWishlist(product_id, modalId) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    url: "{{config('app.url')}}/store-wishlist",
                    method: "POST",
                    data: {
                        product_id: product_id,
                    },
                    success: function (data) {
                        if (data.value == 1) {
                            $(".countWishlist").html(data.count);
                            $(".countWishlist-" + product_id).text(data.product_count);
                            $(".tooltip").html("");
                            $(`.wishlist_icon_${product_id}`).removeClass("fa fa-heart-o").addClass("fa fa-heart");
                            $("#add-wishlist-modal").modal("show");
                            $(`#${modalId}`).modal("show");
                        } else if (data.value == 2) {
                            $("#remove-wishlist-modal").modal("show");
                            $(".countWishlist").html(data.count);
                            $(".countWishlist-" + product_id).text(data.product_count);
                            $(`.wishlist_icon_${product_id}`).removeClass("fa fa-heart").addClass("fa fa-heart-o");
                        } else {
                            $("#login-alert-modal").modal("show");
                        }
                    },
                });
            }
            $('#deliver_button').click(function(){
                var pincode = $('#deliver_pincode').val(); // Get the value from the input field
                console.log(pincode); // Log the pincode value to the console
                var product_id = '{{$product->id}}'; // Make sure this PHP variable is properly set

                // Make AJAX request
                $.ajax({
                    url: '{{ route("get_courier_serviceable") }}',
                    method: 'GET', // or 'GET' depending on your API
                    data: { 
                        delivery_postcode: pincode,
                        product_id: product_id 
                    },// Send pincode as data
                    success: function(response) {
                        $('#delivery_status_message').text(response);

                        console.log(response);
                      // Log the response from the API
                        // Handle the response as needed
                    },
                    error: function(xhr, status, error) {
                        console.error(xhr.responseText); // Log any error messages
                        // Handle errors as needed
                    }
                });
            });

            function removeWishlist(product_id, modalId) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    url: "{{config('app.url')}}/delete-wishlist",
                    method: "POST",
                    data: {
                        id: product_id,
                    },
                    beforeSend: function () {
                        $("#loading").show();
                    },
                    success: function (data) {
                        $(`#${modalId}`).modal("show");

                        $(".countWishlist").html(parseInt($(".countWishlist").html()) - 1);
                        $("#row_id" + product_id).hide();
                        $(".tooltip").html("");
                        if (parseInt($(".countWishlist").html()) % 15 === 0) {
                            if ($("#wishlist_paginated_page").val() == 1) {
                                $("#set-wish-list").empty().append(`
                            <center>
                                <h6 class="text-muted">
                                    No data found.
                                </h6>
                            </center>
                        `);
                            } else {
                                let page_value = $("#wishlist_paginated_page").val();
                                window.location.href = "{{config('app.url')}}/wishlists?page=" + (page_value - 1);
                            }
                        }
                    },
                    complete: function () {
                        $("#loading").hide();
                    },
                });
            }

            function quickView(product_id) {
                $.get({
                    url: "{{config('app.url')}}/quick-view",
                    dataType: "json",
                    data: {
                        product_id: product_id,
                    },
                    beforeSend: function () {
                        $("#loading").show();
                    },
                    success: function (data) {
                        console.log("success...");
                        $("#quick-view").modal("show");
                        $("#quick-view-modal").empty().html(data.view);
                    },
                    complete: function () {
                        $("#loading").hide();
                    },
                });
            }

            function addToCart(form_id = "add-to-cart-form", redirect_to_checkout = false) {
                if (checkAddToCartValidity()) {
                    var data_json={
                        id:$('#product_id').val(),
                       quantity:'1'
                    };
                    $.ajaxSetup({
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                        },
                    });
                    $.post({
                        url: '{{ route('cart.add') }}',
                        data: data_json,
                        beforeSend: function () {
                            $("#loading").show();
                        },
                        success: function (response) {
                            console.log(response);
                            if (response.status == 1) {
                                updateNavCart();
                                toastr.success(response.message, {
                                    CloseButton: true,
                                    ProgressBar: true,
                                });
                                $(".call-when-done").click();
                                if (redirect_to_checkout) {
                                    location.href = "{{route('checkout-details')}}";
                                }
                                return false;
                            } else if (response.status == 0) {
                                $("#outof-stock-modal-message").html(response.message);
                                $("#outof-stock-modal").modal("show");
                                return false;
                            }
                        },
                        complete: function () {
                            $("#loading").hide();
                        },
                    });
                } else {
                    Swal.fire({
                        type: "info",
                        title: "Cart",
                        text: "Please choose all the options",
                    });
                }
            }

            function buy_now() {
                addToCart("add-to-cart-form", true);
                /* location.href = "{{config('app.url')}}/checkout-details"; */
            }

            function currency_change(currency_code) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    type: "POST",
                    url: "{{config('app.url')}}/currency",
                    data: {
                        currency_code: currency_code,
                    },
                    success: function (data) {
                        toastr.success("Currency changed to" + data.name);
                        location.reload();
                    },
                });
            }

            function removeFromCart(key) {
                $.post("cart/remove.html", { _token: "OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO", key: key }, function (response) {
                    $("#cod-for-cart").hide();
                    updateNavCart();
                    $("#cart-summary").empty().html(response.data);
                    toastr.info("Item has been removed from cart", {
                        CloseButton: true,
                        ProgressBar: true,
                    });
                    let segment_array = window.location.pathname.split("index.html");
                    let segment = segment_array[segment_array.length - 1];
                    if (segment === "checkout-payment" || segment === "checkout-details") {
                        location.reload();
                    }
                });
            }
            
            function updateNavCart() {
                $.post('{{route('cart.nav-cart')}}', {_token: '{{csrf_token()}}'}, function (response) {
                    $('#cart_items').html(response.data);
                });
            }
            /*new*/
            $("#add-to-cart-form").on("submit", function (e) {
                e.preventDefault();
            });

            /*new*/
            function cartQuantityInitialize() {
                $(".btn-number").click(function (e) {
                    e.preventDefault();

                    fieldName = $(this).attr("data-field");
                    type = $(this).attr("data-type");
                    productType = $(this).attr("product-type");
                    var input = $("input[name='" + fieldName + "']");
                    var currentVal = parseInt($(".input-number").val());
                    // alert(currentVal);
                    if (!isNaN(currentVal)) {
                        // console.log(productType)
                        if (type == "minus") {
                            if (currentVal > $(".input-number").attr("min")) {
                                $(".input-number")
                                    .val(currentVal - 1)
                                    .change();
                            }
                            if (parseInt($(".input-number").val()) == $(".input-number").attr("min")) {
                                $(this).attr("disabled", true);
                            }
                        } else if (type == "plus") {
                            // alert('ok out of stock');
                            if (currentVal < $(".input-number").attr("max") || productType === "digital") {
                                $(".input-number")
                                    .val(currentVal + 1)
                                    .change();
                            }

                            if (parseInt(input.val()) == $(".input-number").attr("max") && productType === "physical") {
                                $(this).attr("disabled", true);
                            }
                        }
                    } else {
                        $(".input-number").val(0);
                    }
                });

                $(".input-number").focusin(function () {
                    $(this).data("oldValue", $(this).val());
                });

                $(".input-number").change(function () {
                    productType = $(this).attr("product-type");
                    minValue = parseInt($(this).attr("min"));
                    maxValue = parseInt($(this).attr("max"));
                    valueCurrent = parseInt($(this).val());
                    var name = $(this).attr("name");
                    if (valueCurrent >= minValue) {
                        $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr("disabled");
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: "Cart",
                            text: "Sorry the minimum order quantity does not match",
                        });
                        $(this).val($(this).data("oldValue"));
                    }
                    if (productType === "digital" || valueCurrent <= maxValue) {
                        $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr("disabled");
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: "Cart",
                            text: "Sorry stock limit exceeded.",
                        });
                        $(this).val($(this).data("oldValue"));
                    }
                });
                $(".input-number").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if (
                        $.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                        // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Allow: home, end, left, right
                        (e.keyCode >= 35 && e.keyCode <= 39)
                    ) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
            }

            function updateQuantity(key, element) {
                $.post(
                    "cart/updateQuantity.html",
                    {
                        _token: "OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO",
                        key: key,
                        quantity: element.value,
                    },
                    function (data) {
                        updateNavCart();
                        $("#cart-summary").empty().html(data);
                    }
                );
            }

            function updateCartQuantity(cart_id, product_id, action, event) {
                let remove_url = $("#remove_from_cart_url").data("url");
                let update_quantity_url = $("#update_quantity_url").data("url");
                let token = $('meta[name="_token"]').attr("content");
                let product_qyt = parseInt($(`.cartQuantity${cart_id}`).val()) + parseInt(action);
                let cart_quantity_of = $(`.cartQuantity${cart_id}`);
                let segment_array = window.location.pathname.split("index.html");
                let segment = segment_array[segment_array.length - 1];

                if (cart_quantity_of.val() == 0) {
                    toastr.info($(".cannot_use_zero").data("text"), {
                        CloseButton: true,
                        ProgressBar: true,
                    });
                    cart_quantity_of.val(cart_quantity_of.data("min"));
                } else if (cart_quantity_of.val() == cart_quantity_of.data("min") && event == "minus") {
                    $.post(
                        remove_url,
                        {
                            _token: token,
                            key: cart_id,
                        },
                        function (response) {
                            updateNavCart();
                            toastr.info(response.message, {
                                CloseButton: true,
                                ProgressBar: true,
                            });
                            if (segment === "shop-cart" || segment === "checkout-payment" || segment === "checkout-details") {
                                location.reload();
                            }
                        }
                    );
                } else {
                    if (cart_quantity_of.val() < cart_quantity_of.data("min")) {
                        let min_value = cart_quantity_of.data("min");
                        toastr.error("Minimum order quantity cannot be less than " + min_value);
                        cart_quantity_of.val(min_value);
                        updateCartQuantity(cart_id, product_id, action, event);
                    } else {
                        $(`.cartQuantity${cart_id}`).html(product_qyt);
                        $.post(
                            update_quantity_url,
                            {
                                _token: token,
                                key: cart_id,
                                product_id: product_id,
                                quantity: product_qyt,
                            },
                            function (response) {
                                if (response["status"] == 0) {
                                    toastr.error(response["message"]);
                                } else {
                                    toastr.success(response["message"]);
                                }
                                response["qty"] <= 1 ? $(`.quantity__minus${cart_id}`).html('<i class="tio-delete-outlined text-danger fs-10"></i>') : $(`.quantity__minus${cart_id}`).html('<i class="tio-remove fs-10"></i>');

                                $(`.cartQuantity${cart_id}`).val(response["qty"]);
                                $(`.cartQuantity${cart_id}`).html(response["qty"]);
                                $(`.cart_quantity_multiply${cart_id}`).html(response["qty"]);
                                $(".cart_total_amount").html(response.total_price);
                                $(`.discount_price_of_${cart_id}`).html(response["discount_price"]);
                                $(`.quantity_price_of_${cart_id}`).html(response["quantity_price"]);
                                $(`.total_discount`).html(response["total_discount_price"]);
                                $(`.free_delivery_amount_need`).html(response.free_delivery_status.amount_need);
                                if (response.free_delivery_status.amount_need <= 0) {
                                    $(".amount_fullfill").removeClass("d-none");
                                    $(".amount_need_to_fullfill").addClass("d-none");
                                } else {
                                    $(".amount_fullfill").addClass("d-none");
                                    $(".amount_need_to_fullfill").removeClass("d-none");
                                }
                                const progressBar = document.querySelector(".progress-bar");
                                progressBar.style.width = response.free_delivery_status.percentage + "%";
                                if (response["qty"] == cart_quantity_of.data("min")) {
                                    cart_quantity_of.parent().find(".quantity__minus").html('<i class="tio-delete-outlined text-danger fs-10"></i>');
                                } else {
                                    cart_quantity_of.parent().find(".quantity__minus").html('<i class="tio-remove fs-10"></i>');
                                }
                                if (segment === "shop-cart" || segment === "checkout-payment" || segment === "checkout-details") {
                                    location.reload();
                                }
                            }
                        );
                    }
                }
            }
            $("#add-to-cart-form input").on("change", function () {
                getVariantPrice();
            });

            function getVariantPrice() {
                if ($("#add-to-cart-form input[name=quantity]").val() > 0 && checkAddToCartValidity()) {
                    $.ajaxSetup({
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                        },
                    });
                    $.ajax({
                        type: "POST",
                        url: "{{config('app.url')}}/cart/variant_price",
                        data: $("#add-to-cart-form").serializeArray(),
                        success: function (data) {
                            $("#add-to-cart-form #chosen_price_div").removeClass("d-none");
                            $("#add-to-cart-form #chosen_price_div #chosen_price").html(data.price);
                            $("#chosen_price_mobile").html(data.price);
                            $("#set-tax-amount-mobile").html(data.tax);
                            $("#set-tax-amount").html(data.tax);
                            $("#set-discount-amount").html(data.discount);
                            $("#available-quantity").html(data.quantity);
                            $(".cart-qty-field").attr("max", data.quantity);
                        },
                    });
                }
            }

            function checkAddToCartValidity() {
                var names = {};
                $("#add-to-cart-form input:radio").each(function () {
                    // find unique names
                    names[$(this).attr("name")] = true;
                });
                var count = 0;
                $.each(names, function () {
                    // then count them
                    count++;
                });
                if ($("input:radio:checked").length == count) {
                    return true;
                }
                return false;
            }

            $(".clickable").click(function () {
                window.location = $(this).find("a").attr("href");
                return false;
            });
        </script>
        <script>
            function couponCode() {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    type: "POST",
                    url: "{{config('app.url')}}/coupon/apply",
                    data: $("#coupon-code-ajax").serializeArray(),
                    success: function (data) {
                        /* console.log(data);
                return false; */
                        if (data.status == 1) {
                            let ms = data.messages;
                            ms.forEach(function (m, index) {
                                toastr.success(m, index, {
                                    CloseButton: true,
                                    ProgressBar: true,
                                });
                            });
                        } else {
                            let ms = data.messages;
                            ms.forEach(function (m, index) {
                                toastr.error(m, index, {
                                    CloseButton: true,
                                    ProgressBar: true,
                                });
                            });
                        }
                        setInterval(function () {
                            location.reload();
                        }, 2000);
                    },
                });
            }

            jQuery(".search-bar-input").keyup(function () {
                $(".search-card").css("display", "block");
                let name = $(".search-bar-input").val();
                if (name.length > 0) {
                    $.get({
                        url: "{{config('app.url')}}/searched-products",
                        dataType: "json",
                        data: {
                            name: name,
                        },
                        beforeSend: function () {
                            $("#loading").show();
                        },
                        success: function (data) {
                            $(".search-result-box").empty().html(data.result);
                        },
                        complete: function () {
                            $("#loading").hide();
                        },
                    });
                } else {
                    $(".search-result-box").empty();
                }
            });

            jQuery(".search-bar-input-mobile").keyup(function () {
                $(".search-card").css("display", "block");
                let name = $(".search-bar-input-mobile").val();
                if (name.length > 0) {
                    $.get({
                        url: "{{config('app.url')}}/searched-products",
                        dataType: "json",
                        data: {
                            name: name,
                        },
                        beforeSend: function () {
                            $("#loading").show();
                        },
                        success: function (data) {
                            $(".search-result-box").empty().html(data.result);
                        },
                        complete: function () {
                            $("#loading").hide();
                        },
                    });
                } else {
                    $(".search-result-box").empty();
                }
            });

            jQuery(document).mouseup(function (e) {
                var container = $(".search-card");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    container.hide();
                }
            });

            function route_alert(route, message) {
                Swal.fire({
                    title: "Are you sure?",
                    text: message,
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonColor: "default",
                    confirmButtonColor: "#ec1c24",
                    cancelButtonText: "No",
                    confirmButtonText: "Yes",
                    reverseButtons: true,
                }).then((result) => {
                    if (result.value) {
                        location.href = route;
                    }
                });
            }

            function order_again(order_id) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    type: "POST",
                    url: $("#order_again_url").data("url"),
                    data: {
                        order_id,
                    },
                    beforeSend: function () {
                        $("#loading").show();
                    },
                    success: function (response) {
                        if (response.status === 1) {
                            updateNavCart();
                            toastr.success(response.message, {
                                CloseButton: true,
                                ProgressBar: true,
                                timeOut: 3000, // duration
                            });
                            location.href = response.redirect_url;
                            return false;
                        } else if (response.status === 0) {
                            toastr.warning(response.message, {
                                CloseButton: true,
                                ProgressBar: true,
                                timeOut: 2000, // duration
                            });
                            return false;
                        }
                    },
                    complete: function () {
                        $("#loading").hide();
                    },
                });
            }
        </script>
        <script>
            $(".filter-show-btn").on("click", function () {
                $("#shop-sidebar").toggleClass("show active");
            });
            $(".cz-sidebar-header .close").on("click", function () {
                $("#shop-sidebar").removeClass("show active");
            });
            $(".remove-address-by-modal").on("click", function () {
                let link = $(this).data("link");
                $("#remove-address-link").attr("href", link);
                $("#remove-address").modal("show");
            });
        </script>
        <script>
            let cookie_content = `
        <div class="cookie-section">
            <div class="container">
                <div class="d-flex flex-wrap align-items-center justify-content-between column-gap-4 row-gap-3">
                    <div class="text-wrapper">
                        <h5 class="title">Your Privacy Matter</h5>
                        <div></div>
                    </div>
                    <div class="btn-wrapper">
                        <span class="text-white cursor-pointer" id="cookie-reject">No thanks</span>
                        <button class="btn btn-success cookie-accept" id="cookie-accept">Yes i Accept</button>
                    </div>
                </div>
            </div>
        </div>
    `;
            $(document).on("click", "#cookie-accept", function () {
                document.cookie = "6valley_cookie_consent=accepted; max-age=" + 60 * 60 * 24 * 30;
                $("#cookie-section").hide();
            });
            $(document).on("click", "#cookie-reject", function () {
                document.cookie = "6valley_cookie_consent=reject; max-age=" + 60 * 60 * 24;
                $("#cookie-section").hide();
            });

            $(document).ready(function () {
                if (document.cookie.indexOf("6valley_cookie_consent=accepted") !== -1) {
                    $("#cookie-section").hide();
                } else {
                    $("#cookie-section").html(cookie_content).show();
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                // const currentUrl = new URL(window.location.html);
                // console.log(currentUrl);
                // const referral_code_parameter = new URLSearchParams(currentUrl.search).get("referral_code");
                // if (referral_code_parameter) {
                //     window.location.href = "{{config('app.url')}}/customer/auth/sign-up?referral_code=" + referral_code_parameter;
                // }
            });
        </script>
        <script>
            var $bgImg = $("[data-bg-img]");
            $bgImg
                .css("background-image", function () {
                    return 'url("' + $(this).data("bg-img") + '")';
                })
                .removeAttr("data-bg-img")
                .addClass("bg-img");
        </script>
        <script>
            $(document).ready(function () {
                const $stickyElement = $(".bottom-sticky");
                const $offsetElement = $(".product-details-shipping-details");

                $(window).on("scroll", function () {
                    const elementOffset = $offsetElement.offset().top;
                    const scrollTop = $(window).scrollTop();

                    if (scrollTop >= elementOffset) {
                        $stickyElement.addClass("stick");
                    } else {
                        $stickyElement.removeClass("stick");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function showInstaImage(t) {
                $("#attachment-view").attr("src", t), $("#show-modal-view").modal("toggle");
            }
            function focus_preview_image_by_color(t) {
                $('a[href="#image' + t + '"]')[0].click();
            }
            cartQuantityInitialize(),
                getVariantPrice(),
                $("#add-to-cart-form input").on("change", function () {
                    getVariantPrice();
                });
        </script>
        <script>
            let load_review_count = 1;
            function load_review() {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
                    },
                });
                $.ajax({
                    type: "post",
                    url: "{{config('app.url')}}/review-list-product",
                    data: {
                        product_id: 1,
                        offset: load_review_count,
                    },
                    success: function (data) {
                        $("#product-review-list").append(data.productReview);
                        if (data.checkReviews == 0) {
                            $(".view_more_button").removeClass("d-none").addClass("d-none");
                        } else {
                            $(".view_more_button").addClass("d-none").removeClass("d-none");
                        }
                    },
                });
                load_review_count++;
            }
        </script>
        <script>
            $("#chat-form").on("submit", function (e) {
                e.preventDefault(),
                    $.ajaxSetup({ headers: { "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content") } }),
                    $.ajax({
                        type: "post",
                        url: "{{config('app.url')}}/messages-store",
                        data: $("#chat-form").serialize(),
                        success: function (e) {
                            toastr.success("Message send successfully", { CloseButton: !0, ProgressBar: !0 }), $("#chat-form").trigger("reset");
                        },
                    });
            });
        </script>
        <script type="text/javascript" src="platform-api.sharethis.com/js/sharethis.js#property=5f55f75bde227f0012147049&product=sticky-share-buttons" async="async"></script>
        <script>
            function myFunction() {
                $("#anouncement").slideUp(300);
            }
            $(".category-menu").find(".mega_menu").parents("li").addClass("has-sub-item").find("> a").append("<i class='czi-arrow-right'></i>"),
                $(".category-menu-toggle-btn").on("click", function () {
                    $(".megamenu-wrap").toggleClass("show");
                }),
                $(".navbar-tool-icon-box").on("click", function () {
                    $(".megamenu-wrap").removeClass("show");
                }),
                $(window).on("scroll", function () {
                    $(".megamenu-wrap").removeClass("show");
                });
        </script>
        <script>
            $(".close-search-form-mobile").on("click", function () {
                $(".search-form-mobile").removeClass("active");
            }),
                $(".open-search-form-mobile").on("click", function () {
                    $(".search-form-mobile").addClass("active");
                });
        </script>




<script>
    
    $(".menu--caret").on("click", function (e) {
        var element = $(this).closest(".menu--caret-accordion");
        if (element.hasClass("open")) {
            element.removeClass("open");
            element.find(".menu--caret-accordion").removeClass("open");
            element.find(".card-body").slideUp(300, "swing");
        } else {
            element.addClass("open");
            element.children(".card-body").slideDown(300, "swing");
            element.siblings(".menu--caret-accordion").children(".card-body").slideUp(300, "swing");
            element.siblings(".menu--caret-accordion").removeClass("open");
            element.siblings(".menu--caret-accordion").find(".menu--caret-accordion").removeClass("open");
            element.siblings(".menu--caret-accordion").find(".card-body").slideUp(300, "swing");
        }
    });
</script>
<script>
    $('ul.expandible').each(function(){
    var $ul = $(this),
        $lis = $ul.find('li:gt(6)'),
        isExpanded = $ul.hasClass('expanded');
    $lis[isExpanded ? 'show' : 'hide']();
    
    if($lis.length > 0){
        $ul
            .append($('<span class="showmore"><li class="expand">' + (isExpanded ? 'Show Less specs' : 'Show full specs') + '</li></span>')
            .click(function(event){
                var isExpanded = $ul.hasClass('expanded');
                event.preventDefault();
                $(this).html(isExpanded ? 'Show full specs' : 'Show Less specs');
                $ul.toggleClass('expanded');
                $lis.toggle();
            }));
    }
});
</script>  
    </body>
</html>