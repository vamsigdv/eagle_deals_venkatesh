
<li>
    <div class="flash-deal-list">
        <a href="product/{{$featured_product->slug}}"><img src=" {{asset('storage/app/public/product/thumbnail')}}/{{$featured_product->thumbnail}}" alt="deals" /></a>
        
                <span class="dicount-perstg">
                    
                        @if ($featured_product->discount_type == 'percent')
                                        {{round($featured_product->discount, 0)}}%
                        @elseif($featured_product->discount_type =='flat')
                            {{\App\CPU\Helpers::currency_converter($featured_product->discount)}}
                        @endif
                        {{translate('off')}}
                    
                 </span>
           
       
        <span class="favrate-icon">
            <a onclick="addToWishlist({{$featured_product->id}})">
                @if(in_array($featured_product->id, $wishlist->toArray()))
                    <i class="navbar-tool-icon czi-heart filled"></i>
                @else
                    <i class="navbar-tool-icon czi-heart"></i>
                @endif
            </a>
        </span>
        <div class="deal-info">
            <div class="star-rating" style="margin-right: 10px;">
              
            @php
                $average = isset($featured_product['rating'][0]['average']) ? $featured_product['rating'][0]['average'] : 0;
                $full_stars = round($average);
                $empty_stars = 5 - $full_stars;
            @endphp

            @for ($i = 0; $i < $full_stars; $i++)
                <i class="tio-star text-warning"></i>
            @endfor

            @for ($i = 0; $i < $empty_stars; $i++)
                <i class="tio-star-outlined text-warning"></i>
            @endfor

            </div>
            <span class="d-inline-block align-middle mt-1 mr-md-2 mr-sm-0 fs-14 text-muted">({{$featured_product->reviews_count}})</span>
            <h2><a href="{{config('app.url')}}/product/{{$featured_product->slug}}">{{$featured_product->name}}</a></h2>
            <h3><span class="mrp-price">₹{{$featured_product->unit_price}}</span>
            
            <span class="disc-price"> {{\App\CPU\Helpers::currency_converter(
                $featured_product->unit_price-(\App\CPU\Helpers::get_product_discount($featured_product,$featured_product->unit_price))
                        )}}</span></h3>
        </div>
    </div>
</li>

<style>
    .czi-heart.filled {
        color: red; /* or any other color you prefer */
    }
</style>