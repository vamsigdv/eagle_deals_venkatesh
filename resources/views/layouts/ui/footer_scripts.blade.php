<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/jquery/dist/jquery-2.2.4.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/simplebar/dist/simplebar.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/tiny-slider/dist/min/tiny-slider.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/js/lightbox.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/drift-zoom/dist/Drift.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/lightgallery.js/dist/js/lightgallery.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/lg-video.js/dist/lg-video.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/back-end/js/toastr.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/theme.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/custom.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/slick.min.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/front-end/js/sweet_alert.js"></script>
<script src="{{config('app.asset_url')}}/public/deals/assets/back-end/js/toastr.js"></script>
<script>
    jQuery(".search-bar-input").keyup(function () {
        $(".search-card").css("display", "block");
        let name = $(".search-bar-input").val();
        if (name.length > 0) {
            $.get({
                url: "{{config('app.url')}}/searched-products",
                dataType: "json",
                data: {
                    name: name,
                },
                beforeSend: function () {
                    $("#loading").show();
                },
                success: function (data) {
                    $(".search-result-box").empty().html(data.result);
                },
                complete: function () {
                    $("#loading").hide();
                },
            });
        } else {
            $(".search-result-box").empty();
        }
    });

    jQuery(".search-bar-input-mobile").keyup(function () {
        $(".search-card").css("display", "block");
        let name = $(".search-bar-input-mobile").val();
        if (name.length > 0) {
            $.get({
                url: "{{config('app.url')}}/searched-products",
                dataType: "json",
                data: {
                    name: name,
                },
                beforeSend: function () {
                    $("#loading").show();
                },
                success: function (data) {
                    $(".search-result-box").empty().html(data.result);
                },
                complete: function () {
                    $("#loading").hide();
                },
            });
        } else {
            $(".search-result-box").empty();
        }
    });
</script>
<!-- 
<script src="{{asset('public/deals/assets/vendor/jquery/dist/jquery-2.2.4.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/bs-custom-file-input/dist/bs-custom-file-input.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/simplebar/dist/simplebar.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/tiny-slider/dist/min/tiny-slider.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/public/js/lightbox.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/drift-zoom/dist/Drift.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/lightgallery.js/dist/js/lightgallery.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/vendor/lg-video.js/dist/lg-video.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/back-end/js/toastr.js')}}"></script>
        <script src="{{asset('public/deals/assets/front-end/js/theme.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/front-end/js/custom.js')}}"></script>
        <script src="{{asset('public/deals/assets/front-end/js/slick.min.js')}}"></script>
        <script src="{{asset('public/deals/assets/front-end/js/sweet_alert.js')}}"></script>
        <script src="{{asset('public/deals/assets/back-end/js/toastr.js')}}"></script> -->