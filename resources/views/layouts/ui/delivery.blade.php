<div class="__inline-61">
            <div class="container rtl pb-4 pt-3 px-0 px-md-3">
                <div class="shipping-policy-web">
                    <div class="row g-3 justify-content-center mx-max-md-0">
                        <div class="col-md-3 d-flex justify-content-center px-max-md-0">
                            <div class="shipping-method-system">
                                <div class="text-center"><img class="mr-2 size-60" src="{{asset('public/deals/assets/public/company-reliability/index.html')}}" onerror='this.src="{{ asset("public/deals/assets/front-end/img/delivery_info.png") }}"' alt="" /></div>
                                <div class="text-center"><p class="m-0">Fast Delivery all across the country</p></div>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center px-max-md-0">
                            <div class="shipping-method-system">
                                <div class="text-center"><img class="mr-2 size-60" src="{{asset('public/deals/assets/public/company-reliability/index.html')}}" onerror='this.src="{{asset("public/deals/assets/front-end/img/safe_payment.png") }}"'
                                    alt="" /></div>
                                <div class="text-center"><p class="m-0">Safe Payment</p></div>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center px-max-md-0">
                            <div class="shipping-method-system">
                                <div class="text-center"><img class="mr-2 size-60" src="{{asset('public/deals/assets/public/company-reliability/index.html')}}" onerror='this.src="{{asset("public/deals/assets/front-end/img/return_policy.png") }}"' alt="" /></div>
                                <div class="text-center"><p class="m-0">7 Days Return Policy</p></div>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center px-max-md-0">
                            <div class="shipping-method-system">
                                <div class="text-center"><img class="mr-2 size-60" src="{{asset('public/deals/assets/public/company-reliability/index.html')}}" onerror='this.src="{{asset("public/deals/assets/front-end/img/authentic_product.png") }}"' alt="" /></div>
                                <div class="text-center"><p class="m-0">100% Authentic Products</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>