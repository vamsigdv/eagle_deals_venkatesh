<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8" />
        <title>Products</title>
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <link rel="apple-touch-icon" sizes="180x180" href="storage/app/public/company/2021-03-02-603df1634614f.html" />
        <link rel="icon" type="image/png" sizes="32x32" href="storage/app/public/company/2021-03-02-603df1634614f.html" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/simplebar/dist/simplebar.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/tiny-slider/dist/tiny-slider.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/drift-zoom/dist/drift-basic.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/vendor/lightgallery.js/dist/css/lightgallery.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/back-end/css/toastr.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/theme.min.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/slick.css" />
        <link rel="stylesheet" media="screen" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/back-end/css/toastr.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/master.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/css/lightbox.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/back-end/vendor/icon-set/style.css" />
        <meta property="og:image" content='{{config('app.url')}}/storage/app/public/company/{"id":12,"type":"company_web_logo","value":"2024-01-10-659e6d9ba8af2.webp","created_at":null,"updated_at":"2024-01-10T10:12:43.000000Z"}' />
        <meta property="og:title" content='Products of {"id":11,"type":"company_name","value":"Sameer Traders","created_at":null,"updated_at":"2021-02-27T18:11:53.000000Z"} ' />
        <meta property="og:url" content="index.html" />
        <meta property="og:description" content="this is about us page. hello and hi from about page description.." />
        <meta property="twitter:card" content='{{config('app.url')}}/storage/app/public/company/{"id":12,"type":"company_web_logo","value":"2024-01-10-659e6d9ba8af2.webp","created_at":null,"updated_at":"2024-01-10T10:12:43.000000Z"}' />
        <meta property="twitter:title" content='Products of {"id":11,"type":"company_name","value":"Sameer Traders","created_at":null,"updated_at":"2021-02-27T18:11:53.000000Z"}' />
        <meta property="twitter:url" content="index.html" />
        <meta property="twitter:description" content="this is about us page. hello and hi from about page description.." />
        <style>
            .for-count-value {
                right: 0.6875 rem;
            }
            .for-count-value {
                right: 0.6875 rem;
            }
            .for-brand-hover:hover {
                color: #ec1c24;
            }
            .for-hover-lable:hover {
                color: #ec1c24 !important;
            }
            .page-item.active .page-link {
                background-color: #ec1c24 !important;
            }
            .sidepanel {
                left: 0;
            }
            .sidepanel .closebtn {
                right: 25 px;
            }
        </style>
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/home.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/responsive1.css" />
        <link rel="stylesheet" href="{{config('app.asset_url')}}/public/deals/assets/front-end/css/style.css" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="_token" content="{{ csrf_token() }}">
        
        <style>
            .rtl {
                direction: ltr;
            }
            .dropdown-item:focus,
            .dropdown-item:hover {
                color: #ec1c24;
            }
            .dropdown-item.active,
            .dropdown-item:active {
                color: #000;
            }
            .navbar-light .navbar-tool-icon-box {
                color: #ec1c24;
            }
            .search_button {
                background-color: #ec1c24;
            }
            .mega-nav .nav-item .nav-link {
                color: #ec1c24 !important;
            }
            .checkbox-alphanumeric label:hover,
            .owl-theme .owl-nav [class*="owl-"] {
                border-color: #ec1c24;
            }
            ::-webkit-scrollbar-thumb:hover {
                background: #000 !important;
            }
            [type="radio"] {
                border: 0;
                clip: rect(0 0 0 0);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 1px;
            }
            [type="radio"] + span:after {
                box-shadow: 0 0 0 0.1em#000;
            }
            [type="radio"]:checked + span:after {
                background: #000;
                box-shadow: 0 0 0 0.1em#000;
            }
            .navbar-tool .navbar-tool-label {
                background-color: #ec1c24 !important;
            }
            .btn--primary {
                color: #fff;
                background-color: #ec1c24 !important;
                border-color: #ec1c24 !important;
            }
            .btn--primary:hover {
                color: #fff;
                background-color: #ec1c24 !important;
                border-color: #ec1c24 !important;
            }
            .btn-secondary {
                background-color: #000 !important;
                border-color: #000 !important;
            }
            .btn-outline-accent:hover {
                color: #fff;
                background-color: #ec1c24;
                border-color: #ec1c24;
            }
            .btn-outline-accent {
                color: #ec1c24;
                border-color: #ec1c24;
            }
            .text-accent {
                color: #ec1c24;
            }
            .text-base-2,
            a:hover {
                color: #000;
            }
            .active-menu,
            .text-base,
            .text-primary {
                color: #ec1c24 !important;
            }
            .page-item.active > .page-link {
                box-shadow: 0 0.5rem 1.125rem -0.425rem#ec1c24;
            }
            .page-item.active .page-link {
                background-color: #ec1c24;
            }
            .btn-outline-accent:not(:disabled):not(.disabled).active,
            .btn-outline-accent:not(:disabled):not(.disabled):active,
            .show > .btn-outline-accent.dropdown-toggle {
                background-color: #000;
                border-color: #000;
            }
            .btn-outline-primary {
                color: #ec1c24;
                border-color: #ec1c24;
            }
            .btn-outline-primary:hover {
                background-color: #000;
                border-color: #000;
            }
            .btn-outline-primary.focus,
            .btn-outline-primary:focus {
                box-shadow: 0 0 0 0#000;
            }
            .btn-outline-primary:not(:disabled):not(.disabled).active,
            .btn-outline-primary:not(:disabled):not(.disabled):active,
            .show > .btn-outline-primary.dropdown-toggle {
                background-color: #ec1c24;
                border-color: #ec1c24;
            }
            .btn-outline-primary:not(:disabled):not(.disabled).active:focus,
            .btn-outline-primary:not(:disabled):not(.disabled):active:focus,
            .show > .btn-outline-primary.dropdown-toggle:focus {
                box-shadow: 0 0 0 0#ec1c24;
            }
            .for-discoutn-value {
                background: #ec1c24;
            }
            .dropdown-menu {
                margin-left: -8px !important;
            }
            :root {
                --base: #ec1c24;
                --base-2: #000000;
            }
            span.badge-accent {
                color: var(--base);
                background-color: #ec1c2440;
            }
            span.badge-accent:hover {
                color: var(--base) !important;
            }
        </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap" rel="stylesheet">
    </head>
    <body class="toolbar-enabled">
        <span id="order_again_url" data-url="{{config('app.url')}}/cart/order-again"></span>
        <div class="modal-quick-view modal fade" id="quick-view" tabindex="-1">
            <div class="modal-dialog modal-xl"><div class="modal-content" id="quick-view-modal"></div></div>
        </div>
        <style>
            .for-count-value {
                color: #ec1c24;
            }
            .count-value {
                color: #ec1c24;
            }
            @media (min-width: 768px) {}
            @media (max-width: 767px) {
                .search_button .input-group-text i {
                    color: #ec1c24 !important;
                }
                .mega-nav1 {
                    color: #ec1c24 !important;
                }
                .mega-nav1 .nav-link {
                    color: #ec1c24 !important;
                }
            }
            @media (max-width: 471px) {
                .mega-nav1 {
                    color: #ec1c24 !important;
                }
                .mega-nav1 .nav-link {
                    color: #ec1c24 !important;
                }
            }
            /* .spinner-container {
                position: relative;
            }

            .spinner {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                display: none;
            } */
        </style>
       
<header class="box-shadow-sm rtl __inline-10">
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2"><a href="#">Brand Trust</a></div>
                        <div class="col-sm-2 text-center"><a href="#">15 days return policy</a></div>
                        <div class="col-sm-2 text-center"><a href="#">24 Hours Deliver</a></div>
                        <div class="col-sm-2 text-center">
                            <div class="topbar-text dropdown disable-autohide __language-bar text-capitalize">
                                <a class="topbar-link dropdown-toggle" href="#" data-toggle="dropdown">Help & Service</a>
                                <ul class="dropdown-menu dropdown-menu-left" style="text-align: left;">
                                    <li>
                                        <a class="dropdown-item pb-1" href="index.html"><span style="text-transform: capitalize;">Help</span></a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item pb-1" href="index.html"><span style="text-transform: capitalize;">Service</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-2 text-center"><a href="#">Buying Guide</a></div>
                        <div class="col-sm-2 text-right"><a href="#">Contact us</a></div>
                    </div>
                </div>
            </div>
            <div class="navbar-sticky bg-light mobile-head">
                <div class="navbar navbar-expand-md navbar-light">
                    <div class="container">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"><span class="navbar-toggler-icon"></span></button>
                        <a class="navbar-brand d-none d-sm-block mr-3 flex-shrink-0 __min-w-7rem" href="{{config('app.url')}}">
                            <img class="__inline-11" src="{{config('app.asset_url')}}/public/deals/assets/images/logo.jpg" onerror='this.src="{{config('app.asset_url')}}/public/deals/assets/images/logo.jpg"' alt="Sameer Traders" />
                        </a>
                        <a class="navbar-brand d-sm-none" href="index.html"><img class="mobile-logo-img __inline-12" src="#" onerror='this.src="{{config('app.asset_url')}}/public/deals/assets/images/logo.jpg"' alt="Sameer Traders" /></a>
                        <div class="input-group-overlay mx-lg-4 search-form-mobile" style="text-align: left;">
                            <div style="text-align: right;" class="d-lg-none">
                                <button class="btn close-search-form-mobile"><i class="tio-clear"></i></button>
                            </div>
                            <form action="{{config('app.url')}}/products" type="submit" class="search_form">
                                <input class="form-control appended-form-control search-bar-input" type="text" autocomplete="off" placeholder="Search here..." name="search_by_name" />
                                <button class="input-group-append-overlay search_button" type="submit" style="border-radius: 0 7px 7px 0; left: unset; right: 0; top: 0;">
                                    <span class="input-group-text __text-20px"><i class="czi-search text-white"></i></span>
                                </button>
                                <input name="data_from" value="search" hidden /> <input name="page" value="1" hidden />
                                <div class="card search-card __inline-13"><div class="card-body search-result-box __h-400px overflow-x-hidden overflow-y-auto"></div></div>
                            </form>
                        </div>
                        <div class="navbar-toolbar d-flex flex-shrink-0 align-items-center">
                            <a class="navbar-tool navbar-stuck-toggler" href="#">
                                <span class="navbar-tool-tooltip">Expand Menu</span>
                                <div class="navbar-tool-icon-box"><i class="navbar-tool-icon czi-menu open-icon"></i><i class="navbar-tool-icon czi-close close-icon"></i></div>
                            </a>
                            <div class="navbar-tool open-search-form-mobile d-lg-none ml-md-3">
                                <a class="navbar-tool-icon-box bg-secondary" href="#0"><i class="tio-search"></i></a>
                            </div>
                            <div class="navbar-tool dropdown d-none d-md-block ml-md-3">
                                <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" href="{{route('wishlists')}}">
                                    <span class="navbar-tool-label"><span class="countWishlist">{{session()->has('wish_list')?count(session('wish_list')):0}}</span></span><i class="navbar-tool-icon czi-heart"></i>
                                </a>
                            </div>
                            <div class="dropdown">
                               
                            </div>
                            <div id="cart_items0"><a href="#">Track Your order</a></div>
                            @if(auth('customer')->check())
                                <div class="dropdown">
                                    <a class="navbar-tool ml-3" type="button" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                        <div class="navbar-tool-icon-box bg-secondary">
                                            <div class="navbar-tool-icon-box bg-secondary">
                                                <img  src="{{asset('storage/app/public/profile/'.auth('customer')->user()->image)}}"
                                                    onerror="this.src='{{asset('public/assets/front-end/img/user.png')}}'"
                                                    class="img-profile rounded-circle __inline-14">
                                            </div>
                                        </div>
                                        <div class="navbar-tool-text">
                                            <small>{{translate('hello')}}, {{auth('customer')->user()->f_name}}</small>
                                            {{translate('dashboard')}}
                                        </div>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-{{Session::get('direction') === "rtl" ? 'left' : 'right'}}" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item"
                                        href="{{route('account-oder')}}"> {{ translate('my_Order')}} </a>
                                        <a class="dropdown-item"
                                        href="{{route('user-account')}}"> {{ translate('my_Profile')}}</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item"
                                        href="{{route('customer.auth.logout')}}">{{ translate('logout')}}</a>
                                    </div>
                                </div>
                            @else
                                <div class="dropdown">
                                    <a class="navbar-tool {{Session::get('direction') === "rtl" ? 'mr-md-3' : 'ml-md-3'}}"
                                    type="button" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                        <div class="navbar-tool-icon-box bg-secondary">
                                            <div class="navbar-tool-icon-box bg-secondary">
                                                <svg width="16" height="17" viewBox="0 0 16 17" 
                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M4.25 4.41675C4.25 6.48425 5.9325 8.16675 8 8.16675C10.0675 8.16675 11.75 6.48425 11.75 4.41675C11.75 2.34925 10.0675 0.666748 8 0.666748C5.9325 0.666748 4.25 2.34925 4.25 4.41675ZM14.6667 16.5001H15.5V15.6667C15.5 12.4509 12.8825 9.83341 9.66667 9.83341H6.33333C3.11667 9.83341 0.5 12.4509 0.5 15.6667V16.5001H14.6667Z"
                                                    fill="#ec1c24"
                                                />   
                                                </svg>

                                            </div>
                                        </div>
                                    </a>
                                   
                                    <div class="dropdown-menu __auth-dropdown dropdown-menu-{{Session::get('direction') === "rtl" ? 'left' : 'right'}}" aria-labelledby="dropdownMenuButton"
                                        style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
                                        <a class="dropdown-item" href="{{route('customer.auth.login')}}">
                                            <i class="fa fa-sign-in {{Session::get('direction') === "rtl" ? 'mr-2' : 'mr-2'}}"></i> {{translate('sign_in')}}
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{route('customer.auth.sign-up')}}">
                                            <i class="fa fa-user-circle {{Session::get('direction') === "rtl" ? 'mr-2' : 'mr-2'}}"></i>{{translate('sign_up')}}
                                        </a>
                                    </div>
                                </div>
                            @endif
                            <div id="cart_items">
                                @include('layouts.front-end.partials.cart')
                            </div>
                          
                        </div>
                    </div>
                </div>
                <div class="navbar navbar-expand-md navbar-stuck-menu">
                    <div class="container px-10px">
                        <div class="collapse navbar-collapse" id="navbarCollapse" style="text-align: left;">
                            <div class="w-100 d-md-none" style="text-align: right;">
                                <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarCollapse"><i class="tio-clear __text-26px"></i></button>
                            </div>
                            <ul class="navbar-nav">
                                
                            @if($web_config['navbar_categories'] && $web_config['navbar_categories']->count() > 0)    
                                @foreach($web_config['navbar_categories'] as $nav_bar_categories)
                                    @if($nav_bar_categories && $nav_bar_categories->product_count && $nav_bar_categories->product_count > 0)    
                                        <li class="menu-list">
                                            <div class="dropdown">
                                                
                                            
                                                <button
                                                    class="btn dropdown-toggle text-white text-max-md-dark "
                                                    type="button"
                                                    id="dropdownMenuButton"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                    style="padding-left: 0.0rem;"
                                                >
                                                {{ ucwords($nav_bar_categories->name) }}
                                                </button>
                                                @if($nav_bar_categories->childes && $nav_bar_categories->childes->count() > 0)    
                                                    <div class="dropdown-menu __dropdown-menu-3 __min-w-165px" aria-labelledby="dropdownMenuButton" style="text-align: left;">
                                                        @foreach($nav_bar_categories->childes as $sub_categories)
                                                            @if($sub_categories && $sub_categories->sub_category_product_count && $sub_categories->sub_category_product_count > 0)    
                                                            <a class="dropdown-item" href="{{config('app.url')}}/category_based_products/{{$sub_categories->id}}">{{ucwords($sub_categories->name)}}</a>
                                                            <div class="dropdown-divider"></div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        