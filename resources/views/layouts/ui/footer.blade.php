<span id="update_nav_cart_url" data-url="cart/nav-cart-items.html"></span><span id="remove_from_cart_url" data-url="cart/remove.html"></span><span id="update_quantity_url" data-url="cart/updateQuantity-guest.html"></span>
        <span id="order_again_url" data-url="cart/order-again.html"></span>
        <style>
            .social-media :hover {
                color: #000 !important;
            }
            .start_address_under_line {
                width: 331px;
            }
        </style>
        <div class="__inline-9 rtl">
            <footer class="page-footer font-small mdb-color rtl">
                <div class="pt-4" style="background: #fff;">
                    <div class="container text-center __pb-13px">
                        <div class="row text-center text-md-left mt-3 pb-3">
                            <div class="col-md-3"></div>
                            <div class="col-md-6 footer-padding-bottom">
                                <div class="mb-2">
                                    <h6 class="text-uppercase mb-3 font-weight-bold footer-heder">Newsletter</h6>
                                    <span>Subscribe to our new channel to get latest updates</span>
                                </div>
                                <div class="text-nowrap mb-4 position-relative">
                                    <form action="#" method="post">
                                        <input type="hidden" name="_token" value="OlrBc2Ze5N3s50hzD4RvlsUYM02Dl1PKCGU4ZSdO" />
                                        <input type="email" name="subscription_email" class="form-control subscribe-border" placeholder="Your Email Address" required style="padding: 11px; text-align: left;" />
                                        <button class="subscribe-button" type="submit">Subscribe</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3 footer-web-logo">
                                <a class="d-block" href="index.html">
                                    <img class="" src="{{config('app.asset_url')}}/public/deals/assets/images/logo.jpg" alt="logo" />
                                </a>
                                
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-4 footer-padding-bottom">
                                        <h6 class="text-uppercase mb-4 font-weight-bold footer-heder">Contact information</h6>
                                        <p class="head-off">Head office</p>
                                        <ul class="widget-list __pb-10px address">
                                            <li>A UNIT OF EAGLE GROUPS, </li>
                                            <li>NO.44/1,2, City Link Road, </li>
                                            <li>Secretariat colony, Adambakkam, </li>
                                            <li>Chennai 600088</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 footer-padding-bottom" style="">
                                        <h6 class="text-uppercase mb-4 font-weight-bold footer-heder">Help & Service</h6>
                                        <ul class="widget-list __pb-10px">
                                            <li class="widget-list-item"><a class="widget-list-link" href="#">What’s New</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="#">FAQs</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="#">All Brands</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="#">Recently Viewed Products</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="#">Compare Products List</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 footer-padding-bottom" style="">
                                        <h6 class="text-uppercase mb-4 font-weight-bold footer-heder">Know More</h6>
                                        <ul class="widget-list __pb-10px">
                                            <li class="widget-list-item"><a class="widget-list-link" href="about-us">About Us</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="privacy-policy">Privacy Policy</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="refund-policy">Refund Policy</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="cancellation-policy">Cancellation Policy</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="terms">Terms & Condition</a></li>
                                           
                                            <li class="widget-list-item"><a class="widget-list-link" href="#">Careers</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="contacts">Contact us</a></li>
                                            <li class="widget-list-item"><a class="widget-list-link" href="#">Cooperate Enquiry</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="background: #fff">
                    <div class="container">
                        <div class="d-flex flex-wrap end-footer footer-end last-footer-content-align">
                            <div class="mt-3"><p class="text-left __text-16px">© Eagle Deals All Rights Reserved</p></div>
                            <div class="max-sm-100 justify-content-center d-flex flex-wrap mt-md-3 mt-0 mb-md-3 text-left">
                                <div class="payment_option">
                                    <p>Payment Option</p>
                                    <img src="{{config('app.asset_url')}}/public/deals/assets/images/paypal.png" alt="">
                                    <img src="{{config('app.asset_url')}}/public/deals/assets/images/mc.png" alt="">
                                    <img src="{{config('app.asset_url')}}/public/deals/assets/images/visa.png" alt="">
                                </div>
                            </div>
                            <div class="d-flex __text-14px">
                                <div class="payment_option">
                                    <p>EMI Option</p>
                                    <img src="{{config('app.asset_url')}}/public/deals/assets/images/paypal.png" alt="">
                                    <img src="{{config('app.asset_url')}}/public/deals/assets/images/mc.png" alt="">
                                    <img src="{{config('app.asset_url')}}/public/deals/assets/images/visa.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <div class="__floating-btn">
    @php($whatsapp = \App\CPU\Helpers::get_business_settings('whatsapp'))
    @if(isset($whatsapp['status']) && $whatsapp['status'] == 1 )
        <div class="wa-widget-send-button">
            <a href="https://wa.me/{{ $whatsapp['phone'] }}?text=Hello%20there!" target="_blank">
                <img src="{{asset('public/assets/front-end/img/whatsapp.svg')}}" class="wa-messenger-svg-whatsapp wh-svg-icon" alt="Chat with us on WhatsApp">
            </a>
        </div>
    @endif

    <!-- Vendor scrits: js libraries and plugins-->
</div>
        @include('layouts.ui.footer_scripts')
        <script>!function(e,t,a){var c=e.head||e.getElementsByTagName("head")[0],n=e.createElement("script");n.async=!0,n.defer=!0, n.type="text/javascript",n.src=t+"/static/js/widget.js?config="+JSON.stringify(a),c.appendChild(n)}(document,"https://app.wacto.in",{bot_key:"4b31a896c49741ed",welcome_msg:true,branding_key:"wacto",server:"https://app.wacto.in",e:"p" });</script>