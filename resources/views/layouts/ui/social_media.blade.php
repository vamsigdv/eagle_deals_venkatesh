<section class="social-medias">
            <div class="container">
                <div class="row">
                @php($whatsapp = \App\CPU\Helpers::get_business_settings('whatsapp'))
                    @if(isset($whatsapp['status']) && $whatsapp['status'] == 1 )
                    <div class="col-sm-2">
                        <div class="social-btns">
                            <a href="https://wa.me/{{ $whatsapp['phone'] }}?text=Hello%20there!" class="whats-app"><i class="fa fa-whatsapp" aria-hidden="true"></i> WhatasApp Us</a>
                        </div>
                    </div>
                    @endif
    
                    <div class="col-sm-2">
                        <div class="social-btns">
                            <a href="#" class="default"><i class="fa fa-phone" aria-hidden="true"></i> Talk To Us</a>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="social-btns">
                            <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="social-btns">
                            <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="social-btns">
                            <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="social-btns">
                            <a href="#" class="toutube"><i class="fa fa-youtube-play" aria-hidden="true"></i> Youtube</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>